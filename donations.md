---
layout: page
permalink: /about/donate/
title: Donations
subtitle: If you want to support my work or just thank me, you gain much <3
description: Some ways to support me and my work
robots: nofollow,noarchive,noodp,noydir
share: true
comments: true
---

> Malerei verwandelt den Raum in Zeit, Musik die Zeit in Raum.
 
_--- Hugo von Hofmannsthal, 1922_
{: .small}

<i class="fab fa-bandcamp fa-2x fa-fw" style="color:#477987;position:relative;top:0.2em;margin-right:0.2em;" title="Bandcamp wish list"></i>
<span class="hidden">Bandcamp wish list: </span><a href="https://bandcamp.com/tasmo/wishlist">bandcamp.com/tasmo</a>

<i class="fa fa-heart fa-2x fa-fw" style="color:#f6c915;position:relative;top:0.2em;margin-right:0.2em;" title="Liberepay"></i>
<span class="hidden">Liberepay: </span>
<a href="https://liberapay.com/tasmo/donate">liberapay.com/tasmo</a>

<i class="fab fa-patreon fa-2x fa-fw" style="color:rgb(255, 66, 77);position:relative;top:0.2em;margin-right:0.2em;" title="Patreon"></i>
<span class="hidden">Patreon: </span>
<a href="https://www.patreon.com/tasmo">patreon.com/tasmo</a>

<i class="fa fa-btc fa-2x fa-fw" style="color:#e38215;position:relative;top:0.2em;margin-right:0.2em;" title="Bitcoin"></i>
<span class="hidden">Bitcoin: </span>
<a class="disabled" href="bitcoin:bc1q0j0p5e9g0k3wkggpz9nk59j6lkq8546pn6prgp?label=tasmo.rocks">bitcoin:bc1q0j0p5e9g0k3wkggpz9nk59j6lkq8546pn6prgp</a>


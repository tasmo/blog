---
layout: page
permalink: /about/tasmo/
title: "/about:tasmo"
tags: [tasmo, Thomas Friese, whois, about]
image:
  feature: pattern.gif
share: false
comments: true
---

<h2 style="font-weight: normal;"><em><del datetime="2010-09-08T19:29:21+00:00">DJ?</del> <strong><ins datetime="2010-09-08T19:29:21+00:00">_</ins>Tasmo</strong></em> is <abbr title="not a number"><strong>NaN</strong></abbr> and a <strong>Cyber DJ</strong>.</h2>

<a href="/images/head_tasmo.jpg"><img class="pull-left" src="/images/head_tasmo.jpg" width="80" /></a>
[(T)His weblog](/about/site/) aims to express <strong>thoughts</strong> and point to more or less <strong>significant</strong> issues in the <strong>W</strong>orld <strong>W</strong>ide <strong>W</strong>eb and real life. The main author in this weblog is <em><acronym title="Tasmo">Thomas Friese</acronym></em>, who is an <strong>edit</strong>or, <strong>develop</strong>er, [**DJ**](/about/dj/), sound and visual <strong>perform</strong>er and <strong>technic</strong>ian.

My activities in the internet can be found at:

<ul class="inline-list">
    <li><a href="https://hearthis.at/tasmo/podcast/"><i class="fas fa-podcast"></i> Podcast</a></li>
    <li><a rel="me" href="https://m.flaem.ing/@tasmo"><i class="fab fa-mastodon"></i> Mastodon</a></li>
    <li><a href="http://github.com/tasmo"><i class="fab fa-github"></i> GitHub</a></li>
</ul>

## What does Tasmo stands for?

Tasmo is a cheaply chosen anagram of my first name Thomas.

In the late '90s I was looking for a catchy artist name to distribute my music cassettes at bars. Because I could not think of a good name, I decided to roll the dice with the letters of my given name. Unfortunately the H fell down.

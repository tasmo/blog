---
layout: page
permalink: /about/dj/
title: "/about:DJ_Tasmo"
tags: [tasmo, Thomas Friese, whois, about, DJ, vita]
image:
  feature: pattern.gif
share: false
comments: true
robots: nofollow,noarchive,noodp,noydir
---

<h2><strong>DJ_Tasmo</strong> spielt Bass.</h2>

<a href="/images/head_tasmo.jpg"><img class="pull-left" src="/images/head_tasmo.jpg" width="80" /></a>Die Liebe zu dem Instrument ist in der Musik der rote Faden. Als DJ beginnt Tasmo mit Funk, Soul und Disco gepaart mit Jazz und Hip Hop. Gleichzeitig nimmt sein Faible zu experimenteller Elektronik immer mehr Einfluss.  
Heute ist sein Stil von Dub, Dubstep, Glitchhop und UK-Bass mit gebrochenen Beats und satten Bässen geprägt, ohne die Disco- und Soul-Wurzeln zu verlieren.

Als DJ hat er als Resident bei den [Blogrebellen](//blogrebellen.de) die Party-Reihe "[#Tassebier](/tags/#tassebier)" mitgeprägt, spielte mehrmals [in der Dubstation beim Fusion-Festival](/3452/fusion-with-love/) und war immer wieder Gast-DJ in Saetchmos wöchentlicher Sendung "[Echochamber](/3688/echochamber-vertretung/)".

Ein öffentliches Profil mit zahlreichen DJ-Sets ist auf der Plattform Hearthis zu finden:  
[https://hearthis.at/tasmo](https://hearthis.at/tasmo)

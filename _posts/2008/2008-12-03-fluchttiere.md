---
title: Fluchttiere
layout: post
lang: de
date: 2008-12-03T02:45:29+01:00
categories: [performance]
tags: [old,performance,upright bass]
comments: true
share: true
---

Ein kleiner Ausschnitt aus "[Wann stören wir uns endlich](http://www.ballhausost.de/index.php?article96&sub=21)" von und mit Andreas Liebmann (Performance, Cello) im [Ballhaus Ost](http://www.ballhausost.de/). Weiterhin sind Beatrice Fleischlin (Performance), Lajos Talamonti (Performance), Michael Bauer (Musik) und Thomas Friese (Musik) zu hören.

["Wann stören wir uns endlich"  
![Fluchttiere](/images/2008/fluchttiere.jpg)](https://www.youtube.com/watch?v=9-uB3P2tBTU)

Danke [Michael](http://michaelhoelzl.com/)!

*[Thomas Friese]: ich

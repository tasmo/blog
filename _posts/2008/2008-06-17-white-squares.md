---
title: The White Squares must survive!
layout: post
permalink: /199/white-squares/
date: 2008-06-17
dsq_thread_id:
  - 161618145
categories:
  - Video
tags:
  - animation
  - selfmade
  - shorts
  - video
comments: true
share: true
---

<iframe style="border:none;" src="//player.vimeo.com/video/1189360?title=0&amp;byline=0&amp;portrait=0&amp;color=cccccc" width="648" height="518" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

["The White Squares Must Survive!"](http://vimeo.com/1189360) is my first Adobe™ After Effects™ project from 2006 and a study how far to go just with squares (and a few addons).  
I decided to make square music for this short animation as well to give the funk back to the squares.

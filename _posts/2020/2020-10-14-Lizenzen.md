---
title: GPL, MIT, WTFPL – häh?
subtitle: Eine Einführung in Freie- und Open-Source-Software-Lizenzen
description: Warum gibt es so viele Software-Lizenzen? Was heißen die ganzen Abkürzungen und sind die Unterschiede so groß? Was ist der Unterschied zwischen Open-Source und freier Software?
permalink: /freie-software-lizenzen/
layout: post
lang: de
categories: tech
tags:
  - copyright
  - OpenSource
  - code
  - Creative Commons
  - howto
comments: true
share: true
image:
  feature: 2020/lizenzen.jpg
  credit: Thomas Friese (CC0)
modified: 2020-10-18T12:00:00+02:00
---

Wer Open-Source-Software entwickelt, befasst sich auch mit Lizenzen. Seit Januar 2019 betreue ich im Rahmen des [Prototype Fund] Projekte, die für das Programmieren von Software im Bereich Public Interest Tech gefördert werden. Weil das Thema Lizenzen immer wieder aufkommt und auch für die Förderprojekte wichtig ist, habe ich zusammengefasst, was Lizenzen sind, welche Unterschiede es zwischen ihnen gibt und für welche Arten von Softwareprojekten sie sich eignen.<!--more-->

## Freie Software und Open Source

Mit [Freier Software][Freie Software] erhalten Nutzer\*innen die Nutzungsrechte und an Software ohne Vorbehalte und Einschränkungen, diese werden so pauschal für alle eingeräumt. Die zwei wesentlichen Säulen sind frei verfügbare Quelltexte und das Recht auf freie Nutzung.

Open Source hat ihren Urprung unter anderem der Do-it-yourself-Bewegung, der Hacker-Bewegung der 1960/1970er und der Freie-Software-Bewegung der 1980er Jahre. Es ist Software, ["deren Quelltext öffentlich ist und von Dritten eingesehen, geändert und genutzt werden kann."][Open Source] Laut der Open Source Initiative (OSI) steht dabei der praktische Nutzen einer frei verfügbaren Softwareinfrastruktur für die Allgemeinheit im Vordergrund. Dazu gehören ein freier Softwaremarkt und die Ermöglichung einer kollaborativen Entwicklungsmethode.

Freie Software sieht Open Source als eine Bedingung an, ist auf die Kontrolle durch Nutzer\*innen fokussiert und konzentriert sich vor allem soziale, politische und ethische Anliegen. Das Recht auf freie Nutzung wird dabei mit Softwarelizenzen geregelt und abgesichert.

## Was sind Lizenzen?

Eine [Lizenz] ist generell eine Genehmigung, Dinge zu tun, die ohne diese nicht erlaubt wären. Mit einer Lizenz wird also ein Recht auf Nutzung eingeräumt. Eine Softwarelizenz ist ein Vertrag, mit dem bestimmte Nutzungsrechte an einer urheberrechtlich geschützten Software überlassen und/oder beschränkt werden.[^2] Sie kann dabei u. a. Dinge wie das allgemeine Nutzungsrecht sowie das Recht auf Veränderung, Weiterverbreitung und Linking regeln. Außerdem ergeben sich aus ihr Pflichten sowie Nutzungsbedingungen.[^1] Dabei verbleibt das Recht am Eigentum verbleibt bei den Lizenzgebenden.[^3]

**Im folgenden ist bei Lizenz immer die Rede von einer Software-Lizenz.**

### Was regeln Lizenzen?

Mit Lizenzen können die Rechte an folgenden Punkten festgelegt werden:

- Allgemeines Nutzungsrecht an der Software
  - Zahl der Nutzer\*innen, Art der Nutzung usw.
- Recht auf Weiterverbreitung der Software
- Recht auf Veränderung etwaiger Binärdateien
- Recht auf Veränderung des Quellcodes, sofern vorhanden
- Recht auf Weiterverbreitung veränderter Versionen der Binärdateien oder des Quellcodes
- Recht auf Verbindung (“Linking”) der Binärdateien oder des Quellcodes mit anderer Software
- statisches Linking vs. dynamisches Linking[^2]

Mit einer Lizenz sind jedoch nicht nur Rechte geregelt, es ergeben sich aus einer Lizenz auch Pflichten.[^1]  
Es gibt in den meisten Lizenznen Bedingungen, unter denen die Nutzung steht.

### Was können Lizenzen nicht regeln?

Für das bloße Ausführen eines Programms im nicht-öffentlichen, privatem Rahmen ist keine Lizenz erforderlich, da dies keinem Verbot unterliegt.[^3] Oft angezeigte End User License Agreements (EULA) sind keine Lizenzen sondern Nutzer\*innen aufgedrängte Scheinverträge -- und im übrigen nur dann gültig, wenn sie vor dem Erwerb einer Software bekannt waren.

![End User License Agreement](/images/2020/eula.png)

EULAs erlauben selten etwas, was nicht ohnehin schon erlaubt ist, zum Beispiel das Programm zu benutzen.
So sollen sich Lizenznehmende Verpflichtungen ohne eine Gegenleistung auferlegen.
Dies widerspricht dem grundsätzlichen Aufbau eines Vertrages.  
Nach Erwerb einer Software kann nicht mehr davon ausgegangen werden, dass die Nutzenden dem Vertrag wirklich zustimmen.[^3]

## Kleine Geschichte der Freien und Open-Source-Lizenzen

Noch in den 1950er und 60er Jahren wurde Software fast ausschließlich von Wissenschaftlern im akademischen Rahmen in Zusammenarbeit geschrieben.
Software wurde zu diesem Zeitpunkt überhaupt noch nicht als Ware gesehen sondern nach den Prinzipien der Offenheit und Zusammenarbeit vertrieben, was später zu einem zentralen Element der Hacking-Kultur wurde.

Ende der 1960er Jahre stiegen die Produktionskosten für Software durch die Entwicklung von Betriebssystemen und Compilern für Programmiersprachen im Vergleich zu Hardware-Kosten stark an.  
Als Folge entsteht die Software-Industrie, die mit den gebündelten Software-Produkten der Hardware-Hersteller konkurriert -- geleaste Maschinen erfordern Software-Unterstützung, während sie keine Einnahmen mehr für Software bringen.  
Als Folge gibt es immer mehr Software, die unter eingeschränkten Rechten bezahlt werden muss.

Bis 1974 war Software generell nicht mit Lizenzen versehen und wurde als Public-Domain-Software, typischerweise mit Quellcode, behandelt und weitergegeben.  
Die US-Kommission für neue technologische Verwendungen urheberrechtlich geschützter Werke (CONTU) entscheidet 1974, dass "Computerprogramme, soweit sie die ursprüngliche Schöpfung eines Autors verkörpern, ordnungsgemäß Gegenstand des Urheberrechts sind". Das heißt, dass ab dem Zeitpunkt Software ohne besondere Kennzeichnung nicht mehr einfach weitergegeben werden darf.

In den späten 1970er und frühen 1980er Jahren beginnen Computerhersteller und reine Softwarefirmen damit, Software generell eingeschränkt zu lizensieren und zu vermarkten.
Neue Softwareentwicklungen werden durch Urheberrechte, Warenzeichen und Leasingverträge rechtlich beschränkt.
Um die Einnahmen zu steigern, beginnen viele Firmen, nur noch die ausführbaren Binärdateien ohne Quelltexte zu verkaufen.[^6]

### Geburt des GNU -- Copyleft

Die Firma AT&T veröffentlicht 1983 eine proprietäre Version von UNIX unter dem Namen "UNIX System V".
Daraufhin gründet Richard Stallman im September des selben Jahres das GNU-Projekt mit dem Ziel, ein freies, UNIX-ähnliches Betriebssystem zu entwickeln.[^4]

Um das rechtlich abzusichern, entwirft er das Copyleft-Prinzip -- das beinhaltet, dass alle, die die Software (mit oder ohne Änderungen) weiter verteilen, die Freiheit zum Weitergeben und Verändern mitgeben müssen.
Somit wird garantiert, dass alle Nutzer\*innen die gleichen Freiheiten haben und behalten.
Das ist die Grundlage aler Gnu-Lizenzen.[^4]

Das Konzept von "Copyleft" beschreibt Richard Stallman 1985 in seinem Buch "GNU Manifesto":

> GNU is not in the public domain. Everyone will be permitted to modify and redistribute GNU, but no distributor will be allowed to restrict its further redistribution. That is to say, proprietary modifications will not be allowed. I want to make sure that all versions of GNU remain free.

Als eine Copyleft unterstützende Organisation wird 1985 die Free Software Foundation gegründet.
Sie gibt dem GNU-Projekt einen administrativen, rechtlichen und finanziellen Rahmen.

Bereits Mitte der 1980er Jahre experimentierten mehrere Programmierer mit Copyright-Vermerken im Quellcode, die dazu aufriefen, den Source Code zu kopieren und verändern. So gab Larry Wall 1985 dem Unix Tool `trn` den Hinweis mit:
> You may copy the trn kit in whole or in part as long as you don’t try to make money off it, or pretend that you wrote it.

1985 veröffentlicht Stallman den Editor Emacs und legt die "GNU Emacs Copying Permission Notice" bei, die allen Nutzer\*innen das Recht einräumt, Kopien und Modifizierungen des Programms zu machen, solange sie nicht das Recht am Eigentum an der veränderten Version von Emacs erheben. Darauf aufbauend, veröffentlicht Stallman 1986 die "GNU Emacs General Public License" als erste freie Software-Lizenz.

Stallman bekommt anschließend Nachfragen, die Lizenz solle nicht nur für Emacs gelten.
Ende 1986 ändert er den Emacs-Copyright-Vermerk in eine "Copyleft"-Lizenz -- ein Begriff, den er vom Programmierer Don Hopkins entliehen hat -- und der für alle Softwareprogramme gelten könnte.
Diese neue Lizenz wird die erste GNU General Public License (GPL).
Im Jahr 1989 veröffentlicht das GNU-Projekt erst die GPL in der Version 1.0.[^5]

Die Version 2 der GPL beinhaltet dann die "Liberty or Death"-Klausel -- sie sagt, Lizenznehmer\*innen dürfen den Quellcode nur dann weiterverbreiten, wenn sie alle in der Lizenz festgehaltenen Pflichten erfüllen können, unabhängig von Verpflichtungen, die aus anderen Lizenzen oder Verträgen entstehen könnten.
Damit wird 1992 der Grundstein für die Freiheit der Nutzer\*innen gegen eventuelle Patentverletzungsklagen in der GPL gelegt.

Als wichtigen Meilenstein veröffentlicht Linus Torwalds im selben Jahr den Linux Kernel in der Version 0.12 erstmals unter der GPL.
Mit diesem Schritt erreicht er mehr Freiwillge, die an dem Kernel mitwirken wollen.

Die erste ausschließlich unter GPL stehende Software verwendende Linux-Distribution [Debian GNU/Linux wird 1993 von Ian Murdock begonnen.][Debian]
Debian ist ausdrücklich den GNU- und FSF-Prinzipien freier Software verpflichtet, was im [Debian-Gesellschaftsvertrag] festgehalten ist.

### BSD & Co. erscheinen -- freizügige Lizenzen

Als erste nicht ganz restriktive Lizenz ist die der GPL ähnelnde "GCC General Public License" für GNU Compiler Collection ist 1987 erschienen. Sie beinhaltet eine Ausnahmegenehmigung fürs Linken.

Die Verwaltung der University of California, Barkeley veröffentlicht 1988 die BSD-Lizenz in ihrer ersten Version.[^7]  
Sie gilt als die erste in der Gruppe der "Freizügige Open-Source-Lizenzen" und unterscheidet sich von der GPL, dass die keinen Copyleft-Vermerk enthält.
So ist niemand verpflichtet, den Quellcode mit zu veröffentlichen, lediglich der Lizenz-Vermerk muss immer erhalten bleiben.

## Kategorien von Open-Source-Lizenzen

Im Lizenz-Center des "Institut für Rechtsfragen der Freien und Open Source Software" (ifrOSS)[^8] sind insgesamt  275 Open-Source-Lizenzen aufgelistet. Es kommen auch vortlaufend neue dazu, denn jede\*r kann eine eigene Lizenz für die eigenen Bedürfnisse veröffentlichen.

Im Folgenden sind die Hauptgruppen mit den wichtigsten Beispielen aufgeführt.

### Public Domain und ähnliche

Den Nuzter\*innen werden alle Rechte gewährt und keine Pflichten gefordert.

Software-Lizenzen:

- Gemeinfreiheit (PD)
- Creative Commons -- Kein Urheberrechtsschutz (CC0)
- WTF Public License (WTFPL)
- BSD 0-clause License
- Unlicense

Kreativ-Lizenzen:

- Gemeinfreiheit (PD)
- Creative Commons -- Kein Urheberrechtsschutz (CC0)
- Künstlerische Freiheit

### Permissive Licenses (BSD-ähnliche)

Gewähren den Nutzenden alle Nutzungsrechte.
Kopien müssen nicht unter der gleichen Lizenz stehen.
Die Software und der Quellcode dürfen in kommerzieller, proprietärer Software verwendet werden.
Die Lizenzen dürfen mit anderen Lizenzen gemischt werden.
Nur der Lizenzvermerk muss immer erhalten bleiben.

Stark verbreitete Software-Lizenzen:

- BSD-Lizenzen
- MIT-Lizenz (auch X-Lizenz oder X11-Lizenz)
- Apache

Weitere Software-Lizenzen:

- GNU All-permissive License
- Academic Free License (AFL)
- Artistic License

Kreativ-Lizenzen:

- Creative Commons mit Namensnennung (CC-BY)

### Schwaches Copyleft

Hat Copyleft als Grundlage, nur werden Ausnahmen eingeräumt.

Software-Lizenzen:

- GNU Lesser General Public License (LGPL)
- GPL linking exception
- Mozilla Public License (MPL)

### Copyleft

Es gewährt alle Nutzungsrechte und verbietet die Verwendung in nicht-freier Software.

Software-Lizenzen:

- GNU General Public License (GPL)
- GNU Affero General Public License (AGPL)
- The Common Development and Distribution License (CDDL)

Weitere Software-Lizenzen:

- Common Public License (CPL)
- European Public License (EUPL)

Kreativ-Lizenzen:

- Creative Commons mit Namensnennung und Weitergabe unter gleichen Bedingungen (CC-BY-SA)
- GNU Free Documentation License (GNU FDL)
- Design Science License (DSL)
- Open Content License (OPL)
- SIL Open Font License (OFL)

### Nicht-kommerzielle Lizenzen

Erlauben die Nutzung nur abseits von wirtschaftlichen Interessen -- privater Nutzung oder für gemeinnützige Organisationen.
Können mit der GPL und anderen Copyleft-Lizenzen kombiniert werden.

Software-Lizenzen:

- Java Research License (JRL)
- Aladdin Free Public License (AFPL)

Kreativ-Lizenzen:

- Creative Commons mit Namensnennung und nicht kommerziell (CC-BY-NC)

### Prpoprietäre Lizenzen

Beschränken das Recht und die Möglichkeiten der Wieder- und Weiterverwendung sowie Änderung und Anpassung durch Nutzer\*innen und Dritte.  
Gängige Mittel zur Einschränkung sind Softwarepatente, Urheberrecht, Lizenzbedingungen (EULAs), nicht veröffentlichte Standards und die Behandlung des Quelltextes als Geheimnis.

### Betriebs- und Geschäftsgeheimnis

Es gibt keine öffentliche Informationen beziehungsweise nur innerhalb von geschlossenen Gruppen benutzbar.

Wenn keine Lizenz angegeben ist, gilt das Urheberrecht.

### Übersicht der wichtigsten Lizenzen

| | BSD/MIT | Apache | AGPLv3 | GPLv3 | GPLv2.1 | LGPLv3 | LGPLv2.1 | MPL-2
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:
|Verteilen | ✔ | ✔ | CL | CL | CL | CL | CL | CL
|Ändern | ✔ | ✔ | CL | CL | CL | CL | CL | CL
|Linken | ✔ | ✔ | GPL | GPL | teilweise | teilweise | teilweise | ✔
|Software as a service | ❌ | ❌ | ✔ | ❌ | ❌ | ❌ | ❌ | ❌
|Schutz vor Tivoisierung | ❌ | ❌ | ✔ | ✔ | ❌ | ✔ | ❌ | ❌
|Schutz vor Patent-Trolling | ❌ | ✔ | ✔ | ✔ | ❌ | ✔ | ❌ | ❌
|Schutz vor Proprietarisierung | ❌ | ❌ | ✔ | ✔ | ✔ | teilweise | teilweise | teilweise
|Sublizenz | ✔ | ✔ | CL | CL | CL | CL | CL | CL
|wirkt auf | --- | --- | Projekt | Projekt | Projekt | Library | Library | Datei
|Markenrechtsschutz | ❌ | ❌ | ✔ | ✔ | ❓ | ✔ | ❓ | ❌

## Verbreitung und Beliebtheit

Noch [2003 waren auf SourceForge][berlios] die Copyleft-Lizenzen sehr viel beliebter beziehungsweise weiter verbreitet, als die freizügigen Lizenzen.

![Verteilung der Open-Source-Lizenzen auf SourceForge 2003](/images/2020/open-source-lizenzen.png)

Eine [2015 auf Github veröffentlichte Analyse][Open Source 2015 on Github] zeigt, dass Mitte der 2010er Jahre freizügige Lizenzen im Verhältnis an Verbreitung gewonnen haben.

![License breakdown on github.com by repository creation date](/images/2020/license-breakdown.png)

In den letzten Jahren stiegen die freizügigen Lizenzen im Trend. Die MIT- und die Apache-Lizenz sind gerade die beiden am häufigsten genutzten Lizenzen. Copyleft-Lizenzen werden etwas weniger benutzt, aber nur im Vergleich zu den anderen Open-Source-Lizenzen. Ein Grund dafür ist, dass Projekte sich zunehmend für duale Lizenzen ([Mehrfachlizenzierung]) entscheiden. Damit kann Software zum Beispiel kommerziell vertrieben und gleichzeitig von einer Community betreut werden. Auch können andere restriktive Erweiterungen benutzt werden.

![Permissive Open Source Licenses Continue to Trend](/images/2020/permissive-vs-copyleft-over-time.jpg)  
<small>Trending Open Source Licenses by [WhiteSource]</small>

Beim Prototype Fund haben wir 2020 erstmals verglichen, wie viele bei uns geförderte Projekte [welche Lizenz benutzen][dataviz]. Hier zählten GPL und MIT zu den meistgenutzten Lizenzen.

![Lizenzen beim Prototype Fund 2020](/images/2020/Lizenzen-beim-Prototype-Fund.png)  
<small>Lizenzen beim Prototype Fund, Stand Runde 7, 2020</small>

## Rechtliches und Klagen

Es gab zwei größere, beispielhafte Prozesse, bei denen Unternehmen versuchten, freie Software in ihren ausschließlichen Besitz zu bringen:

1992 klagte AT&T gegen die Universität von Kalifornien um die Rechte an den UNIX-Quellen.
Wie sich herausstellte, hatte AT&T die freie universitäre Produktion gesamt in ihren Bestand übernommen und lediglich die Urheberangaben entfernt.
Im Rahmen eines Vergleichs erhielt AT&T von etwa 18.000 umstrittenen Dateien ausschließliche Rechten an dreien.

2003 klagte SCO gegen IBM um die Rechte an den Linux-Quellen.
SCO begründete ihren Vorstoß mit angeblichen Verletzungen des UNIX-Copyrights.
IBM hätte Quellen aus UNIX illegal nach Linux übertragen.
SCO konnten seine Behauptung im Prozess aber nicht belegen.
Das Verfahren endete, als sich herausstellte, dass SCO nicht im Besitz von UNIX-Urheberrechten war, sondern diese bei Novell lagen.[^4]

### Rechtsprechung in Deutschland

Wer in Deutschland einen Verstoß feststellt muss als erstes mahnen und verlangen, dass der Vertrieb nur unter den vorausgesetzten Bedingungen erfolgt.
Je nach Lizenz bedeutet das, dass der Quellcode unter der entsprechenden Lizenz verfügbar gemacht werden muss -- bei strengen Copyleft-Lizenzen auch die Anpassungen beziehungsweise Erweiterungen.

Zudem kann die Erstattung von Mahnkosten und Schadensersatz gefordert werden.
Die Beklakten müssen dabei Auskunft über Kund\*innen geben, damit die Distributionskette nachvollzogen werden kann.

Wenn darauf hin nichts passiert, kann aufgrund einer "dauerhaften Verletzung des Copyrights" eine vorläufige einstweilige Verfügung beantragt werden.
Ein Einspruch der Gegenseite würde es dann zu einem Gerichtsverfahren führen.

Eine Klage gegen eine solche Urheberrechtsverletzung kann in Deutschland nur von den Urhebern beziehungsweise deren rechtliche Vertreter\*innen geführt werden.
Die klagende Partei muss also maßgeblich an der Entwicklung der Software beteiligt gewesen sein.

---

{: .small}
Dieser Artikel erschien zuerst auf [PrototypeFund.de](https://prototypefund.de/freie-open-source-lizenzen/). Vielen Dank an Patricia für's Lektorieren!

[berlios]: https://www.berlios.de/open-source-lizenzen/
[dataviz]: https://prototypefund.de/projects/?filter=dataviz
[Debian]: https://www.debian.org/intro/about#history
[Debian-Gesellschaftsvertrag]: https://www.debian.org/social_contract.de.html
[Freie Software]: https://de.wikipedia.org/wiki/Freie_Software
[Lizenz]: https://de.wikipedia.org/wiki/Lizenz
[Mehrfachlizenzierung]: https://de.wikipedia.org/wiki/Mehrfachlizenzierung
[Open Source 2015 on Github]: https://github.blog/2015-03-09-open-source-license-usage-on-github-com/
[Open Source]: https://de.wikipedia.org/wiki/Open_Source
[Prototype Fund]: https://prototypefund.de/
[WhiteSource]: https://resources.whitesourcesoftware.com/blog-whitesource/top-open-source-licenses-trends-and-predictions

[^1]: <https://www.brennecke-rechtsanwaelte.de/Lizenzrecht-eine-Einfuehrung-Lizenzarten-und-Lizenzvertraege-Teil-01-Einfuehrung_203165>
[^2]: <https://www.berlios.de/open-source-lizenzen/#was-ist-eine-lizenz>
[^3]: <https://de.wikipedia.org/wiki/Lizenz>
[^4]: <https://de.wikipedia.org/wiki/Freie_Software>
[^5]: <https://www.channelfutures.com/open-source/a-brief-history-of-free-and-open-source-software-licensing>
[^6]: <https://en.wikipedia.org/wiki/History_of_free_and_open-source_software>
[^7]: <https://en.wikipedia.org/wiki/Free-software_license>
[^8]: <https://ifross.github.io/ifrOSS/Lizenzcenter>

*[CL]: Copyleft
*[EULA]: End User License Agreement
*[EULAs]: End User License Agreements
*[GPL]: GNU Public License
*[GNU]: GNU's not Unix!
*[MIT]: Massachusetts Institute of Technology
*[BSD]: Berkeley Software Distribution

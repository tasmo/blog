---
title: Zwei Jingles für zwei Podcasts
subtitle: 'Die Intros für die Podcasts der FSFE und des Prototype Fund'
description: 'Audiophile Coverbilder zum Hören'
permalink: /zwei-jingles/
layout: post
lang: de
categories:
  - music
tags:
  - selfmade
  - shorts
  - OpenSource
  - podcast
  - Prototype Fund
  - FSFE
  - Creative Commons
comments: true
share: true
image:
  feature: 2020/PIP-cover.jpg
modified: 2020-11-08T18:00:00+02:00
---

Ein Podcast hat viele Elemente, die wir schon lange zum Beispiel aus Blogposts kennen -- nur eben für die Ohren. Viele schriftliche Veröffentlichungen bekommen einheitliche grafische Stile, Illustrationen, wiederkehrende Icons und Coverbilder.

Ein solches Coverbild ist bei Radioproduktionen, Videos und Podcasts der Jingle. Er wird meist am Anfang, oft am Ende und manchmal auch als Trenner in der Mitte verwendet und ist so ein hörbares Logo, ein Avatar oder Icon. Wenn er am Anfang steht, wissen Hörer\*innen schon in den ersten Momenten, um welche Serie oder Art von Beitrag es sich handelt. Es ist die Schwelle, über die der Podcast betreten wird -- und genau das fehlte dem [Software Freedom Podcast] und dem [Public Interest Podcast] zu Beginn noch.<!--more-->

<iframe id="hearthis_at_user_" style="overflow:hidden;border:none;background-color:transparent;height:150px;width:100%;" src="https://app.hearthis.at/set/174901-7407/embed/"></iframe>

## Der Jingle zum Public Interest Podcast

Für uns im Team des Prototype Fund ist es wichtig, dass der Jingle die Offenheit und Aktualität der Themen um unser Projekt begleitet. Aber was kann rein ästhetisch zu einem Public Interest Podcast passen und was thematisch? Das schien am Anfang gar nicht einfach. Wir hatten zum Beispiel die Idee, [eine Roboter-Band] für uns spielen zu lassen.

Irgendwann kam der Gedanke, ein Stück Quellcode zu vertonen. Aber wie kann Programmcode klingen? Wir unterstützen die Entwicklung von Open Source, was vor allem Programmiercode ist. Programmieren findet nur selten auf einer Bühne und in der Öffentlichkeit statt und ist mit seinem Tastenklackern eher leise und akustisch monoton.

### Aber wie klingt Code?

Als erstes dachten wir, geschriebenen Code zu Morsecode zu übersetzen. Das könnte eventuell ein guter Rhythmus werden. Aber beim Morsen ist immer nur derselbe Ton zu hören.

Ich habe eine kleine Spieluhr, für die man mit Lochstreifen Melodien programmieren kann. Lochstreifen sind eines der ersten Medien, die digitale Informationen zur Steuerung enthielten und einfach vervielfältigt werden konnten. Telegrafen verwendeten Lochstreifen mit [Baudot-Code] (auch Fernschreibcode oder Telexcode) – das ist ein einfacher 5bit-Code, mit dem das ganze Alphabet abgedeckt ist.

Die Buchstaben unseres Hauptthemas, nämlich „Public Interest Tech“, in Baudot-Code (ITA1) sehen so aus:

!["Public Interest Tech", in Baudot-Code (ITA1)](/images/2020/ita1_PUBLIC_INTEREST_TECH.png)

Das können moderne Musik-Sequenzer hervorragend in ein Notenmuster und somit in Töne und Klang übersetzen. Die MIDI-Noten im Sequencer sehen dann so aus:

![der Baudot-Code übersetzt in MIDI](/images/2020/midi_PUBLIC_INTEREST_TECH.png)

### Musik bitte!

Das klingt zwar – aber noch nicht so richtig nach einer Melodie. Das ist die [Frequenzanalyse der rohen MIDI-zu-Sound-Übersetzung]:

[![Frequenzanalyse der rohen MIDI-zu-Sound-Übersetzung](/images/2020/spectrum_PUBLIC_INTEREST_TECH.png)][Frequenzanalyse der rohen Mid-zu-Sound-Übersetzung]

Ich habe dann die Töne auf verschiedene Instrumente aufgeteilt. Dabei habe ich mir angehört, welche Töne davon kombiniert nach einer Melodie klingen. Zum Teil habe ich sie tonal und rhythmisch verschoben, ein paar Noten weggelassen und überleitende Zwischentöne hinzugefügt, bis eine Melodie entstand, die wiedererkennbar und relativ eingängig ist.

Die Melodie bekam zudem einen sie tragenden, mit Arpeggio versehenen Bass und Akzente durch elektronische und gesampelte Percussions. So sieht das [Frequenzspektrum des fertigen Jingles] aus:

[![Frequenzspektrum des fertigen Jingles](/images/2020/spectrum_jingle.png)][Frequenzspektrum des fertigen Jingles]

Das alles habe ich in Bitwig Studio arrangiert, dafür 10 verschiedene Instrumente benutzt und mit knapp 50 Effekten den Klang verschönert. Schlagzeuge und Bässe müssen komprimiert werden und damit es nicht kratzt, müssen Spitzen abgeschnitten werden. Equalizer zur Klangregelung sind in fast jeder Tonspur wichtig und dazu kommen noch Hall und Echo, die der Musik einen Raum geben.

Damit das Ende nicht abrupt stoppt und der Jingle angenehmer in die Sprache übergeht, gibt es am Ende Echo und Hall einzelner Tonspuren.

### Geschmackssache

<iframe id="hearthis_at_track_5244347" style="overflow:hidden;border:none;background-color:transparent;height:150px;width:100%;" src="https://app.hearthis.at/embed/5244347/transparent_black/?hcolor=e805cd&color=00bbff&style=2&block_size=2&block_space=1&background=1&waveform=0&cover=0&autoplay=0&css=" allow="autoplay"></iframe>
Listen to [Public Interest Podcast Jingle](https://hearthis.at/tasmo/master/) by [tasmo].
{:.small}

Wichtig ist dann noch die Meinung der anderen. Um bei der eigenen kreativen Arbeit nicht blind für die Wirkung zu werden, ist es wichtig, schon im Prozess Feedback einzuholen. Das Team und insbesondere Patricia haben mir mit ihren rein subjektiven Eindrücken sehr geholfen, mich auf das konkrete Ziel zu konzentrieren und auch ein paar Lieblinge rauszuschmeißen.

## Der Jingle zum Software Freedom Podcast

Im späten Sommer 2019 freute ich mich sehr, als Katharina und Matthias von der [Free Software Foundation Europe][FSFE] mich ansprachen, ob ich Lust hätte, ein Intro für den bald erscheinenden Podcast zu gestalten. Na klar hatte ich das!
Da ich schon verschiedene Musik für Theater, kleine Filme und den Dancefloor komponiert und produziert habe und mich mein Interesse immer weiter in Richtung [Freier Software][Lizenzen Blogpost] bewegte und ich noch nie einen Jingle für einen Podcast gemacht habe, darauf aber richtig Lust hatte, kamen hier viele gute Gründe zusammen.

Etwas einfacher als beim [Jingle für den Public Interest Podcast](#der-jingle-zum-public-interest-podcast) war die hier Wahl der Melodie -- schließlich hat die [FSFE] bereits eine Hymne, den vom Gründer Richard Stallman selbst performten [Free Software Song]. Dazu kam, dass sich Katharina und Matthias schon um den Stil Gedanken gemacht haben und ihn mir anhand von Beispielen gut kommunizieren konnten. Der Jingle sollte nach freier-Software-Szene klingen und darf ruhig den Charme eines [klassischen Game-Soundtracks][Tetris Theme] haben. Und kurz soll er sein.

Kurz? Wie kurz? --- Etwa 15 Sekunden.

### Kill Your Darlings

Die Podcast-Jingles, die sich bei mir eingeprägt haben, sind sehr episch und haben Längen bis zu drei Minuten. Also waren meine ersten Vorschläge auch länger.

Dazu kam auch berechtigte Kritik zu verschiedenen stilistischen Entscheidungen. So drehten wir viele Runden mit zahlreichen Wünschen und herangetatsteten Verbesserungen. Kurz vor der finalen Version dachte ich sogar, dass der Frust auf beiden Seiten jetzt so hoch gestiegen ist, dass wir entscheiden, dass doch jemand anderes den Jingle produzieren sollte. Es musste sogar die [erste Folge mit dem von mir sehr verehrten Cory Doctorow][Erste SFP-Folge] ohne Jingle erscheinen.

Hier eine der diskutierten Versionen:

<iframe id="hearthis_at_track_5244586" style="overflow:hidden;border:none;background-color:transparent;height:150px;width:100%;" src="https://app.hearthis.at/embed/5244586/transparent_black/?hcolor=&color=&style=2&block_size=2&block_space=1&background=1&waveform=0&cover=0&autoplay=0&css=" allow="autoplay"></iframe>
Listen to [FSFE](https://hearthis.at/tasmo/fsfe/) by [tasmo].
{: .small}

Schließlich bin ich sehr dankbar für die vielen Vorschläge, Nachfragen und die Geduld (auch von mir) und bin sehr zufrieden mit dem Intro. Entstanden ist ein Jingle, der gleich erkennbar ist, kurz genug, dass Hörer\*innen nicht gleich wieder abschalten, weil nichts passiert und eine Dynamik für einen guten Moderationsabsprung bietet. Mittlerweile mag ich sogar die Melodie. :stuck_out_tongue_winking_eye:

<iframe id="hearthis_at_track_5244342" style="overflow:hidden;border:none;background-color:transparent;height:150px;width:100%;" src="https://app.hearthis.at/embed/5244342/transparent_black/?hcolor=00d600&color=002bff&style=2&block_size=2&block_space=1&background=1&waveform=0&cover=0&autoplay=0&css=" allow="autoplay"></iframe>
Listen to [Software Freedom Podcast Jingle](https://hearthis.at/tasmo/fsfe-podcast-jingle-03/) by [tasmo].
{: .small}

---

{: .small}
Der [erste Teil dieses Artikels](#der-jingle-zum-public-interest-podcast) erschien zuerst auf [PrototypeFund.de](https://prototypefund.de/jingle/). Vielen Dank an Patricia für‘s Lektorieren!

[tasmo]: https://hearthis.at/tasmo/
[eine Roboter-Band]: https://www.midi-orchestra.net/
[Baudot-Code]: https://de.wikipedia.org/wiki/Baudot-Code
[Software Freedom Podcast]: https://fsfe.org/news/podcast.en.html
[Public Interest Podcast]: https://prototypefund.de/news/?filter=topics&topics=sound
[Frequenzanalyse der rohen Mid-zu-Sound-Übersetzung]: https://vimeo.com/462985242
[Frequenzspektrum des fertigen Jingles]: https://vimeo.com/462985131
[FSFE]: https://fsfe.org/
[Lizenzen Blogpost]: /freie-software-lizenzen/
[Free Software Song]: https://www.youtube.com/watch?v=9sJUDx7iEJw
[Tetris Theme]: https://www.youtube.com/watch?v=9Fv5cuYZFC0
[Erste SFP-Folge]: https://fsfe.org/news/podcast/episode-1.en.html

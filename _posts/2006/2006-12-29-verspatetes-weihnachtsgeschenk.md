---
title: verspätetes Weihnachtsgeschenk
layout: post
lang: de
date: 2006-12-29T16:19:00+01:00
categories: [music]
tags: [old,punk,music,radio]
comments: true
share: true
---

Nostalgie ist manchmal sehr schön.  
Wer das gern nachvollziehen möchte und genau wie ich in den späten 80ern der DDR alternative Musik gehört hat, ist hier genau richtig:

[![Zeichnung mit zwei Punks](/images/2006/337368936_a1548dc579.jpg)  
www.parocktikum.de/archiv](http://www.parocktikum.de/archiv)

Es gibt viel Punk-Musik in MP3-Form zum Runterladen oder als RSS-Feed, Teile alter Sendungen mit Lutz Schramm, Sendungsmanuskripte und ein Info-Wiki zum Mitbauen.

* * *

{: .small}
Image: Paracktikum @ Flickr

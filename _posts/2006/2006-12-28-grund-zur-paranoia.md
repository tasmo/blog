---
title: Grund zur Paranoia
layout: post
lang: de
date: 2006-12-28T14:53:00+01:00
categories: [tech]
tags: [old,privacy,security]
comments: true
share: true
---

Endlich kriege ich mal wieder Futter zur Begründung meines Verfolgungswähnchens, bei Heises:

[23C3: "Sind wir paranoid genug?"](https://www.heise.de/newsticker/meldung/83002)  
Diese, meine Paranoia ist zwar mehr in Richtung Online-Überwachung sensibelisiert, doch "vorsichtig" kann man ja nie genug sein.

Da kann ich auch gleich 2 Empfehlungen zum Erweitern des von mir bevorzugten Browsers Firefox geben:

* [Adblock Plus](https://addons.mozilla.org/de/firefox/addon/adblock-plus/) verhindert das Laden von Werbung, also auch "Spionage"-Grafiken, durch White-Lists und Regulare Ausdrücke und
* [NoScript](https://addons.mozilla.org/de/firefox/addon/noscript/) das Laden von PlugIns und Scripten, die Daten sammeln oder einfach nicht nötig erscheinen.

Weitere Erweiterungen hier, in meiner del.icio.us-Sammlung:  
[tasmo's bookmarks tagged with "Firefox+PlugIn" on del.icio.us](http://del.icio.us/tasmo/Firefox%2BPlugIn)

## update:

Hier gibt's Antworten auf die wichtigsten Fragen zum Thema Sicherheit im Internet:  
[CCC | FAQ - Sicherheit](http://dasalte.ccc.de/faq/security.de)

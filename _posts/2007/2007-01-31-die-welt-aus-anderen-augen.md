---
title: Die Welt aus anderen Augen
layout: post
lang: de
date: 2007-01-31T10:51:48+01:00
categories: [society]
tags: [old,OpenData,society]
comments: true
share: true
image:
  feature: 2007/174.png
  credit: Worldmapper
  creditlink: http://www.worldmapper.org/display.php?selected=174
---

Aus einer anderen Sicht bekommt die Welt ein ganz anderes "Gleichgewicht".  
Zum Beispiel durch die Darstellung der Verteilung der Armut:<!--more-->

[![Human Poverty](/images/2007/174.png)  
Worldmapper: The world as you've never seen it before - Human Poverty](http://www.worldmapper.org/display.php?selected=174)

> Poverty is not just a financial state. Being poor affects life in many ways. The human poverty index uses indicators that capture non-financial elements of poverty, such as life expectancy, adult literacy, water quality, and children that are underweight. The 30 territories of the Organisation for Economic Cooperation and Development use a different index which includes income and long-term unemployment; and not water quality or underweight children. This implies that the poor in richer territories are materially better off.

--- © Copyright Sasi Group (University of Sheffield) and Mark Newman (University of Michigan)
{: .small}

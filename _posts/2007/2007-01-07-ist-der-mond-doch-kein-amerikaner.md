---
title: Ist der Mond doch kein Amerikaner?
layout: post
lang: de
date: 2007-01-12T00:28:37+01:00
categories: [Kram]
tags: [old]
comments: true
share: true
---

[heise online - Der Mond ist metrisch](http://www.heise.de/newsticker/meldung/83571/from/rss09)

Amerika kann also doch nicht nur eigene Standards gegen schon bestehende erheben, sondern auch mal internationale wiederanerkennen. Immerhin, und weiter so! Lesenswert sind auch die Leserkommentare zu dem Heise-Artikel.

## update:

[Bush gesteht weitere Fehler im Irak ein \| tagesschau.de](http://www.tagesschau.de/aktuell/meldungen/0,,OID6298320_REF1,00.html)

Bitte noch mehr...

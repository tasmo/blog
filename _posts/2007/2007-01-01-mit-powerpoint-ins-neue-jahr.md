---
title: Mit PowerPoint in’s neue Jahr
layout: post
lang: de
date: 2007-01-01T16:50:43+01:00
categories: [tech]
tags: [old,Microsoft,Creative Commons,security]
comments: true
share: true
---

Allen ein gutes, neues Jahr!  
Und das beginnt gleich mit einer Warnung:  
[TP: "PowerPoint is evil!"](http://www.heise.de/tp/r4/artikel/24/24346/1.html)
<!--more-->

> Einer Schätzung des Herstellers zufolge sollen täglich dreißig Millionen PowerPoint-Präsentationen auf der ganzen Welt gehalten werden. Unvorstellbar! Im Jahr 2003 entbrannte allerdings, wohl aus finalem Verdruss an den vielen von rechts und links ins Bild rollenden Folien, an lustigen Wischeffekten, bunten Animationen, Charts und Bulletpoints, eine wütende Diskussion über die Gefährlichkeit dieser Art von Präsentation für den gesunden Menschenverstand.

Ich habe PowerPoint noch nie benutzt, aber oft genug im Einsatz gesehen. Vieleicht hat es ja bereits gewirkt...

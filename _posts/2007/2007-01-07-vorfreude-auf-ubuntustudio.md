---
title: Vorfreude auf Ubuntustudio
layout: post
lang: de
date: 2007-01-22T19:10:51+01:00
categories: [tech]
tags: [old,linux,audio,OpenSource,music]
comments: true
share: true
---

[heise online - Ubuntu strebt zum Multimedia-Studio auf](http://www.heise.de/newsticker/meldung/Ubuntu-strebt-zum-Multimedia-Studio-auf-137099.html) und im April soll es soweit sein. Ich kann es gar nicht erwarten!

![UbuntuStudio](/images/2007/g20700.png)  
Ubuntu Studio Logo via [ubuntu.com](https://wiki.ubuntu.com/UbuntuStudio/Artwork/OfficialHardy) [(CC BY-SA 3.0)](http://creativecommons.org/licenses/by-sa/3.0/)
{: .small}

Ich benutze immer mal wieder Linux und am liebsten Debian-Systeme, wie eben Ubuntu. Diese ist zwar als "Weicheierlinux" verschrien, aber ich mag es wegen seiner für Linux einfachen Bedienbarkeit, ist halt ein Konsumenten-Linux.  
Um daraus ein Musikstudio zu zaubern, ist es bisher noch notwendig, das System selbst anzupassen, wie [hier beschrieben](https://help.ubuntu.com/community/UbuntuStudioPreparation "UbuntuStudioPreparation").  
Genau das soll ab April anders werden.

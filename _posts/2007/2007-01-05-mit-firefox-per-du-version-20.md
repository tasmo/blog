---
title: Mit Firefox per Du - Version 2.0
layout: post
lang: de
date: 2007-01-05T20:12:04+01:00
categories: [tech]
tags: [old,OpenSource,howto,Microsoft]
comments: true
share: true
---

...ist soeben erschienen. Wer hier schonmal vorbeigeschaut hat, weiß, dass ich den Firefox ganz gern mag. Und auch deshalb sei zur Lektüre sehr empfohlen:

[Mit Firefox per Du - 11 Kapitel einer Browser-Freundschaft auf Windows von Ralph Segert](http://firefox-anleitung.net/index.html)
<!--more-->

> Die Anleitung Mit Firefox per Du - 11 Kapitel einer Browser-Freundschaft wendet sich an Einsteiger, die wenig Erfahrung mit Browsern ?? und dem Internet haben. Schritt für Schritt und in einfacher Sprache lernen Sie Firefox 2 kennen. Lesen Sie die 11 Kapitel am besten der Reihenfolge nach, sie bauen aufeinander auf! Ihre Fragen können Sie später gerne im Weblog stellen. Einige Kapitel enthalten zudem kurze Video-Anleitungen.

Die Anleitung richtet sich eher an Anfänger, ist übersichtlich strukturiert und sollte überzeugen, ein Microsoft-Produkt weniger zu benutzen.

Viel Spaß!

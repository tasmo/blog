---
title: Noche ‘ne Linux-Audio-Distribution
layout: post
lang: de
date: 2007-01-31T09:10:26+01:00
categories: [tech]
tags: [old,linux,audio,OpenSource,music]
comments: true
share: true
---

Ich mag SUSE nicht besonders, da es auf meinem nunmehr schon betagten PC nicht nur nicht flüssig sondern ohne Ende zäh läuft. Aber vieleicht ist das bei aktuellen Rechnern ja besser. ;)  
Aber diese openSUSE-Version empfehle ich trotzdem:

[JackLab Audio Distribution](http://www.jacklab.org/)

> A remastered openSUSE for musicians, producers and media creators

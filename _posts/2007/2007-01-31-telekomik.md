---
title: Telekomik
layout: post
lang: de
date: 2007-01-31T11:43:29+01:00
categories: [Kram]
tags: [old]
comments: true
share: true
---

[FTD - Krug entschuldigt sich bei T-Aktionären - IT+Telekommunikation](http://www.ftd.de/technik/it_telekommunikation/156710.html)

> Der Ex-"Tatort"-Kommissar Manfred Krug hat sich bei den Kleinanlegern der Telekom dafür entschuldigt, für die Aktie geworben zu haben. Er selbst halte seine eigenen Papiere noch heute als eine Art "Selbstbestrafung".

Was gibt's da noch zu sagen?

---
title: Freunde und Lizenzen
layout: post
lang: de
permalink: /385/freunde-und-lizenzen/
date: 2011-07-06
dsq_thread_id:
  - 350787868
categories:
  - social
tags:
  - Creative Commons
  - copyright
comments: true
share: true
---

## Hej Freunde,

ich finde es schön, dass Euch meine Fotos und meine Musik gefallen und ja, Ihr dürft sie auch gern benutzen.  
Nur <del datetime="2011-07-06T08:56:36+00:00">fragt Ihr mich nicht</del> <ins datetime="2011-07-06T08:56:36+00:00">fragt Ihr nicht alle</ins>, unter welchen Bedingungen und/oder wofür ich das möchte. Ich setze mich für freie Inhalte ein, das verschafft vielen unkompliziert den Zugang dazu und dient der Verbreitung. Das heißt aber noch lange nicht, dass Inhalte und Kreativität zum Freiwild deklariert werden.

Auch wenn ein Urheber seine Ergüsse zur Verfügung stellt, bleibt er der Urheber. Ihn mit Füßen zu treten und zu missachten heißt, ihn nicht nur nicht zu fördern (also schmarotzen) sondern die weitere Kreativität zu behindern.

Einen Künstler, den Ihr mögt, solltet Ihr unterstützen und nicht unterdrücken. Zerstört nicht den Boden, der Euch nährt!

<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br />Diese(s) Werk bzw. Inhalt von Thomas Friese steht unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Namensnennung-Weitergabe unter gleichen Bedingungen 3.0 Unported Lizenz</a>.

---
title: onemorebreak
layout: post
lang: de
permalink: /476/onemorebreak/
date: 2011-11-22
dsq_thread_id: 480312987
categories: music
tags:
  - cover
  - Tasmo
  - remix
  - selfmade
  - Tom Freeze
comments: true
share: true
image:
  feature: 2011/11/scrat.jpg
  credit: Thomas Friese
  creditlink: https://soundcloud.com/tasmo/onemorebreak
---

Ich mag diese synthetischen Computerstimmen sehr. Und ich mag [Kontraste][1].

Dazu gehört der Kontrast zwischen **<span style="color: #ff00ff;">Kitsch</span>** und **Kunst**. Von letzterer ist hier wenig vorhanden und doch mag ich, wie übertrieben poppig schon das [Original][2] (hint: *achtet auf das Cover-Bild*) und dann auch noch mein Cover daherkommt – selbst mir ist es nicht nur angenehm. :)

Irgendwann zwischen 2001 und 2003 saß ich an einem Soundtrack für ein Theaterstück und die Ideen kamen nur sehr zäh in meinen Sinn. [Travis][3] waren zu dieser Zeit mit Ihrem Cover genau dieses Songs in aller Ohren und auch in meinen. And I liked it. Es schien mir zwar billig aber recht, [auch eine Version](https://soundcloud.com/tasmo/onemorebreak) zu machen.

Et voilà!

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/28580280&amp;color=ffbbbb&amp;auto_play=false&amp;show_artwork=true"></iframe>

----

Music & Lyrics by Max Martin
{: .small}

[1]: http://kontraste.tasmo.de/ "Kontraste"
[2]: http://www.amazon.de/Baby-More-Time-Britney-Spears/dp/B00000G1IL "Baby one more time"
[3]: http://www.youtube.com/watch?v=acULghgYUg0 "Travis on Youtube"

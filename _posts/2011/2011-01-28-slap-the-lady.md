---
title: Slap the Lady!
layout: post
permalink: /292/slap-the-lady/
date: 2011-01-28
dsq_thread_id:
  - 217784525
categories:
  - finding
tags:
  - cover
  - art
comments: true
share: true
---

[PMS 562 Lady](http://print-process.com/product/?product-id=412) <small>by [David Marsh](http://print-process.com/Artist/David_Marsh/)</small>

![PMS 562 Lady](/images/2011/01/412.png)

> 'PANTONE solid uncoated' is a series of iconic masterpieces reinterpreted using PANTONE swatch icons from Adobe Illustrator.

## Das kenn ich doch, das kenn ich doch!
<!--more-->

[Chumbawamba -- Slap!  
![Chumbawamba -- Slap!](/images/2011/01/Chumbawamba_-_Slap.jpg)](https://secure.wikimedia.org/wikipedia/en/wiki/Slap!)

[(click for the video)](http://www.youtube.com/watch?v=oPwljXHWZw4&#038;fs=1)

---
title: Jobs glitched
layout: post
permalink: /331/jobs-glitched/
date: 2011-02-09
dsq_thread_id:
  - 226065323
categories:
  - finding
tags:
  - glitch
  - screenshot
comments: true
share: true
image:
  feature: 2011/02/jobs.jpg
---

This glitchy VHS picture is the last frame from the video showing Steve Jobs preparing for his first TV appearance and found in the fantastic [Progolog](http://www.progolog.de/2011/02/whats-left-is-in-this-room-09-02/).<!--more-->

[![Jobs glitched](/images/2011/02/jobs.jpg)](/images/2011/02/jobs.jpg)

[Youtube-Video angucken](http://www.youtube.com/watch?v=FzDBiUemCSY&#038;fs=1)

*[VHS]: Video Home System
*[TV]: Tele Vision

---
title: Bathroom Rebels – Color my Deck
layout: post
permalink: /457/bathroom-rebels/
date: 2011-11-04
dsq_thread_id:
  - 461776011
categories:
  - music
tags:
  - DJ-Set
  - music
  - selfmade
  - vinyl
  - Tasmo
comments: true
share: true
---

**Simone Fachel** invites artists to her photo projekt **[squatter my bath][1]** to occupy, transforn or just use her bathroom.

[![DJ on toilet](/images/2011/11/toilet_s.jpg)](/images/2011/11/toilet.jpg)  
In November 2010 she invited us **Bathroom Rebels** [Marinelli][2] und [_Tasmo][3] to [present our colored vinyl][4].

It was a session sitting on the toilet playing all the hoped-forgotten records more or less well mixed. ;) We had joy we had fun!!!

<iframe style="overflow:hidden;border:none;width:100%;height:145px;" id="hearthis_at_track_115257" src="https://hearthis.at/embed/115257/transparent/?hcolor=&color=&style=1&block_size=2&block_space=1&background=0&waveform=0&cover=1"></iframe>

[1]: http://squattermybath.blogspot.com/2010/08/squatter-my-bath.html "squatter my bath"
[2]: http://soundcloud.com/marinelli "Marinelli"
[3]: http://hearthis.at/tasmo "_Tasmo"
[4]: http://squattermybath.blogspot.com/2011/08/smb-21-thomas-friese-walter-marinelli.html "smb #21: thomas friese / walter marinelli - colour my bath"

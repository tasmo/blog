---
title: Dubstep Symphony
layout: post
lang: de
permalink: /417/dubstep-symphony/
date: 2011-07-26
dsq_thread_id:
  - 368834975
categories:
  - findings
tags:
  - free download
  - music
comments: true
share: true
---

[![BBC Philharmonic Presents...Nero's Dubstep Symphony](/images/2011/07/nero.jpg)](http://www.youtube.com/watch?v=5JAxgFe4hpE&#038;fs=1)

Bei aller Kritik:

## Klassische Musik zu hören ist in allererster Linie ein emotionales Ereignis.

Ebenso verhält es sich mit dem Konsum von zeitgenössischer Club-Musik, in diesem Fall: **Dubstep**.  
Beides zusammen bringt mir den Genuss aus beiden Welten und ist hier sehr gelungen. Ich möchte mich nicht weiter über den kulturphilosophischen Ansatz “Darf man das?” auslassen noch den Geschmack als Streitobjekt hervorholen. Kurzum: Man muss es nicht mögen.

Bitte mehr!<!--more-->

> In June 2011, pioneering dubstep/drum and bass producers, Nero, teamed up with the world-famous BBC Philharmonic. The result is this unique blend between two music genres. Orchestrated and conducted by [Joe Duddell](http://joeduddell.co.uk/). Performed live at MediaCity in Salford.
> 
> Nero's symphony has been rebalanced to more closely fulfill the vision that the band had for the piece and to ensure that this pioneering mix of orchestra and dubstep is available in its full glory.
> 
> Hosted by BBC Radio 1 (The Zane Lowe Show) and BBC Radio 1Xtra (MistaJam).

_--- (c) BBC 2011 (MMXI)_{: .small}

Weitere Literatur: [Arfmann mischt Karajan auf](http://www.zeit.de/online/2005/36/karajan)

[Download MP3](http://ul.to/20edy0s1){: .download}

----

Angeregt und aufgeweckt durch [Blogrebellen Kreuzberg](http://blog.rebellen.info/2011/07/25/neros-dubstep-symphony) & [@Saetchmo](https://twitter.com/#!/saetchmo/status/95469465617174529).
{: .small}

Download via [Evil Concussion](http://soundcloud.com/evil_concussion/2011-06-06-neros-dubstep)
{: .small}

Foto taken from the [BBC](http://www.bbc.co.uk/programmes/galleries/p013vt4v)
{: .small}

---
title: '_Tasmo – #Tassebier “Mega Deluxe”'
layout: post
lang: de
permalink: /309/tassebier-mega-deluxe/
date: 2011-02-06
dsq_thread_id:
  - 223584822
categories:
  - music
tags:
  - DJ-Set
  - selfmade
  - Tom Freeze
  - Tassebier
  - Tasmo
  - c-base
comments: true
share: true
---

<iframe style="width:100%; height:360px; border:none;" src="//www.mixcloud.com/widget/iframe/?feed=http%3A%2F%2Fwww.mixcloud.com%2Ftasmo%2F_tasmo-tassebier-mega-deluxe%2F&mini=&stylecolor=&hide_artwork=&embed_type=widget_standard&embed_uuid=28903ad6-1a2c-4758-b19e-53ef09e02190&hide_tracklist=&hide_cover=1&autoplay="></iframe>

Mein DJ-Set zur [#Tassebier "Mega Deluxe" zur Transmediale in der c-base](http://blog.rebellen.info/2011/01/17/tassebier-zur-transmediale-in-der-c-base/) ist ein wenig Dubstep-lastiger geworden, als ich vor hatte. Schuld daran ist Beatport!

Mehr von diesen leckeren Sets gibt es auf [Mixcloud](http://www.mixcloud.com/search/?x=0&y=0&mixcloud_query=tassebier) und bei den [Blogrebellen](http://blog.rebellen.info/2011/02/08/blick-von-den-decks/).

---

update:

Danke auch für das schöne Kompliment von [[jo.han.nes]][1]!

[1]: http://www.progolog.de/2011/02/tasmos-view-from-the-decks-at-tassebier/

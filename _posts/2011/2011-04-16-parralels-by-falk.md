---
title: Parralels by fALk
layout: post
lang: de
permalink: /340/parralels-by-falk/
date: 2011-04-16
dsq_thread_id:
  - 281059381
categories:
  - finding
tags:
  - Tassebier
  - DJ-Set
  - visuals
  - Tasmo
comments: true
share: true
---

<a class="fancybox" href="#falk"><img src="/images/2011/04/fALk.jpg" alt="fALk" width="320" height="240" /></a>

Ich zitiere [fALks Blogeintrag](http://prototypen.com/beamaz/vjblog/parralels_tassebier_tasmo) bei den Prototypen:

> A recording of the Parrarels Live Cinema Movie – in collaboration with _Tasmo who mixed the fine music.  
> Towards the end I had a memory bug in VDMX and things almost came to a standstill and also I lost my MidiControler midway through the performance – still quite okish.

<div class="fancybox-hidden hidden"><object id="falk" width="538" height="345" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0">
  <param name="file" value="http://prototypen.com/sites/all/themes/proto-zen/protozenstlyepics/page_radio/mediaplayer.swf?0#0" />
  <param name="wmode" value="transparent" />
  <param name="flashvars" value="height=345&amp;width=538&amp;file=http://protobits.com/beamaz/vjblog/TasmoTassebierParallels_1k_320.mp4&amp;" />
  <param name="src" value="http://prototypen.com/sites/all/themes/proto-zen/protozenstlyepics/page_radio/mediaplayer.swf?0#0" />
  <embed width="538" height="345" type="application/x-shockwave-flash" src="http://prototypen.com/sites/all/themes/proto-zen/protozenstlyepics/page_radio/mediaplayer.swf?0#0" file="http://prototypen.com/sites/all/themes/proto-zen/protozenstlyepics/page_radio/mediaplayer.swf?0#0" wmode="transparent" flashvars="height=345&amp;width=538&amp;file=http://protobits.com/beamaz/vjblog/TasmoTassebierParallels_1k_320.mp4&amp;" /></object></div>

*[_Tasmo]: Author von tasmo.rocks

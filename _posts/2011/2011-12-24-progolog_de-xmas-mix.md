---
title: progolog_de xmas mix
layout: post
lang: de
permalink: /508/progolog_de-xmas-mix/
date: 2011-12-24
dsq_thread_id:
  - 514756433
categories:
  - music
tags:
  - DJ-Set
  - mashup
  - music
  - selfmade
  - Tasmo
comments: true
share: true
---

Der [Progolog Adventskalender][1] ist eine feine Sache. Umso mehr habe ich mich über Frage von [[jo.han.nes]][2] gefreut, ob ich ein Türchen davon füllen könnte. „Natürlich!“

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/31341599&amp;color=ee0000&amp;auto_play=false&amp;show_artwork=true"></iframe>

Erst wollte ich wahnsinnig originell sein, aber dann fehlte mir die Zeit dazu. ;)
<!--more-->

Aber da genügend Andere genügend originell sind, hatte ich es leicht, <cite title="Musikalisches Weihnachtsgebäck - New Urban Music Blog">„[einen XMas-Mix](https://soundcloud.com/tasmo/progolog_de-xmas-mix/download) mit vielen vornehmlich dubbigen Coverversionen bekannter Weihnachtsklassiker (‚Blitzkrieg Bop‘, ‚Foxy Lady‘)“</cite> (Zitat vom [numblog][3]) zusammenzustellen.

Hört selbst!

**Merry Xmas!**

[1]: http://www.progolog.de/tag/progolog-adventskalender-11
[2]: https://twitter.com/#!/cptpudding
[3]: http://www.numblog.de/archives/1062-Musikalisches-Weihnachtsgebaeck.html

---
title: 'Dubstation 9pm @ Фузион #15'
layout: post
permalink: /402/dubstation9pm/
date: 2011-07-11
dsq_thread_id:
  - 355131842
categories:
  - music
tags:
  - DJ-Set
  - Tasmo
  - Festival
comments: true
share: true
image:
  feature: 2011/07/5001730752_1f92e190f9_b.jpg
  credit: Thomas Friese
  creditlink: http://www.flickr.com/photos/tasmo/sets/72157624982203674/
---

My DJ set at the dubstation floor at 15th Fusion Festival 2011 9pm to noon – big thanx'n'hugs to Saetchmo for his great support!

<iframe style="overflow:hidden;border:none;width:100%;height:400px;" id="hearthis_at_track_115168" src="https://hearthis.at/embed/115168/transparent_black/?style=1&block_size=2&block_space=1&background=1&waveform=0&cover=1"></iframe>

[download](https://twitter.com/#!/LinkLuder/statuses/88893220184014848){: .download}

More Fusion mixes @[Blogrebellen](http://blog.rebellen.info/tag/fusion-festival/)

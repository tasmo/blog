---
title: Das Jahresendevorspiel von Hal Faber
layout: post
lang: de
date: 2009-01-02T10:56:28+01:00
categories: [Gedanken]
tags: [old,society]
comments: true
share: true
---

...ist mit vielen schönen Zitaten verziert:

> Aber darum muss man die Dummheit ja ausrotten, weil sie dumm macht, die ihr begegnen.
<!--more-->

> Denken ist etwas, das auf Schwierigkeiten folgt und dem Handeln vorausgeht.

> Ganz allgemein sollte gelten, dass jedes Land, in dem besondere Sittlichkeit nötig ist, schlecht verwaltet ist.

> Der Denkende benützt kein Licht zuviel, kein Stück Brot zuviel, keinen Gedanken zuviel.

> Denn es ist eine Kluft zwischen oben und unten, größer als  
> Zwischen dem Berg Himalaja und dem Meer  
> Und was oben vorgeht  
> Erfährt man unten nicht  
> Und nicht oben, was unten vorgeht  
> Und es sind zwei Sprachen oben und unten  
> Und zwei Maße zu messen  
> Und was Menschengesicht trägt  
> Kennt sich nicht mehr.

> Es ist ungemein wichtig, sich immer zu fragen, von was der abhängt, der vor einem fährt.

> Wenn die Irrtümer verbraucht sind  
> Sitzt als letzter Gesellschafter  
> Uns das Nichts gegenüber.  

> Die kleinen Verbrechter verletzten nur die Spielregeln der Selbstsüchtigen. Diese Spielregeln sind aber das Verdammungswürdigste.

> Wenn der Kapitän ein Genie sein muss, sind seine Instrumente wohl unzuverlässig.

> Der, den ich liebe  
> Hat mir gesagt  
> Dass er mich braucht  

> Darum  
> Gebe ich auf mich acht  
> Sehe auf meinen Weg und  
> Fürchte von jedem Regentropfen  
> Dass er mich erschlagen könnte  

Weiter: [Was war. Was wurde. Ein Jahresendevorspiel.](http://www.heise.de/newsticker/Was-war-Was-wurde-Ein-Jahresendevorspiel--/meldung/121031)

---
title: Fantomton – Radarstation
layout: post
permalink: /135/radarstation/
dsq_thread_id:
  - 176533205
categories:
  - music
tags:
  - Tasmo
  - music
  - selfmade
  - remix
comments: true
share: true
---

<iframe style="width:100%; height:450px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/14492"></iframe>

The result is the first release of Fantomton, made out mainly by these [Radarstation](http://www.fantomton.de/alben/Radarstation/radarstation.html) mysterious samples. Every artist had the possibility to make every kind of track: the border of this album is not the genre, but the source of the sounds.

Released by: **Fantomton**  
Release/catalogue number: **FT001**  
Release date: Apr 10, 2009

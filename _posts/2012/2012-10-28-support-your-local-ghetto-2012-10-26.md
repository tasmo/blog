---
title: Support Your Local Ghetto 2012-10-26
layout: post
permalink: /1877/support-your-local-ghetto-2012-10-26/
date: 2012-08-28
categories:
  - music
tags:
  - Berlin
  - DJ-Set
  - Tasmo
  - music
  - selfmade
  - Tom Freeze
comments: true
share: true
image:
  feature: 2012/10/ghetto.jpg
---

It was an honor to play at Panke for [Barb Nerdy][1]'s [Support Your Local Ghetto][2]. We had a nice audience and a smooth and warm evening in atumn.

<iframe style="width:100%; height:120px; border:none;" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&light=1&feed=%2Ftasmo%2Ftom-freeze-support-your-local-ghetto-2012-10-26%2F"></iframe>
<!--more-->

direct play & track list @ [Mixcloud] or download as [MP3](http://dl.dropbox.com/u/618500/SupportYourLocalGhetto.mp3){: .download}
{: .small}

[1]: http://twitter.com/BarbNerdy
[2]: https://www.facebook.com/events/474461705917931/
[Mixcloud]: http://www.mixcloud.com/tasmo/tom-freeze-support-your-local-ghetto-2012-10-26/

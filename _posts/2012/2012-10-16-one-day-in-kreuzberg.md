---
title: one day in Kreuzberg
layout: post
permalink: /1863/one-day-in-kreuzberg/
date: 2012-10-16
categories:
  - Foto
tags:
  - Berlin
  - Kreuzberg
  - Fotos
  - selfmade
comments: true
share: true
image:
  feature: 2012/10/PA150330.jpg
  credit: Thomas Friese
  creditlink: http://www.flickr.com/photos/tasmo/sets/72157631782814543/
---

I got visitors from far away. That is the opportunity to look around in the own neighborhood with different and relaxed eyes. The autumn in Kreuzberg is just beautiful and so we did a day trip through Kreuzberg 36 – with our cameras.
<!--more-->

[![one day in Kreuzberg](/images/2012/8093480627_28159aeae0_b.jpg)](https://www.flickr.com/photos/tasmo/albums/72157631782814543)

---
title: Dilettant Habits two
layout: post
permalink: /1029/dilettant-habits-two/
date: 2012-04-23
dsq_thread_id:
  - 660911394
categories:
  - music
tags:
  - DJ-Set
  - Tasmo
  - music
  - selfmade
  - tape
  - Tom Freeze
  - vinyl
comments: true
share: true
image:
  feature: 2012/04/tape.jpg
---

In 1997/98 I began as a vinyl mixing player (aka DJ) to work officially in some bars and clubs. And I thought: The first thing you need is a unique name. And I had no idea! So I played around with my first name and chosed an anagram – "Tasmo" was born.

As you can hear on the following tape, my technical skills have not been very professional. A question mark after the two critical letters "DJ" should compensate for this and make that clear. So I became my alter ego "**DJ? Tasmo**".

The second thing you need as a becoming DJ are application tapes. The cassette "<a class="fancybox" href="#dilettant2">Dilettante Habits two</a>" shows some of my musical tastes at the late 90s. The tools I've used are very traditional, one 1210, a cheep ["Dual" player](http://de.wikipedia.org/w/index.php?title=Datei:Plattenspieler350.jpg&filetimestamp=20070421004530), a two-channel mixer and a couple of vinyls.
<!--more-->

<iframe id="dilettant2" style="width:100%; height:360px; border:none;" src="//www.mixcloud.com/widget/iframe/?feed=http%3A%2F%2Fwww.mixcloud.com%2Ftasmo%2Fdj-tasmo-dilettant-habits-two-tt04%2F&mini=&stylecolor=&hide_artwork=&embed_type=widget_standard&embed_uuid=f8cd7995-7a98-4186-ad0f-68d70b7c3c0a&hide_tracklist=&hide_cover=&autoplay="></iframe>

Dilettante mixed vinyl on tape from 1998  
Released by: Tasmo Tapes 2000  
Release/catalogue number: TT04

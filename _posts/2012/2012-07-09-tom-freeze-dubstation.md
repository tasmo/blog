---
title: Tom Freeze @ Dubstation (Фузион 2012)
layout: post
permalink: /tom-freeze-dubstation/
redirect_from:
  - /2012/07/tom-freeze-dubstation/index.html
  - /1288/tom-freeze-dubstation/index.html
date: 2012-07-09
modified: 2014-07-02 19:14
dsq_thread_id:
  - 757671937
categories:
  - music
tags:
  - DJ-Set
  - Tom Freeze
  - Tasmo
  - Festival
comments: true
share: true
image:
  feature: 2012/07/P7020265.jpg
  credit: Thomas Friese
  creditlink: http://www.flickr.com/photos/tasmo/sets/72157630399946956/
---

Many thanks to Saetchmo for the great support, Robot Koch &amp; Graciela Maria for their exclusive track(!) and the Dubstation and Fusion teams!

<iframe style="width:100%; height:120px; border:none;" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&light=1&feed=%2Ftasmo%2Ftom-freeze-dubstation-2012%2F"></iframe>

Track list:<!--more-->

1.  Space Intro – *Steve Miller Band*
2.  Fly Like An Eagle (Pretty Lights Remix) – *Steve Miller Band*
3.  Dungeon Sound – *Gramatik*
4.  Birdshell (6th Borough Project Shell Toe Remix) – *Craig Bratley*
5.  Belgrade Riddim (Hobo Kings Remix) – *Sub Swara*
6.  Dem Can't Stop We From Talk *ft. Devon D* (Dubblestandart's Kingston Riot Riddim Dub) – *Subatomic Sound System*
7.  Bombaclaad Star – *Liquid Stranger*
8.  Earthquake Vox Stems No Rap *ft. Tinie Tempah* – *Labrinth*
9.  Ten Feet Tall – *Jstar*
10. Blockhouse – *Jahfayah*
11. Summertime – *Sam Cooke*
12. Rather Go Blind *ft. Graciela Maria* – *Robot Koch*
13. Somebody That I Used To Know (Enigma Dubz Mix Master) – *Gotye*
14. The Ganja Games – *Upgrade*
15. Barefoot Dub (Grodio Remix) – *Jahcoozi*
16. Fever *ft. Nini Martini* – *Grodio*
17. Valley Of The Shadows (The Living Graham Bond Remix) – Origin Unknown
18. Brainwashing (Dubstep Remix) – *Bob Marley*
19. Bad Baws – *Savant*

([uploaded to mixcloud](http://i.mixcloud.com/CB7syq) / [download](https://www.dropbox.com/s/d9mmws2urxvigu3/Tom%20Freeze%20-%20Fusion%202012%20Dubstation.mp3){: .download})
{: .small}

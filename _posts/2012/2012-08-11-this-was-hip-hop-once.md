---
title: This was Hip Hop once
layout: post
lang: de
permalink: /1369/this-was-hip-hop-once/
date: 2012-08-11
dsq_thread_id:
  - 801274246
categories:
  - music
tags:
  - DJ-Set
  - Tasmo
  - Hip Hop
  - tape
comments: true
share: true
image:
  feature: 2012/08/tt05-e1359207857800.jpg
  credit: tasmo
---

Damals, als wir Home DJs noch Mischkassetten machten, um die holde Weiblichkeit zu beeindrucken, klang Hip Hop so. Und heute stellen wir die alten Mischkassetten neu in die digitale Wolke, um wieder die holde Weiblichkeit zu beeindrucken – und nicht nur die.

<iframe id="hearthis_at_track_770268" style="width:100%;height:166px;overflow:hidden;border-style:none;background:transparent;" src="https://hearthis.at/embed/770268/transparent/?hcolor=&color=&style=2&block_size=1&block_space=0&background=0&waveform=0&cover=0&autoplay=0&css="></iframe>
Listen to [DJ? Tasmo - Dilettant Habits one](https://hearthis.at/tasmo/dilettant-habits-one/).
{: .small}

Mit Musik von:<!--more-->

* Funkdoobiest
* DJ Food
* Howie B.
* Kool Keith
* DJ Krush
* Hurricane
* House Of Pain
* A Tribe Called Quest
* Soul Assassins
* Glamorous Hooligan
* Sinister 6000
* The Herbalizer
* Die Fantastischen Vier
* B-Real, Busta Rhymes, Coolio, LL Cool J, and Method Man
* Beastie Boys
* Delinquent Habits
* Beck
* MC Solar
* Portishead
* Andrea Parker
* La Funk Mob

[Hier aus der Wolke herausladen](https://hearthis.at/tasmo/dilettant-habits-one/download/){: .download}
{: .small}

---
title: Mir haddn dor nüschd!
layout: post
lang: de
permalink: /1446/mir-haddn-dor-nuschd/
date: 2012-08-20
modified: 2014-06-24T15:07:07+01:00
dsq_thread_id:
  - 812388573
categories:
  - findings
tags:
  - Hip Hop
  - Video
comments: true
share: true
image:
  feature: 2012/08/Here_We_Come.jpg
  credit: Kinokalender Dresden
  creditlink: http://kinokalender.com/va.php?va=4690
---

"Here We Come" ist eine nicht mehr ganz so junge aber weiter [sehenswerte Retrospektive][1] an eine noch vergangenere Jugendszene. Die Protagonisten in dieser Homage lebten und setzten sich seinerzeit mit einer Kultur auseinander, die sie größtenteils nur von Ferne kannten und sehr bewunderten.

Am vergangenen Wochenende traf ich ganz privat die <abbr title="EST">East Side Rebels</abbr>. Von ihnen wurde ich wieder einmal auf großartigste Weise an meine Vergangenheit erinnert. An die Zeit und den Ort, wo wir nichts hatten. Dieses “Nichts” musste “im Osten” für diverse Rechtfertigungen, dass wir dieses oder jenes nicht machen konnten und deshalb dem Zeitgeist -- zum Teil immer noch -- hinterher hinken, hinhalten. Einige haben sich aber mitnichten von “nichts” abbringen lassen, dem System zu trotzen. Wir sprachen über die guten, alten Zeiten in denen wir eben nichts hatten und diesen tollen Film über Hip Hop in der DDR.

Dieser gelungene Zusammenschnitt aus dem Bonus-Material illustriert ganz wunderbar, was wir damals hatten. Here We Come:

[Wir hatten ja nichts
![Here We Come](/images/2012/08/Here_We_Come1.jpg)](http://www.youtube.com/watch?v=tykrjt71wJQ)

`<update>`

[Ronny] hat die Doku in voller Länge gefunden:  
[Here we come - Breakdance in der DDR (Dokumentarfilm)](https://www.youtube.com/watch?v=QRII3bBEv6o)

[1]: http://www.herewecome.de/ "Here We Come"
[Ronny]: http://www.kraftfuttermischwerk.de/blogg/doku-here-we-come-breakdance-in-der-ddr/

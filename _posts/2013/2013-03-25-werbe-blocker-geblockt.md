---
title: 'Android: Werbe-Blocker geblockt'
layout: post
lang: de
permalink: /3060/werbe-blocker-geblockt/
categories:
  - tech
tags:
  - ads
  - OpenSource
  - Android
  - howto
comments: true
share: true
image:
  feature: 2013/03/blocked.jpg
---

Viele Angebote im Internet sind zwar kostenlos für den Benutzer, werden aber über Werbung finanziert. So bezahlen Nutzerinnen in den Fällen nicht mit Geld sondern mit ihrer nicht weniger kostbaren Aufmerksamkeit.

# Achtung, Werbung! {#achtungwerbung}

Mich persönlich stört das Flimmern der kleinen, wechselhaft eingeblendeten Bildchen sehr --- sowohl auf Webseiten als auch in Apps. Die oft bunt flimmernden Anzeigen stören meine Konzentration auf den eigentlichen Inhalt oder zerstören die Ästhetik der manchmal doch ganz ansprechend gestalteten Webseite oder des GUI der App. Zudem möchte ich keinem Ad-Provider mein Surf-Profil zur freien Verfügung stellen.

Ob Werbung unterdrückt werden sollte oder nicht, lässt sich [diskutieren][1]. Was ich nicht möchte, ist die Entwicklerinnen um ihren angesetzten Lohn bringen. Nur mit der Art und Weise bin oft nicht einverstanden. Das geht offensichtlich nicht nur mir so.
<!--more-->

Es gibt zum Glück auch [löbliche][2] [Ausnahmen][3]! In meinem Webbrowser kommen diese [Positiv][4]-[Beispiele][5] auf meine White-List.

## Unterdrückung {#unterdrueckung}

Zum Browsen durch das WWW existieren viele Möglichkeiten. Angefangen von dem im lokalen Netzwerk laufenden Proxy-Server, Browser-Erweiterungen bis zu kleinen Hilfsprogrammen, die die lokale hosts-Datei anpassen. Der Effekt ist bei den drei Möglichkeiten der gleiche: Die Werbung wird vor der Auslieferung an die Anzeige abgefangen und kann so nicht mehr angezeigt werden.

Im März 2013 wurden zahlreiche Programme, die Werbung auf einem Android-System unterdrücken können, wegen des [Verstoßes gegen die Google Play-Programmrichtlinien für Entwickler][6] aus dem [Store verbannt][7]. Das betrifft unter anderen Ad Blocker Root (auch Teil der Rom Toolbox), AdAway, AdBlock Plus und AdFree.

Bereits installierte Apps bleiben von Google unangetastet, werden also nicht automatisch gelöscht, bekommen aber auch keine Updates mehr über den Play-Store.

## Wie installiere ich einen Ad-Blocker auf meinem Android? {#ad-blocker}

Diese Apps lassen sich aber leicht ohne Googles Play Store installieren. Vorraussetzung für die meisten Werbeunterdrücker ist ein "[gerootetes][8]" System. Dadurch werden einzelnen Programmen Rechte gegeben, Sytem-kritische Dateien verändern zu können. In den Einstellungen muss auch das Installieren von nicht signierten Programmen Dritter erlaubt werden.

Um eine der Apps zu bekommen gibt es den Weg des manuellen Herunterladens oder den bequemeren über einen alternativen App-Store. Meine Empfehlung dafür (und auch [die der fsfe][9]) ist, sich den Open-Source-Store [F-Droid][10] zu installieren. Damit gibt es über die Suche nach "Ad" eine kleine Auswahl an Ad-Blockern, es können einfach Aktualisierungen verwaltet werden und noch mehr freie Tools, Software und Spiele installiert werden.

Die Apps können aber auch direkt von den meisten Entwicklerinnen geladen werden. Das populärste Programm dabei ist [AdBlock Plus][11]. Mit Bekanntgabe des Rauswurfs wurde es sogar [noch beliebter][12].

Direkt-Download-Links (Auswahl):

* [Ad Blocker Root][13] (from Rom Toolbox)
* [AdAway][14]
* [AdBlock Plus][15]
* [AdFree][16]

## selbst gemacht {#selbst-gemacht}

Ein dritter Weg für Vortgeschrittene ist, die Text-Datei `/etc/hosts` zu editieren.

> [@_Tasmo](https://twitter.com/_Tasmo) Oder man verzichtet auf Apps und editiert einfach seine hosts Datei ;)
>
> --- julian (@vivaeltopo) [March 25, 2013](https://twitter.com/vivaeltopo/statuses/316147353566457856)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Eine gute Vorlage ist unter <http://someonewhocares.org/hosts/> zu finden.

## Update:

Heise hat eine [gute Zusammenfassung][17] über den Rausschmiss aus dem Play Store.

----
Mehr zum Thema: [Android abhärten][18]

[1]: http://www.dradio.de/dlf/sendungen/computer/2051129/
[2]: http://www.kraftfuttermischwerk.de/blogg/
[3]: http://www.heise.de/
[4]: http://re-publica.de/
[5]: https://netzpolitik.org/
[6]: http://play.google.com/about/developer-content-policy.html
[7]: http://pastebin.com/HDf5Pqw5
[8]: http://www.androidpit.de/de/android/wiki/view/Root
[9]: https://fsfe.org/campaigns/android/liberate.de.html "Befreien Sie Ihr Android"
[10]: https://f-droid.org/
[11]: http://adblockplus.org/blog/adblock-plus-for-android-removed-from-google-play-store
[12]: http://www.pcgameshardware.de/Panorama-Thema-233992/News/Adblock-Plus-mit-Nutzerrekord-1061636/
[13]: https://www.facebook.com/photo.php?fbid=517217961658280&set=a.336261666420578.76213.336227679757310&type=1
[14]: https://code.google.com/p/ad-away/
[15]: http://adblockplus.org/de/android
[16]: http://www.eapktop.com/tag/adfree
[17]: http://www.heise.de/newsticker/meldung/Google-wirft-Werbeblocker-aus-dem-Android-Play-Store-1822706.html "Google wirft Werbeblocker aus dem Android Play Store"
[18]: /3364/android-abhaerten/ "Android abhärten"

*[WWW]: World Wide Web

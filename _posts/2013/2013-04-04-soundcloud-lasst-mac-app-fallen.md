---
title: Soundcloud lässt Mac-App fallen
layout: post
lang: de
permalink: /3125/soundcloud-lasst-mac-app-fallen/
categories:
  - tech
tags:
  - macosx
image:
  feature: 2013/04/drop.jpg
comments: true
share: true
---

Zum Download der App bitte [ganz nach unten](/3125/soundcloud-lasst-mac-app-fallen/#soundcloud_app_download).
{: .small}

## Was bisher geschah

Am 1. Januar 2011 [kündigte][1] Soundcloud eine eigene, native Desktop-App für den Mac an. Mit diesem Programm <del datetime="2013-04-04T13:40:19+00:00">ist</del><ins datetime="2013-04-04T13:40:19+00:00">war</ins> es komfortabel und vor allem **viel übersichtlicher** als auf der [Webseite][2] möglich, den eigenen Stream zu hören, Favoriten zu verwalten und sogar das zuletzt Gesuchte zu sichten. Die App lief nativ auf Mac OS X und war somit schnell und belastete auch den Speicher von älteren Geräten nur wenig.

Seit Februar 2013 ist die App aus dem Mac App Store verschwunden. Wo sie installiert ist, läuft sie zwar noch, liefert aber keinen Sound mehr.
<!--more-->

> [@murdelta](https://twitter.com/murdelta) Sorry to be the bearer of bad news, but we don't support the Mac app anymore. We're focusing more on the web experience. /Annelise
>
> --- SoundCloud Support (@SCsupport) [April 3, 2013](https://twitter.com/SCsupport/statuses/319581844091846656)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

## Der [Drop][3] {#derdrop}

In einem [undatierten Update erklärt das Team][1] seinen Rückzug:

> *Please note*: Due to resources, maintaining the desktop app became a bit difficult, so it has now been discontinued. We’re currently focusing energy on making the iOS app and web experience as good as they can be. 

Stattdessen empfiehlt Soundcloud nun ein [OpenSource-Programm][4], das lediglich die Webseite im HTML-Frame ohne Browser öffnet. Den einzigen Mehrwert, den ich da finden kann, ist die Steuerung mit den Media-Keys. Die gibt es allerdings auch für [Chrome][5] und [Firefox unter Linux][6].

Außer dieser spärlichen Ergänzung gibt es bisher kein offizielles Statement von Soundcloud — weder im [Blog][7], [Tumblr][8], noch im [Hilfe-System][9] ist ein Hinweis zu finden.

## Und warum? {#undwarum}

[Der Entwickler][10] dieses wunderbaren Programms ist seit Februar nicht mehr bei Soundcloud. Im Zuge des Redisigns der gesamten Plattform — [das nicht jeder gut findet][11] — ist vermutlich auch eine Umstellung/Pflege der API notwendig. Wie oben zu sehen ist, scheint der Support der Mac-Community auf diesem Wege für Soundcloud nicht mehr so wichtig.

Das ist ein Schritt, den ich sehr bedaure und mich an andere "soziale" Services wie [Twitter][12] erinnert.

Soundcloud, mach bitte nicht weiter so!

### Update:

Bin gespannt, worauf wir warten sollen…

> [@_Tasmo](https://twitter.com/_Tasmo) Hey Tom, we're working on this – hold tight! /Luke
>
> --- SoundCloud Support (@SCsupport) [April 4, 2013](https://twitter.com/SCsupport/statuses/319803052980858880)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Ansonsten kann Soundcloud auch witzig: <a id="dropometer" href="https://blog.soundcloud.com/2013/04/01/dropometer/" title="The Dropometer">The Dropometer</a>

## BIG News {#bignews}

"Unsere Freunde von Soundcloud" haben heute einen Newsletter verschickt, indem sie einen Link zum wieder (Hurra!) funktionierenden Update senden:

> We recently made some changes which caused the app to stop working, for which we apologize. However, we've updated the app so that it's functional again, and you can download it for free.  
> We're currently focusing our energies on making the web experience, and Android and iOS apps, as good as they can be. Because of this, we're not able to offer support for the Mac desktop app. It will no longer appear in the Mac app store, and we're unable to fix any issues that may occur in the future with the app.  
> We hope you'll enjoy the fixed Mac app, and explore the new features we're developing on web and mobile.

<del datetime="2013-04-17T20:12:50+00:00">Wenn Soundcloud den Link veröffentlicht, werde ich ihn hier selbstverständlich ergänzen.</del>

Wieder da: die Soundcloud-App für den Mac. Der SoundCloud Support hat in einem Service-Tweet den Link veröffentlicht.

> [@murdelta](https://twitter.com/murdelta) You can also download a fix for the Mac app here: <http://t.co/2EPGHve71b>. More info here: <http://t.co/ZqQsgh8fWx>. /Luke
>
> --- SoundCloud Support (@SCsupport) [April 17, 2013](https://twitter.com/SCsupport/statuses/324520937733099521)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<span id="soundcloud_app_download">TL;DR</span>: Die App gibts unter [snd.sc/mac_app][13].

Danke an [@blog_bleistift][14], [@cptpudding][15] und [@murdelta][16] für die Hinweise!

[1]: https://blog.soundcloud.com/2011/01/06/desktop/
[2]: http://soundcloud.com/
[3]: #dropometer
[4]: http://salomvary.github.com/soundcleod/
[5]: https://chrome.google.com/webstore/detail/swayfm-unified-music-medi/icckhjgjjompfgoiidainoapgjepncej
[6]: https://github.com/paulrouget/htmlmediakeys
[7]: https://blog.soundcloud.com/?s=mac
[8]: http://soundcloud.tumblr.com/search/mac
[9]: http://help.soundcloud.com/customer/portal/articles/search?q=mac
[10]: http://robb.is/leaving-soundcloud/
[11]: http://de-bug.de/medien/archives/das-neue-soundcloud.html#comments
[12]: http://www.heise.de/newsticker/meldung/Twitter-Aus-fuer-Mobil-und-Air-Anwendungen-von-Tweetdeck-1816048.html
[13]: http://snd.sc/mac_app
[14]: https://twitter.com/intent/user?screen_name=blog_bleistift
[15]: https://twitter.com/intent/user?screen_name=cptpudding
[16]: https://twitter.com/intent/user?screen_name=murdelta

*[heute]: 17. April 2013

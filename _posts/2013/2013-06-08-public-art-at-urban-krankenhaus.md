---
title: public art at Urban Krankenhaus
layout: post
lang: de
permalink: /3363/public-art-at-urban-krankenhaus/
categories:
  - Foto
tags:
  - Fotos
format: image
comments: true
share: true
---

![public art at Urban Krankenhaus][1]  
[public art at Urban Krankenhaus][2]

Wer eine Weile am [Urbanufer][3] auf der Wiese sitzt, entspannt, dabei in der Sonne schwitzt und trinkt, muss zweifellos auch irgendwann das Örtchen aufsuchen. Mein Tipp – und auch der vieler andere – ist, einfach am netten Pförtner des *Vivantes Klinikum am Urban* freundlich zulächelnd vorbei zu gehen und direkt im Erdgeschoss fündig werden.

Beim letzten Gang dorthin ist mir beim Eintreten mit geradeaus gerichtetem Blick ein Stück dekorativer Kunst aufgefallen, die ich da gar nicht vermutet habe. Den Titel und Künstler des Werkes werde ich nachreichen, sobald er mir bekannt ist. Es ist eines der vielen, nicht mal kleinen, sehenswerten Dinge, die meist unbeachtet an mir vorübergehen. Dieses nicht!

[1]: /images/2013/06/8988985024_55d2c48bb9_c.jpg
[2]: http://www.flickr.com/photos/tasmo/8988985024
[3]: https://de.foursquare.com/v/urbanufer/500f05ece4b05c6252d29e23

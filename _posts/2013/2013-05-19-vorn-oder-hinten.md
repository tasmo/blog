---
title: Vorn oder hinten?
layout: post
lang: de
permalink: /3306/vorn-oder-hinten/
categories:
  - Kram
tags:
  - richtig wichtig
  - howto
image:
  feature: 2013/05/3242592265_76223089a2_o.jpg
  credit: doug3465
  creditlink: http://www.reddit.com/r/funny/comments/gv5uh/over_or_under/
comments: true
share: true
---

Während die neue Klotür-Kriegsbemahlung in der [c-base][1] nur noch mäßiges Echo bekommt

> Update: Die [#klotür](https://twitter.com/search?q=%23klot%C3%BCr&amp;src=hash) der [#cbase](https://twitter.com/search?q=%23cbase&amp;src=hash) ist zurück <http://t.co/ZA2PDejsCA>
>
> --- antiblog (@anti_blog) [May 15, 2013](https://twitter.com/anti_blog/statuses/334744051859456000)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

> Also von der Rückkehr der [@klotuer](https://twitter.com/klotuer) hätte ich jetzt etwas mehr Resonanz in meiner Timeline erwartet.
> 
> --- @plomlompom (@plomlompom) [May 16, 2013](https://twitter.com/plomlompom/statuses/335002919252398080)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

und sich DRadio Wissen der [Wichtigkeit von Aborten in Computerspielen][2] befasst, war ich [in Madrid][3] und sah mich mit der Frage konfontiert: Bin ich eigentlich zu kleinlich oder die anderen zu unachtsam? Doch die wirkliche Frage ist eine andere:

# Wie herum gehört das Toilettenpapier?
<!--more-->

> Klopapierverkehrtherumaufhänger vs. Klopapierverkehrtherumaufhängerbemerker
> 
> --- (_)Tasmo (@_Tasmo) [May 10, 2013](https://twitter.com/_Tasmo/statuses/332940670396416003)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Und damit bin ich nicht allein:

> Wer Toilettenpapier falsch gerum aufhängt, quält bestimmt auch kleine Tiere...
> 
> --- benson (@benjaminsohn) [May 15, 2013](https://twitter.com/benjaminsohn/statuses/334548154500411392)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Aber was ist falsch herum?

Ein früher Post hat es sich dabei sehr einfach gemacht: "[Toilettenpapier: Rüber, nicht drunter.][4]" Ist das wirklich richtig?

> Dieses Thema scheint wirklich viele Menschen zu bewegen...
> 
> --- Herr Schwarz (@phrobion) [May 15, 2013](https://twitter.com/phrobion/statuses/334608059844403200)
{: .twitter-tweet lang="en"}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Aufklährung gibt es bei [Basic thinking][5]. Die englische Wilipedia hält sogar [handfeste Lösungen][6] bereit. Beide kommen zu dem Schluss, dass beide Art und Weisen, Toilettenpapier aufzuhängen, Vor- und Nachteile haben.

![Over or Under?](/images/2013/05/Gky7j.jpg)
Foto: [jmawork](http://www.flickr.com/photos/illuminated_photography/3242592265/)
{: .small}

## TL;DR

Für mich heißt das, es gibt kein Richtig und kein Falsch und somit auch keinen Grund zum Berichtigen oder sogar Ärgern. Dann spare ich mir diese Energie und werde an anderer Stelle umso überzeugender pingelig sein können.

[1]: http://c-base.org/
[2]: http://wissen.dradio.de/aborte-computerspieltoiletten.85.de.html?dram:article_id=244208 "Aborte: Computerspieltoiletten"
[3]: http://www.flickr.com/photos/tasmo/sets/72157633520291939/show/ "Madrid - Smartphone"
[4]: http://www.whudat.de/toilettenpapier-uber-nicht-drunter/ "Toilettenpapier: Rüber, nicht drunter."
[5]: http://www.basicthinking.de/blog/2011/04/28/damit-es-ein-fuer-allemal-klar-ist-so-haengt-ihr-die-rolle-toilettenpapier-richtig-auf/
[6]: http://en.wikipedia.org/wiki/Toilet_paper_orientation#Solutions "Toilet paper orientation - Solutions"

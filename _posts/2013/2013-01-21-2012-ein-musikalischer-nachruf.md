---
title: 2012 – ein musikalischer Nachruf
layout: post
lang: de
permalink: /2240/2012-ein-musikalischer-nachruf/
categories:
  - music
tags:
  - Echochamber
  - podcast
  - radio
  - DJ-Set
comments: true
share: true
modified: 2021-07-05T12:34:00+01:00
image:
  feature: 2013/01/echochamber-e1359200127308.jpg
  credit: BarbNerdy
---

Im Januar 2013 wurde ich vom Lord of Dub – [Saetchmo][1] – eingeladen, ihn in seiner wöchentlich stattfindenden Online-Radio-Show "[Echochamber][2]" zu vertreten.

Saetchmo selbst hängt die musikalische Qualitätslatte sehr hoch. Und ich wollte ihn natürlich so gut wie möglich vertreten. Das steigerte meine Freude als auch meine Aufregung enorm. Es sollte thematisch besonders werden, mit Stil und zugleich stiltreu.
<!--more-->

`<Einschub>` Wenn ein neues Jahr beginnt, wollen viele (so auch ich) einiges besser machen, als in den Jahren zuvor. Als ich kurz nachdachte, was ich so alles vernachlässigt habe, aber total gerne gemacht hätte, fiel mir – neben Joggen, besser und regelmäßig essen, früh aufstehen, mehr Zeit für alles mögliche nehmen – Musik auf und ein. Ich habe 2012 eindeutig zu wenig Mixe veröffentlicht und habe doch noch so viele Ideen. Also veröffentliche ich ab jetzt mindestens einmal im Monat einen Mix. `</Einschub>`

Als hätte Saetchmo das gewusst, bekomme ich also gleich die beste Unterstützung für mein Vorhaben – der Januar-Mix wird schon mal ein Live-Mix. Jetzt noch das Thema… Es war ja noch so viel schöne und zum Teil von mir ungespielte Musik aus dem letzten Jahr übrig. Aber einfach so zusammenstückeln und nacheinander spielen? Beim genaueren Hinhören fielen mir die unterschiedlichen Stile auf, ich verschubladete die Titel und erstellte in jeder Gruppe eine kleine Best Of. Und das sind sie: Dub'n'Reggae, Vocal Step, Hip Hop, Dance, Dubstep, Ragga Step, Mash Ups, Freestyle und Kuschel Step.

<iframe id="hearthis_at_track_6059912" style="overflow:hidden;border:none;background-color:transparent;height:150px;width:100%;" src="https://app.hearthis.at/embed/6059912/transparent_black/?hcolor=&color=&style=2&block_size=2&block_space=1&background=1&waveform=0&cover=0&autoplay=0&css=" allow="autoplay"></iframe>
<small>(download auf <a title="saetche.net" href="https://www.saetche.net/blog/category/echochamber/page/381/">Saetchmos Blog</a>)</small>

## Tracklist und Download-Links:
<!--more-->

1.  "**Faso Dub** ft. Daba Makourejah" von Mahom auf "Dub by Sub" <small>([odgprod.com](http://odgprod.com/2012/10/mahom-dub-by-sub/){: .download})</small>
    * ein sehr schöner Dub-Remix dieses träumerischen Reggae-Songs
2.  "**Peace And Love**" von FLeCK <small>([Soundcloud](http://soundcloud.com/fleckathens/fleck-peace-and-love){: .download})</small>
    * verspielter Lo-Fi-Reggae auf Drum'n'Bass
3.  "**Dem Can't Stop We From Talk** feat. Devon D (Dubblestandart's Kingston Riot riddim dub)" von Subatomic Sound System auf "Dem Can't Stop We From Talk" <small>(commercial)</small>
    * diesen rollenden Beat habe ich einfach oft spielen müssen…
4.  "**Blasted**" von Robotic Pirate Monkey auf "Heat.wav" <small>([roboticpiratemonkey.com](http://roboticpiratemonkey.com){: .download})</small>
    * ich hätte mindestens drei Tracks von dieser Platte spielen wollen
5.  "**How I Roll** (TYR Remix)" von Charlie Mars auf "Stimulate EP" <small>([Soundcloud](https://soundcloud.com/tyrdubstep/charlie-mars-how-i-roll-tyr){: .download})</small>
    * toller Pop
6.  "**Lifeline** (Figure Remix)" von Citizen Cope <small>([citizencope.com](http://citizencope.com/){: .download})</small> 
    * bei dieser Alternative-Ballade schmelze nicht nur ich dahin
7.  "**One Day** (Nilow Remix)" von Asaf Avidan <small>([www.facebook.com/nilow030](http://www.facebook.com/nilow030/){: .download})</small>
    * "Hey Ladies!"
8.  "**Makes Me Ill**" von Razorback auf "Vital: Infinity" <small>([Soundcloud](https://soundcloud.com/vitalfm/sets/vital-infinity-free-12-track){: .download})</small>
    * Headbanger – Metal war gestern
9.  "**Soul Sent**" von NickNack auf "State Machine" <small>([nicknack.bandcamp.com](http://nicknack.bandcamp.com/){: .download})</small>
    * die gebrochenen Beats purzeln dahin
10. "**La Piñata**" von Kool A.D. auf "51" <small>([mishkanyc.bandcamp.com](http://mishkanyc.bandcamp.com/album/51){: .download})</small>
    * dieser Flow und der mellow Rhythmus dazu
11. "**World Domination**" von Joey Bada$$ auf "1999" <small>([DatPiff](http://www.datpiff.com/Joey-Bada-1999-mixtape.361792.html){: .download})</small>
    * und noch mehr mehr davon – get funky
12. "**Look Who's Back**" von KOAN Sound feat. Foreign Beggars auf "OWSLA Presents Free Treats Vol. 2" <small>(commercial)</small> 
    * das nenne ich einen kraftvollen Tanzflächenfüller
13. "**Pepper** (remix Broad Rush)" von HBZ auf "Pepper EP" <small>([DBZ](http://dbangerz.com/music/hbz_pepper_ep.html){: .download})</small>
    * Ska Speed Garage Hip Step
14. "**Big Bada Boomtown**" von Slamboree <small>([freear.bandcamp.com](http://freear.bandcamp.com){: .download})</small>
    * Let's dance!
15. "**Warlord** (Garage Mix)" von ENiGMA Dubz auf "Dank 'N' Dirty Dubz (30 Freebies from ENiGMA Dubz)" <small>([Soundcloud](https://soundcloud.com/enigmadubz/enigma-dubz-warlord-garage-mix){: .download})</small>
    * düüüüühhhster
16. "**Sweden**" von Two Fingers auf "Stunt Rhythms" <small>(commercial)</small>
    * Amon Tobin mit meiner Platte des Jahres 2012
17. "**Alternate**" von DJ Zinc <small>([Soundcloud](https://soundcloud.com/zinc/zinc-alternate){: .download})</small>
    * einfach, naiv, groovy
18. "**Ow Yeah!**" von R.O <small>([Dubstep.NET](http://www.dubstep.net/track/ow-yeah-by-r-o){: .download})</small>
    * das ist wohl unter Funky Step zu verschubladen
19. "**Final Boss**" von Direct <small>([Dubstep.NET](http://www.dubstep.net/track/final-boss-by-direct){: .download})</small>
    * 8bit meets massive bass
20. "**Konichiwa**" von Glockwize <small>([Dubstep.NET](http://www.dubstep.net/track/konichiwa-by-glockwize){: .download})</small>
    * Stampfer
21. "**Extreme Ways** (Bourne's Legacy) (PatrickReza Remix)" von Moby <small>([Dubstep.NET](http://www.dubstep.net/track/extreme-ways-bournes-legacy-by-moby-patrickreza-official-remix){: .download})</small>
    * so gefällt mir Moby am besten
22. "**Dis Iz Why I'm Hot** (Mutrix Remix)" von Die Antwoord <small>([Dubstep.NET](http://www.dubstep.net/track/dis-iz-why-im-hot-by-die-antwoord-mutrix-remix){: .download})</small>
    * Die Antwoord geht immer!
23. "**So What Cha Want** (Reggae Remix)" von Beastie Boys auf "Beastie Revolution – Beastie Boys Reggae Remixes" <small>([Soundcloud](https://soundcloud.com/upcutsound/03-so-what-cha-want){: .download})</small>
    * einfach GROßARTIG
24. "**Killing in the Name** (jazz version)" von The Andy Lim Trio feat. Rage Against the Machine
    * Ja, ich mag Jazz!
25. "**2 bit Blues**" von Kid Koala auf "2 bit Blues x 6 bit Blues" <small>(commercial)</small>
    * meisterhafter Turntable Blues Rock auf Ninja Tune
26. "**Slowine**" von Schlachthofbronx auf "Dirty Dancing" <small>([Soundcloud](https://soundcloud.com/schlachthofbronx/schlachthofbronx-slowine-1){: .download})</small>
    * dieser Bass!!!
27. "**POWER**" von Big Gigantic x GRiZ <small>([Soundcloud](https://soundcloud.com/griz/big-gigantic-x-griz-power){: .download})</small>
    * pompöser geht kaum – breit und laut
28. "**Welcome to Jamrock** (LabRat Remix)" von Damian Marley <small>([Dubstep.NET](http://www.dubstep.net/track/welcome-to-jamrock-by-damian-marley-labrat-remix){: .download})</small>
    * mein persönlicher Nummer-1-Hit 2012
29. "**Make It Bun Dem** (Luke Da Duke Remix)" von Skrillex & Damian Marley <small>(["Dubstep.NET"](http://www.dubstep.net/track/make-it-bun-dem-by-skrillex){: .download})</small>
    * in 2012 um Skrillex herum zu kommen, ging einfach nicht
30. "**My Selector** (Dirt Monkey Remix)" von Feral is Kinky <small>([Soundcloud](https://soundcloud.com/feral/feral-is-kinky-my-selector){: .download})</small>
    * Ragga und Dubstep – mehr brauche ich nicht
31. "**Isis**" von Seven Lions <small>([Soundcloud](https://soundcloud.com/viperrecordings/seven-lions-isis-free-download){: .download})</small>
    * viel mehr als nur World Music mit Bass
32. "**Stealth Camel**" von Secret Archives of the Vatican auf "SUBBASS – Ethnostep 2" <small>([subbass.bandcamp.com](http://subbass.bandcamp.com/album/ethnostep-2){: .download})</small>
    * Bass\<3 im 3/4-Takt
33. "**Fiya!**" von Umoja auf "Je – Ka – Lo!" <small>([inirecordings.bandcamp.com](http://inirecordings.bandcamp.com){: .download})</small>
    * bester Afro-Funk
34. "**Strutter**" von Smerf auf "Strutter / Believe" <small>(commercial)</small>
    * langsam und minimal
35. "**Dragonfly** feat. Zaki Ibrahim (Shorterz & Enigma Mix)" von Richard The Third <small>([Soundcloud](https://soundcloud.com/tom-shorterz/free-download-richard-the){: .download})</small>
    * einfach nur schön
36. "**Bombs Over Capitals** (Clark Kent Remix)" von Max Vangeli & AN21 <small>([Dubstep.NET](http://www.dubstep.net/track/bombs-over-capitals-by-max){: .download})</small>
    * kraftvoller Gesang mit ebensolchen Bass
37. "**Arcadia**" von Traveler <small>([Dubstep.NET](http://www.dubstep.net/track/arcadia-by-traveler){: .download})</small>
    * ein gemütlicher und zugleich verspielter Track
38. "**Princess**" von FLeCK
    * klassisches Orchester mit Stolper-Beats #hach

## Danke

an [Saetchmo][1] für die Einladung und das Vertrauen, [BarbNerdy][3] (hier ihre [Echochamber-Vertretung][4]) für Strom, Tee, Schnittchen, Platz und Unterstützung bei der Einrichtung (und das tolle [Foto][5]), [Basti][6] für die Hilfe bei der Einrichtung und das Streaming, die [Blogrebellen][7] (happy Birthday! #SIXYRS) für das Hosten des Streams und die Werbung, [CptPudding][8] für das Anheizen mit dem [Vorzimmer][9] und allen, die mich aktiv im [Chat][10] und auf Twitter unterstützt oder auch einfach nur zugehört haben!

Und vergesst nicht, Donnerstag um 22 Uhr ist [Echochamber][11]-Zeit! #TGIT

[1]: https://www.saetche.net/ "Saetchmo"
[2]: https://www.saetche.net/blog/category/echochamber/page/381/ "#Echochamber"
[3]: http://supportyourlocalghetto.tumblr.com/ "support your local ghetto"
[4]: http://hallama.org/earcandy-2/echochamber
[5]: http://instagram.com/p/UmlIz3yPpr/
[6]: https://twitter.com/DiahRihaJones "@DiahRihaJones"
[7]: http://blog.rebellen.info/saetchmo-echochamber-livestream/ "Blogrebellen"
[8]: http://www.progolog.de/ "Progolog"
[9]: http://blog.rebellen.info/vorzimmer/ "#Vorzimmer"
[10]: irc://irc.freenode.net/blogrebellen "#blogrebellen"
[11]: https://www.saetche.net/blog/livestream/ "#Echochamber"

*[TGIT]: Thank God It's Thursday

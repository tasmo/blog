---
title: Android unter Kontrolle
layout: post
lang: de
permalink: /android-unter-kontrolle/
redirect_from:
  - /3364/android-abhaerten/index.html
  - /3364/android-unter-kontrolle/index.html
categories:
  - tech
tags:
  - Android
  - Datenschutz
  - privacy
  - security
  - howto
  - OpenSource
comments: true
share: true
image:
  feature: 2013/06/3803623394_44a49b871c_b.jpg
  credit: Wee Sen Goh
  creditlink: http://www.flickr.com/photos/weesen/3803623394/
---

Die [Entscheidung][1], ein Smartphone mit iOS oder [Android][2] zu benutzen, war bei mir vor allem von den Möglichkeiten geprägt, das jeweilige System an meine Bedürfnisse anpassen zu können.

Aus ganz ähnlichen Gründen benutze ich auch seit meinem ersten Computer immer wieder Linux und bevorzuge Mac OS X gegenüber MS Windows. Unten soll es vor allem um die Sicherheit und den Datenschutz gehen.

## Spieglein, Spieglein {#spiegleinspieglein}

Mein Smartphone benutze ich mittlerweile viel häufiger, als ich es mir bei der Anschaffung vorstellen konnte. Es ist mein tragbares Büro, meine Kamera für Schnappschüsse, Adressbuch, Lexikon und Telefon – der [Pocket Guide to the Galaxy][3].

Und da ich es so viel benutze, möchte ich, dass es mir gefällt, ich es gerne anschaue und bediene. Klarer Vorteil für Android ist, dafür gibt es massenhaft Anpassungen in Form von Launchern, Themes oder ROMs, die das alles gleich eingebaut haben.
<!--more-->

## Apps, Apps, Apss {#appsappsapss}

Da so ein tolles Device alles mögliche kann, gibt es auch sehr viele Wege, alles mögliche zu tun – in Form von Apps. Da ein Android anders als ein iPhone nicht nur an einen App-Markt gebunden ist, kann ich auch einfach nur freie Software installieren wie zum Beispiel von [F-Droid][4]. Im Vergleich zum iTunes-Store für iOS gibt es für Androiden mehr, qualitativ oft hinterfragenswürdige, manchmal sogar schönere aber vor allem eben mehr freie Apps.

## Sesam, schließe Dich {#besamschlieedich}

Wenn doch mal mein Smartphone abhanden kommt – kann leider jederzeit passieren – möchte ich dafür sorgen, dass die Daten nicht in Hände von dritten Menschen oder Firmen gelangen. Schließlich trage ich auf so einem kleinen Hosentaschenrechner allerhand Informationen über mich und mein Umfeld – also auch Daten, die Dritte betreffen – mit mir.

Also ist eine Verschlüsselung der Daten Voraussetzung. Android kann das ab Version 4.0 von Hause *Google* aus, nur muss es vom Benutzer selber in den Einstellungen unter dem Punkt Sicherheit aktiviert werden. Und das ohne das Gerät roten zu müssen. Das ist ein längere Prozedur, da alle Daten in eine verschlüsselte Partition kopiert und anschließend wieder gelöscht werden müssen. Davor hatte ich etwas Respekt, ist aber bei meinem Nexus 4 gleich gut gegangen und ich kann es so weiter empfehlen.

## Finger weg! {#fingerweg}

![no permissions screen](/images/2013/06/2013-02-11-09.58.jpg)

Weiterhin möchte ich verstehen, auf welche Daten einzelne Apps zugreifen und wohin sie diese schicken. Wenn eine App zuviel darf, möchte ich ihr dafür erstens gehörig auf die Finger klopfen und zweitens die Berechtigungen entsprechend einschränken. Das ist einer der größten Minuspunkte von Android: iOS hat das nämlich mittlerweile schon eingebaut.

Dafür habe ich verschiedene Varianten ausprobiert. Wichtigste Voraussetzung dafür scheint zu sein, dass das Gerät [gerootet][5] ist. So bekomme ich Zugang zu Funktionen im System, an die sonst nur administrative Prozesse kämen.

Eine Variante ist, eine [ROM][6] wie [PACman][7] aufzuspielen, ein modifiziertes Android-Sytem mit eingebautem [PDroid][8]. Genau dieses PDroid macht es möglich, die Rechte für jede einzelne App zu überprüfen und einzeln abzustellen. Will ich PDroid auf meinem bestehenden System benutzen, muss ich dafür die ROM und somit den Kernel flashen. Und das leider bei jedem System-Update erneut.

Die mir nun liebste Weise ist eine Kombination aus zwei Apps. Die erste überprüft die Berechtigungen jeder installierten App und informiert mich bei Veränderungen. Das macht zum Beispiel [Clueful][9] von Bitdefender hervorragend. Für die zweite App ist wieder das Rooten notwendig. Mit dem nicht so schön designten [Permissions Denied][10] kann ich dann in mühevoller Kleinarbeit alles Abstellen, was ich nicht brauche und nicht haben will. Das ist auch ein guter Weg, mein Android vor [Malware][11] zu schützen.

<figure class='third'>
  <a rel='attachment' href='/images/2013/06/DFG_2013-06-10-12-32-59.png'><img width="150" height="150" src="/images/2013/06/DFG_2013-06-10-12-32-59.png" alt="Clueful - Übersicht" /></a>
  <a rel='attachment' href='/images/2013/06/DFG_2013-06-10-12-32-18.png'><img width="150" height="150" src="/images/2013/06/DFG_2013-06-10-12-32-18.png" alt="Clueful - Notifications" /></a>
  <a rel='attachment' href='/images/2013/06/DFG_2013-06-10-12-33-05.png'><img width="150" height="150" src="/images/2013/06/DFG_2013-06-10-12-33-05.png" alt="Clueful - Ingress" /></a>
  <a rel='attachment' href='/images/2013/06/DFG_2013-06-10-12-33-13.png'><img width="150" height="150" src="/images/2013/06/DFG_2013-06-10-12-33-13.png" alt="Permissions Denied - Packages" /></a>
  <a rel='attachment' href='/images/2013/06/DFG_2013-06-10-12-33-09.png'><img width="150" height="150" src="/images/2013/06/DFG_2013-06-10-12-33-09.png" alt="Permissions Denied - F-Droid" /></a>
  <figcaption>"Clueful" &amp; "Permissions Denied"</figcaption>
</figure>

Das ist eine Möglichkeit, das Android-OS etwas sicherer zu machen. Viel wichtiger ist, schon vor dem Installieren fragwürdiger Apps vorsichtig zu sein.

## Nachtrag I:

Mit [CyanogenMod 10.1.1][12] und neuer und [darauf][13] [basierenden][14] [ROMs][15] ist es nun ohne zusätzliche Apps möglich, einzelne Programme in eine [Sandbox][16] zu sperren. Der "Privacy Guard" kann über die *App-Info* aktiviert werden. Damit kann das entsprechende Programm nicht mehr auf das Adressbuch zugreifen, SMS und Anrufprotokoll lesen und kann den aktuellen Standort nicht mehr abfragen.  
Für das alternative Betriebssystem ist auch eine detaillierte Anleitung auf Englisch auf [additivetips][17] erschienen.

---

Mehr zum Thema: [Android: Werbe-Blocker geblockt][18]

[1]: http://www.androidauthority.com/10-reasons-why-android-is-still-better-than-ios-145370/
[2]: /tag/Android/
[3]: http://de.wikipedia.org/wiki/Per_Anhalter_durch_die_Galaxis
[4]: http://f-droid.org/
[5]: http://en.wikipedia.org/wiki/Android_rooting
[6]: http://www.androidpit.de/de/android/wiki/view/ROMs
[7]: http://forum.xda-developers.com/showthread.php?t=2146879
[8]: http://forum.xda-developers.com/showthread.php?t=1994860
[9]: http://www.cluefulapp.com/
[10]: https://play.google.com/store/apps/details?id=com.stericson.permissions.donate
[11]: http://www.heise.de/security/meldung/Android-ist-das-Top-Ziel-fuer-mobile-Malware-1818986.html
[12]: http://www.cyanogenmod.org/blog/cyanogenmod-10-1-1-release
[13]: http://www.paranoid-rom.com/
[14]: http://aokp.co/
[15]: http://pac-rom.com/
[16]: http://de.wikipedia.org/wiki/Sandbox
[17]: http://www.addictivetips.com/android/cyanogenmod-10-1-complete-review-guide/ "CyanogenMod 10.1 – A Complete Hands-On Review & Guide"
[18]: /3060/werbe-blocker-geblockt/ "Android: Werbe-Blocker geblockt"

---
layout: post
lang: de
date: 2013-12-23
modified: 2016-01-22T14:18:33+01:00
title: Mac OS X im öffentlichen Netz
subtitle: Eine Anleitung zum Schutz der eigenen Daten
permalink: /osx-im-netz/
categories: tech
tags:
 - macosx
 - privacy
 - SSL
 - Verschlüsselung
 - howto
comments: true
share: true
image:
  feature: 2013/Galaxy_panic.jpg
---

Ein Computer im Internet befindet sich in einem virtuellen, öffentlichen Raum. Hier versuchen verschiedene Seiten Informationen jeglicher Art en masse zur beliebigen Aus- und Verwertung zu sammeln. Nicht jede\*r möchte, dass alle Welt in ihrem digitalen Wohnzimmer ein- und ausgeht.

Im Angesicht der unaufhörlichen, sogar staatlicherseits willentlichen Beschnüffelns ist es für immer wichtiger, zu zeigen, dass Privatsphäre keine Zauberei ist und fast alle Mittel an Board aktueller Computer vorhanden sind. Ich habe mich hier auf das weit verbreitete _Mac OSX_ konzentriert.

Es soll jedoch nicht der Eindruck entstehen, dass das eigene System danach unangreifbar ist. Vieles lässt sich vermeiden, die totale Sicherheit gibt es nicht.
<!--more-->

> Ein Mac ist sicherer als ein PC.

Dazu sage ich mal “jein”.  
__Technisch gesehen__ werden die Schwachstellen von _Mac OSX_ weniger oft ausgenutzt als die von _Microsoft Windows_[^1]. Das liegt zum großen Teil an der immer noch weiten Verbreitung von WinDOwS und ist zum anderen auch historisch, traditionell oder hacker-ethisch gesehen ein beliebteres Ziel.  
__Der Begriff__ _PC_ -- also _Personal Computer_ -- schließt jedoch alle Heimrechner ein, also jegliche Computer für Büro und zu Hause. Egal, ob tragbar oder nicht.  
__Genau hingeschaut__ ist die größte Schwachstelle doch nach wie vor der Mensch, der den Computer bedient. Und dabei kann an allen populären Plattformen viel falsch gemacht werden.  
Hinter einem _Mac_ wägen sich die meisten sicher und könnten genau dadurch unachtsam werden. Da alle _Mac_s je Modell gleich gebaut sind, weisen wenn, dann auch alle der Serie die selben Sicherheitsmängel auf.

## Daten sichern

Der __wichtigste Punkt__ für die Sicherheit der eigenen Daten ist, sie vor Verlust zu schützen. Deshalb ist es wichtig, -- egal, ob ich einen Computer, eine Kamera oder ein Smartphone benutze -- __regelmäßige Backups__ zu erstellen. Mit [_Time Machine_](https://support.apple.com/de-de/HT201250) besitzt _Mac OSX_ die nötige Software. _Time Machine_ kann nicht nur von sich aus regelmäßig Backups erstellen, es kann sie sogar verschlüsselt abspeichern sowie wieder einlesen und hat nebenbei eine Versionsverwaltung mit ebenso komfortabler Oberfläche. Es wird nur noch ein Datenträger mit genügend Speicherplatz (mindestens das Doppelte des bisher belegten Speichers) und dem richtigen Anschluss gebraucht, also eine __externe__ Festplatte, SSD, ein NAS oder Server.

## Standardeinstellungen ändern

Die __Grundeinstellungen__ müssen unbedingt überprüft werden. _Apple_ versucht es allen Benutzern möglichst leicht zu machen und geht dabei Kompromisse ein, die zu Lasten der Sicherheit und Privatsphäre gehen. Die __Gratwanderung zwischen Komfort und Sicherheit__ ist in fast allen Bereichen der Datenverarbeitung vorhanden. Die nächsten Schritte können zu Lasten des Komforts gehen, werden Ihnen aber mehr Kontrolle ermöglichen.

Im Programm _Systemeinstellungen_, welches seit “Mountain Lion” unter `/Applications` zu finden ist, gibt es die Rubrik “Sicherheit”.  
Falls das noch nicht geschehen ist, sollte dort ein _Anmeldekennwort_ gesetzt werden (siehe [sichere Passwörter](#sichere-passwrter)).

### Firewall starten

Ebenfalls unter “Sicherheit” finden Sie die Einstellungen zur Firewall. Diese sollte unbedingt aktiviert sein. Überprüfen Sie unter “Firewall-Optionen” auch die vorhanden Regeln -- gegebenen Falls löschen Sie Regeln, denen Sie nicht vertrauen. Von jetzt an wird _Mac OS X_ Sie fragen, ob ein Programm jetzt Daten ins Internet/Netzwerk senden darf oder nicht.  Zudem sollte der “Tarnmodus” aktiviert sein.

### Privatsphäre

Im nächsten Reiter sollten Sie allen Programmen prinzipiell alles verbieten -- natürlich bis auf die Dienste, von denen Sie wissen, dass Sie sie brauchen.

### Bildschirmschutz

Ein __Bildschirmschoner__ hat bei modernen Displays nicht mehr die Hauptaufgabe, das Einbrennen von grafischen Elementen auf die Bildschirm-Matrix zu verhindern, vielmehr kann er den Computer vor unberechtigtem Gebrauch schützen. Es sollte eine kurze Aktivierungszeit (\< 5 Minuten) gewählt werden und die Passwortabfrage aktiviert sein.  
Zu dem ist ratsam, tragbare Computer zuzuklappen. Das Display kann mit der Kombination <kbd>Umschalttaste</kbd> + <kbd>Steuerung</kbd> + <kbd>Auswurftaste</kbd> ausgeschaltet und geschützt werden.

### Tarnkappe

In den _Systemeinstellungen_ unter “Freigaben” sollten an allen Diensten die Häkchen entfernt werden, bei denen Sie sich nicht sicher sind, dass Sie sie benötigen. Somit verschwinden viele Türen zu Ihrem _Mac_, die zwar nicht unbedingt offen stehen -- sie sind ja mit einem Passwort geschützt -- sie sind gar nicht mehr da. Es ist unerlässlich den “Gerätename” zu ändern. Im Ursprungszustand enthält er nämlich Ihren Benutzernamen und kann so in einem Netzwerk einfacher angegriffen werden.

## Ohne Rechte arbeiten

Entziehen Sie sich die Rechte, auf systemrelevante Dateien zugreifen zu können. Alle Programme, die Sie benutzen, haben prinzipiell die selben Rechte wie Sie -- können alles lesen und verändern, was Sie auch können.

Der einfachste Weg dagegen ist, ein zweites Nutzerkonto anzulegen. Eines bekommt die notwendigen Rechte, zur Administration des Computers, das andere nur die nötigsten, um den Computer bequem bedienen zu können. Die notwendigen Einstellungen finden Sie unter “Benutzer & Gruppen” in den _Systemeinstellungen_.  
Keine Sorge -- um wichtige, administrative Aufgaben erledigen zu können, müssen Sie trotzdem selten das Konto wechseln. Für die meisten Arbeiten erscheint ein Dialog, mit dem Sie sich als Administrator authentifizieren können.

## Sichere Passwörter

Ein sicheres Passwort ist eines, das nur Sie kennen, das nur möglichst gar nicht erraten werden kann, weder von einem Programm noch einer Person geknackt werden kann und auf das Sie trotzdem zugreifen können, wenn Sie es brauchen. Es gibt [verschiedene Strategien, sich sichere Passwörter auszudenken](https://de.wikipedia.org/wiki/Passwort#Sicherheitsfaktoren).

Mit dem _Kennwortassistent_en ist zum Erzeugen sicherer Passwörter ein kleines Tool an vielen Stellen erreichbar. Es befindet sich zum Beispiel in den “Benutzer & Gruppen”-Einstellungen in den _Systemeinstellungen_ unter “Kennwort ändern”, in der _Schlüsselbundverwaltung_ hinter dem kleinen “+” ganz unten, um neue Objekte zu erstellen und im _AirPort-Dienstprogramm_ für die WLAN-Sicherheit.

### Schlüsselbund

Das Programm _Schlüsselbundverwaltung_ bietet sehr guten Komfort, die Passwörter an Board zu verwalten. Dabei werden alle Kennwörter in eine verschlüsselte Datenbank gespeichert. Wenn dies nicht explizit geändert wird, ist das Passwort für die Datenbank das Anmeldekennwort. So entsperrt sich der Schlüsselbund mit erfolgreicher Anmeldung.

Genau das sollten Sie ändern, damit andere nicht über einfache Umwege an Ihre gespeicherten Passwörter gelangen können. Im Programm auf der linken Seite den Schlüsselbund “Anmeldung”/“login” mit einem rechten Mausklick anwählen und im Menü “Kennwort ändern” wählen. Ab jetzt muss das neue Passwort des Schlüsselbundes eingegeben werden, obwohl Sie schon eingelockt sind. Unter “Einstellungen für...” können Sie eine Zeit bestimmen, nach der sich der Schlüsselbund schließt und das Kennwort erneut eingegeben werden muss.

## Festplatte verschlüsseln

Ein Computer ist nicht nur vom Netz aus angreifbar. Gegen Diebstahl oder sonstigen, direkten, physischen Zugriff ist eine Verschlüsselung ratsam. Mit _FileVault_ ist es sehr einfach die Festplatte/SSD an Board komplett[^2] zu verschlüsseln.  
Apple selbst hat dazu eine [verständliche Anleitung](https://support.apple.com/de-de/HT204837). Ich möchte dem noch hinzufügen, dass ich es __nicht__ empfehle, den Wiederherstellungsschlüssel oder sonstige private Daten -- insbesondere Passwörter -- via _iCloud_ bei Apple oder anderen prominenten Sharing-Diensten zu speichern!

Wenn der Computer läuft, ist natürlich auch die Festplatte für den User entschlüsselt. Der Rechner sollte dann __ausgeschaltet__ sein, wenn er allein gelassen wird.

## Firmware-Passwort setzen

Bein Starten eines Mac gibt es [verschiedene Kurzbefehle](https://support.apple.com/kb/PH21908?locale=de_DE), um beim Startvorgang administrative Aufgaben ausführen zu können. Gegen einen Missbrauch davon, hilft es, ein Passwort für die Firmware EFI zu setzen. Auch dazu hat Apple selbst eine [passende Anleitung](https://support.apple.com/de-de/HT204455) (und eine [für ältere Macs](https://support.apple.com/de-de/HT1352)).

## Gast im Netzwerk

Nicht nur im Internet ist ein Computer angreifbar. Noch einfacher ist es in einem Netzwerk mit viel weniger Teilnehmer\*innen, wie in einem Internetcafé, öffentlichen WLAN oder Firmennetzwerk. `<ironie>`Ganz anders verhält sich das im Netzwerk eines Hacker-Kongresses[^3].`</ironie>`

Ein Weg trotzdem relativ unbelauscht im WWW aktiv sein zu können, ist das Nutzen eines VPN. Das setzt zwei Dinge voraus, einen VPN-Server -- also Endpunkt -- und ein VPN-Client. Für letzteres wird vielerorts [Tunnelblick](https://de.wikipedia.org/wiki/Tunnelblick_%28Software%29) empfohlen. So auch hier.

Als Endpunkt bietet sich entweder der Router zu Hause oder ein vertrauenswürdiger [VPN-Dienst](https://wiki.cacert.org/openVPN/CommunityTunnel) an.

## Mikrofon und Kamera

Erst kürzlich [ist bestätigt worden](http://www.heise.de/mac-and-i/meldung/Forscher-aktivieren-MacBook-Kamera-ohne-Warn-LED-2070161.html), dass es relativ einfach möglich ist, die Web-Kamera eines *Mac*s zu aktivieren, ohne dass die Warn-LED an ist. Das heißt, Sie bekommen nichts davon mit. Dass dies technisch möglich ist, schien vielen bisher logisch, praktisch nachvollziehbar ist es jetzt.  
Dagegen hilft ein einfacher Trick: __Überkleben__ Sie Ihre Kamera dann, wenn Sie sie nicht brauchen.

Anders ist es beim Mikrofon. Da gibt es gleich gar keine Warn-LED. Zwar kann in den Einstellungen die _Input_-Quelle auf stumm gestellt werden, einen generellen Zugriff von Prozessen auf das Mikrofon kann trotzdem nicht hundertprozentig unterbunden werden. Es ist möglich, unbemerkt die Lautstärke temporär zu ändern oder eine andere Audio-Quelle zu wählen.

## Programme der anderen

Benutzen Sie keine Programme, denen Sie nicht vertrauen können. Viele kleine Helferlein telefonieren nach Hause zu ihrem Herren, ohne dass Sie wissen, wer das ist, was die Programme übermitteln, noch, dass sie es überhaupt tun. Das betrifft ins Besondere Chat- und Such-Programme. Viele prominente Anbieter und Hersteller schießen mit der Analyse der Daten weit über das Ziel hinaus. So werden ganze Adressbücher, gesamte Nachrichten-Verläufe, Profile des Rechners samt Übersicht der installierten Programme und viele Details mehr ausgelesen und übertragen.

Ich werde es an dieser Stelle unterlassen, einzelne Programme zu nennen, weil der Eindruck entstehen könnte, es handele sich nur um die genannten.

## Bluetooth

Wenn Sie es nicht unbedingt brauchen, schalten Sie es einfach aus. Falls doch, seien Sie sich sicher, dass Ihr Rechner nicht sichtbar ist und sie den "[Gerätenamen](#tarnkappe)" in einen allgemeinen geändert haben.

## Fazit

Trotz aller Sicherheitsmaßnamen sind diese nur Erschwernisse und nichts kann zu einhundert Prozent verhindern, dass Ihr Rechner angegriffen oder ausspioniert wird. Keine Anleitung kann den gesunden Menschenverstand ersetzen.

---

## Quellen:

- Im Wiki des CCC für den 29C3 2013 steht unter [How To Survive](https://events.ccc.de/congress/2013/wiki/Static:How_To_Survive) das Wichtigste, was technisch zu beachten ist.
- Philip Brechler aka [Plätzchen](https://twitter.com/plaetzchen) zeigt in einer Video-Anleitung ganz anschaulich, [wie man Mac OS X 10.7 Lion für Veranstaltungen wie den 28C3 absichert](http://www.youtube.com/watch?v=Yv608m9d6ys). Obwohl das, __wie__ er es vorträgt, für meinen Geschmack etwas schulmeisterlich klingt, hat er in dem, __was__ er sagt und zeigt, vollkommen recht.
- Apple selbst hat für die nicht mehr gepflegte Version 10.6 aka _Snow Loepard_ eine [Security Configuration als PDF](http://images.apple.com/support/security/guides/docs/SnowLeopard_Security_Config_v10.6.pdf) veröffentlicht.

[^1]: Das auch sehr beliebte UNIX-Derivat _LINUX_ lasse ich gerade bewusst außen vor, da ich davon ausgehe, das die Nutzer\*innen dieses OS sich mehrheitlich mit ihrem System eh näher beschäftigen und Sorge tragen.
[^2]: Komplette Verschlüsselung erst mit _FileVault 2_ ab _Mac OS X Lion_, davor nur für den Benutzer-Ordner
[^3]: Diese Anleitung ist ursprünglich für die Teilnahme am [30C3](https://events.ccc.de/congress/2013) bestimmt.

*[Mountain Lion]: Mac OSX 10.8
*[WLAN]: wireless local area network
*[SSD]: Solid State Disk
*[WWW]: World Wide Web
*[VPN]: Virtual Private Network
*[EFI]: Extensible Firmware Interface

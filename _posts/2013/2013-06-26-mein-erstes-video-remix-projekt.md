---
title: Mein erstes Video-Remix-Projekt
layout: post
lang: de
permalink: /3436/mein-erstes-video-remix-projekt/
categories:
  - Video
tags:
  - remix
  - video
  - VJing
comments: true
share: true
image:
  feature: 2013/06/VDMX5.png
  credit: Thomas Friese (CC BY-NC-SA)
---

Für VJing an sich interessiere ich mich schon ganz lange. Wenn in Clubs oder auf Konzerten die Bilder den Sound gelungen untermalen oder gar die Musik zum Soundtrack für die Bilder wird, bin ich jedes Mal begeistert. So etwas möchte ich schon ganz lange auch probieren.

Wie so vieles, das ich schon immer mal können will: Akrobatik, Fremdsprachen sprechen, im [Recht][1] auskennen, [Programmieren][2], Einradfahren, Filmprojekte… und VJing.

## Jetzt also VJ {#jetztalsovj}
<!--more-->

Seit vielen Jahren bin ich [DJ][3]. Nicht ganz so lange nenne ich mich gelegentlich auch so. Jeder DJ mixt verschiedene Musik ineinander. Das ist der Beginn des Remixes. Dann kommen eigene Rhythmen und Basslinien dazu, das Arrangement wird verbogen, Effekte rein, Stücke herausgeschnitten und mit Samples garniert. Fertig. Aber mit Video?

Für das Projekt “[Spring Lessons -- Musiksehen Schwarzweiss II][4]” fragte mich [Caram Kapp][5], ob ich ein paar Remixes Ägyptischer Filmmusik machen könnte. Natürlich! Das ist ein interessantes Projekt mit vielen Fassetten, die mich interessieren.  
Aber wie und womit? Auf der Suche nach den Tools, um Video synchron mit Audio zu spielen, habe ich meine Bisherigen Erfahrungen zusammengetragen.

## Wahl der Waffen {#wahlderwaffen}

Als erstes schaute ich mir mal wieder [Pure Data][6] mit Gem an – ein Projekt, das ständig wächst, immer besser wird, OpenSource lizensiert ist und mit dem ich mich relativ auskenne. Das entscheidende Problem dabei ist, der Movie-Player kann kein Audio abspielen. Das ließe sich umgehen, indem ich je einen Video- und einen Audio-Player baue und beide synchron anspreche. Die Zeit konnte ich mir jetzt nicht nehmen, das will ich aber <mark>unbedingt</mark> nachholen.

Konzeptionell ähnlich ist [Max/MSP][7] mit Jitter und Gen – der große, kommerzielle Bruder von Pure Data. Hier müsste ich mich wieder richtig einarbeiten und billig ist es nicht gerade. Großer Pluspunkt ist aber die Integration in und mit [Ableton Live][8]. Der Audio-Sequenzer kann nämlich mit Max/MSP gebastelte Effekte intern laden und benutzen. Der Ansatz ist aber wegen der flachen Lernkurve und des Preises vertagt worden.

### klassische VJ-Tools {#klassischevj-tools}

Dann habe ich mir verschiedene VJ-Programme angeschaut. [Modul8][9] sieht gut aus und wäre eine denkbare Lösung. Ebenso gut sieht [Resolume Avenue][10] aus.  
Bei mir hat aufgrund einer Empfehlung, den verfügbaren Tutorials und der großen Nutzer\*innenschaft [VDMX5](/images/2013/06/VDMX5.png) von [Vidvox][11] gemacht. Das Programm kann über verschiedene Protokolle angesprochen und synchronisiert werden. So klappt die Verbindung zum Audio-Programm der Wahl auf jeden Fall. [fALk][12] hat mir nach ein paar Tagen kläglicher Versuche eine tolle Einführung gegeben.

Allerdings ist der Weg mit allen Lösungen nicht einfach und schnell. Ich möchte mit dem Video-Material am liebsten so umgehen, wie mit einem Audio-Sampler. Allein um kleine Filmsequenzen einfach hintereinander abzuspielen, muss das Filmmaterial in VDMX für die Bilder und in Live für die Töne vorbereitet werden. VDMX hat zwar einen Ton-Ausgang für jede Ebene, kann jedoch nicht schnell genug umschalten.

## Musiksehen Schwarzweiss Remix one {#musiksehenschwarzweissremixone}

Der erste Versuch ist ein Remix von [أسمهان في كتاب الأغاني الثاني -- يا ديرتي مالك علينا لوم][13]. Es ist ein live gespielter Remix, bei dem alle Effekte in Echtzeit erzeugt werden – in Bild und Ton. Die Musik basiert selbstverständlich auf dem Original, dazu habe ich zwei Bass- und zwei Rhythmusspuren ergänzt. Die Bässe werden via Midi gespielt, die Rhythmen sind entwickelte Loops.

Das Video ist nur ein File. Ich habe es in kleine Abschnitte eingeteilt, mit denen ich dann spielen kann – also von einem Punkt zum anderen springen. Die Video-Effekte sind an verschieden Parameter gekoppelt und werden ausschließlich vom Sound gesteuert.

[Das erste Ergebnis sieht dann so aus: ![Musiksehen Schwarzweiss](/images/2013/06/vlcsnap-2013-06-26-21h15m53s158.png)](https://vimeo.com/69181230)

Ich bin auch sehr gespannt, wie es weitergeht. ;) Updates dann auch unter [Spring Lessons – Musiksehen Schwarzweiss II][4].

*[VJ]: Visual Jockey

[1]: http://www.lawblog.de/
[2]: http://fionalerntprogrammieren.wordpress.com/
[3]: http://www.mixcloud.com/tasmo/
[4]: http://springlessons.org/schwarzweiss/index.html
[5]: https://twitter.com/dot_seekay
[6]: http://puredata.info/
[7]: http://cycling74.com/products/max/
[8]: https://www.ableton.com/de/live/
[9]: http://www.modul8.ch/
[10]: http://resolume.com/
[11]: http://vidvox.net/
[12]: https://twitter.com/protofalk
[13]: http://www.youtube.com/watch?v=UgHhOUdbSKA

---
title: city lights
layout: post
lang: de
permalink: /3362/city-lights/
categories:
  - Foto
tags:
  - Fotos
format: image
comments: true
share: true
---

![city lights][1]  
[city lights][2]

Eigentlich wollte ich die abendlich romantische Stimmung in Berlin mit dem Fernsehtum im Hintergrund fotografieren… Ich mag Zufälle!

[1]: /images/2013/06/8987666415_112b5f0d20_c.jpg
[2]: http://www.flickr.com/photos/tasmo/8987666415

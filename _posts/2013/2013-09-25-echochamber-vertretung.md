---
layout: post
lang: de
title:  Echochamber-Vertretung
permalink: /3688/echochamber-vertretung/
categories: music
tags:
 - music
 - Echochamber
 - DJ-Set
 - free download
comments: true
share: true
image:
  feature: 2013/09/saetchmos-echochamber-tape.jpg
  credit: Saetchmo
  creditlink: http://www.saetche.net/
---

Dubmaster [Saetchmo](http://twitter.com/saetchmo) streamt jeden Donnerstag seine Radio-Show "[Echochamber](http://www.saetche.net/blog/echochamber-19-09-2013/)" mit jeder Menge Bass in das Netz™.

Das macht er nun schon über drei Jahre und hat dabei neben namhaften DJs und Musikern auch immer wieder nicht so bekannte Gäste.
Am letzten Donnerstag durfte ich für ihn einspringen und die Show für zwei Stunden übernehmen. Neben meiner Aufregung, endlich wieder einmal einen Teil meiner Lieblingsmusik spielen zu dürfen, lief am Anfang auch technisch nichts glatt. Zu guter Letzt – doch hört selbst…

<div markdown="0">
<img class="fullwidth" src="/images/2013/09/vote_for_pedro.gif" alt="Vote for Pedro" /><audio controls class="fullwidth">
<source src="//friese.online/files/Echochamber-2013-09-20.ogg" type="audio/ogg">
<source src="//friese.online/files/Echochamber-2013-09-20.mp3" type="audio/mpeg">
<a href="//friese.online/files/Echochamber-2013-09-20.mp3">Echochamber with Tasmo</a>
</audio>
</div>

## Tracklist: {#tracklist2013-09-25}
<!--more-->

1. Kromestar – [In 2 Minds](https://soundcloud.com/kromestar/kromestar-in-2-minds)
1. Ziplokk – [All There Is](https://soundcloud.com/bluered/ziplokk-all-there-is) / Beyond Reality EP
1. Robo Bass Hifi – [Nu Danger Dub](https://soundcloud.com/robo-bass-hi-fi/robo-bass-hifi-nu-danger) / 16 Bit Skanks
1. ODG – [A Nice Dub](https://soundcloud.com/ondubground/odg-chapter-one-in-dub-03-a) / Chapter One in DUB
1. U-Niko - Desierto Dub
1. Maject – [Body](https://soundcloud.com/subablock/maject-body-free-320kbps) // private free download
1. Major Lazer – [Jah No Partial (Jstar Remix)](https://soundcloud.com/jstar-1/major-lazer-jah-no-partial) // [free download](https://soundcloud.com/jstar-1/major-lazer-jah-no-partial/download)
1. The Streets – [Push things Refix 2 (Riddim Tuffa)](https://soundcloud.com/riddimsurfer/the-streets-lets-push-things) // [free download](https://soundcloud.com/riddimsurfer/the-streets-lets-push-things/download)
1. The Police – [So Lonely (C-G's RaggaDub Edit Ft.Big Youth)](https://soundcloud.com/mickcollins_gellar/the-police-so-lonely-c-gs) // Mick Collins-Gellar
1. Djtzinas – [This Jam is Hot Riddim](https://soundcloud.com/djtzinas/djtzinas-this-is-jam-hot) // [free download](https://www.facebook.com/Djtzinas.Kostas/app_220150904689418){: .fa .fa-facebook-square}
1. TITAN SOUND – [3 The Jam Hot Way (DJ Tzinas Vs The Beastie Boys)](https://soundcloud.com/selectademo/3-the-jam-hot-way-dj-tzinas-vs) // [free download](https://soundcloud.com/selectademo/3-the-jam-hot-way-dj-tzinas-vs)
1. Boeboe – [Bout Dat (Liquid Rockz Remix)](http://music.saturaterecords.com/track/bout-dat-liquid-rockz-remix) / El Dorado
1. Skism – [Kick It Ft. Zomboy](https://soundcloud.com/bassface10/skism-ft-zomboy-kick-it)
1. Radiex – Angry Wasp / [Rubber Beats vol. IV](http://caoutchou.bandcamp.com/album/rubber-beats-vol-iv) // pay what you want sampler
1. G Jones – Zig Zag / [Eyes EP](http://robox-neotech.bandcamp.com/album/eyes-ep) // pay what you want
1. Olux - [Assult Squid](https://soundcloud.com/adapted-records/olux-assult-squid-free) / Colossal Attack EP // [free download](https://www.facebook.com/Adaptedrecords/app_220150904689418){: .fa .fa-facebook-square}
1. Toby Stone – [Smash](https://soundcloud.com/glitchhop/smash-by-toby-stone) // [free download](https://soundcloud.com/glitchhop/smash-by-toby-stone/download)
1. R.O – [Optimus VS Megatron](https://soundcloud.com/olivierugi/r-o-optimus-vs-megatron) // [free download](https://soundcloud.com/olivierugi/r-o-optimus-vs-megatron/download)
1. Chimpo – [Buzzin' ft Dub phizix & Skeptical](https://www.youtube.com/watch?v=HM9wkYrpMAs)
1. DRS – Play Wid Fire ft Dub Phizix / [I Don't Usually Like MC's But…](http://mp3.soulr.com/album/i-dont-usually-like-mcs-but)
1. Optiv & BTK feat. Ryme Tyme – [Ignition (Maject remix)](https://soundcloud.com/subablock/optiv-btk-feat-ryme-tyme-1) // free download
1. Bassnectar – Infinite
1. Zinc – [Super Sharp Shooter (Jantsen & Dirt Monkey Remix)](https://soundcloud.com/dirtmonkey/dj-zinc-super-sharp-shooter) // [free download](https://www.facebook.com/jantsenbeats/app_220150904689418){: .fa .fa-facebook-square}
1. Grenier – [Say Something](https://soundcloud.com/djg/say-something) / Here Come the Dark Lights // !K7
1. Perera Elsewhere – Bizarre (Kyson Remix) / [Bizarre EP](http://fofmusic.bandcamp.com/album/bizarre-ep)
1. ill.Gates – Fragile / Flesh // [free download](http://ill-gates.com/category/music/)
1. SHEM – Gloam / [Haywire EP](http://hopskotch.bandcamp.com/album/haywire-ep) // pay what you want
1. RobHex – Timegone / [Timegone EP](http://hexstatic.bandcamp.com/) // pay what you want
1. Die Fantastischen Vier – Tag am Meer

### TL;DR:

Es war toll!

---
title: Musik der Woche
layout: post
lang: de
permalink: /3150/musik-der-woche/
categories:
  - social
  - findings
tags:
  - music
  - compilation
comments: true
share: true
image:
  feature: 2013/04/20130411-191614.jpg
  credit: Gilly
  creditlink: http://blog.gilly.ws/2013/04/11/cats-love-bass
---

## Eine kleine Premiere

Was ich schon lange mal anfangen wollte, ist Themen der vergangenen Woche, die mich bewegten, zusammen zu fassen. Musik bewegt mich immer und immer wieder:

Die [Weisheit der Woche kommt](http://www.youtube.com/watch?v=vftIGU8-uqs) von Reggie Watts:

<iframe style="width:100%; height:360px; border:none;" src="//www.youtube.com/embed/vftIGU8-uqs" allowfullscreen></iframe>
<!--more-->

Die fantastischen The Knife habe [ein schönes, fast neues Video](http://www.youtube.com/watch?v=W10F0ezCTIQ):

<iframe style="width:100%; height:360px; border:none;" src="//www.youtube.com/embed/W10F0ezCTIQ" allowfullscreen></iframe>

Mit der [WDR-Computernacht][2] wird auch — [mindestens kurz](http://www.youtube.com/watch?v=7Rtdb_zgqdQ) — France Gall wieder populär:

<iframe style="width:100%; height:360px; border:none;" src="//www.youtube.com/embed/7Rtdb_zgqdQ" allowfullscreen></iframe>

Vom @[das_kfmw][3] wurde mir diese [Daft-Punk-Cover-Version](http://www.youtube.com/watch?v=fXP-7Do8uy4) zugespielt:

<iframe style="width:100%; height:360px; border:none;" src="//www.youtube.com/embed/fXP-7Do8uy4" allowfullscreen></iframe>

Ebenfalls über Twitter kam [dieser entspannte Track](https://soundcloud.com/trans_mitter/zugvoegel) (Danke @jimBasta!):

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/86652821"></iframe>

Nicht ganz Fra<ins>k</ins>tus, [kommt aber nahe ran](http://www.youtube.com/watch?v=THh2SCdnvpA):

<iframe style="width:100%; height:360px; border:none;" src="//www.youtube.com/embed/THh2SCdnvpA" allowfullscreen></iframe>

Mal was Relaxtes — Dubstep.NET bringt immer wieder [ein paar Perlen](https://soundcloud.com/ganjawhitenight/ganja-white-night-mango) zu Tage:

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/87590572"></iframe>

Und da noch [ein alter Bekannter](https://soundcloud.com/paniq/astley) – Let's have some #[RickNRoll][4]!

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/77940907"></iframe>

## Und sonst so

- Die [Blogrebellen][5] haben mit der [Dubnight von Phil Harmony][6] - Live-Show-Zuwachs bekommen.  
- Bei [Bandcamp][7] ist alles neu und chic.  
- Sehr erwartungsvoll schaue ich auch auf [#music][8], da tut sich - aber [erst nächste Woche][9] richtig was.  
- Bei Anna-Lena könnt Ihr ab sofort die erlesenen Musik-Funde im [- Blog&Bleistift-Newsletter][10] abonnieren.

---

Und das Katzen-Foto der Woche kommt von [Gilly][11]. Danke!

[2]: https://netzpolitik.org/2013/wdr-computernacht-zum-zeitsouveranen-nachschauen/
[3]: http://www.kraftfuttermischwerk.de/blogg/?p=51462
[4]: http://de.wikipedia.org/wiki/Rickrolling
[5]: http://blog.rebellen.info/2013/04/12/neue-streamshow-dubnight-mit-phil-harmony-ab-heute-jeden-zweiten-freitag-im-monat/
[6]: http://blog.rebellen.info/2013/04/15/die-blogrebellen-taufe-dubnight-radioshow-vom-12-04-2013/
[7]: http://blog.bandcamp.com/2013/04/09/the-new-bandcamp-com/
[8]: https://music.twitter.com/
[9]: http://t3n.de/news/twitter-music-456983/
[10]: http://www.blogbleistift.de/newslettermusik
[11]: http://blog.gilly.ws/2013/04/11/cats-love-bass "Cats love Bass"

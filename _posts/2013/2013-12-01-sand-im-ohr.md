---
title: Sand im Ohr
description: Mama, ich hab Sand im Ohr!
layout: post
lang: de
tags:
 - music
 - compilation
categories:
 - music
comments: true
share: true
---

Ein Ohrwurm ist ein Ohrwurm ist ein Ohrwurm... In dieser Woche war [einer, den ich nicht mehr wegbekommen konnte](https://www.youtube.com/watch?v=VNUgsbKisp8).

## Mr. Sandman

![The Chordettes](/images/2013/12/the_chordettes.gif)  
[^gif]

Und der beste Ohrwurm ist _nichts_ wert, wenn er nicht weitergegeben wird. Während also von anderen Blogs aus mit Stöckchen geworfen wird, werfe ich mit Melodien.
<!--more-->

Eine [nette, kleine Bass-Musik](https://soundcloud.com/giovanniprestoni/mr-sandman-racecarbed-trap):

<iframe style="width:100%; height:166px; border:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/64156735"></iframe>

Die [Funk'n'Disco'n'Hop-Variante](https://soundcloud.com/owen-westlake/the-chordettes-mr-sandman-owen/download):

<iframe style="width:100%; height:166px; border:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/70937278"></iframe>

Oder in [buntem House-Kleidchen](https://soundcloud.com/allefarben/flapjacks-mister-sandman-alle)?

<iframe style="width:100%; height:166px; border:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/49376101"></iframe>

Wenn etwas weniger sein darf, [hier in Minimal](https://soundcloud.com/vfresh/mr-sandman-reedit-free/download):

<iframe style="width:100%; height:166px; border:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/82886555"></iframe>

Der [Bass Freestyle](https://soundcloud.com/mrnohands/the-chordettes-mr-sandman-mr-no-hands-wobbly-remix):

<iframe style="width:100%; height:166px; border:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/3942085"></iframe>

Der amtliche™ [Trap-Mix](https://soundcloud.com/mrvandaldub/the-chordettes-mr-sandman-mr) darf nicht fehlen:

<iframe style="width:100%; height:166px; border:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/94168378"></iframe>

…und wer den Song jetzt immer noch mag [^tumblr], sollte beim folgenden [Trash Noise](https://soundcloud.com/breakmaster-cylinder/mr-sandman-chordettes-remix/download) am besten weghören:

<iframe style="width:100%; height:166px; border:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/5339515"></iframe>

[^gif]: gif via [WFMU](http://blog.wfmu.org/freeform/2007/05/cleaning_out_my_1.html)
[^tumblr]: mehr Gesammeltes gibt's auf meinem [Tumblr](http://tumblr.tasmo.de/)

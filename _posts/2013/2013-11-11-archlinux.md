---
layout: post
title:  Zweiter Frühling mit SSD und Arch Linux
permalink: /archlinux/
categories: [selfish, tech stuff]
tags: [Linux, nerdity]
comments: true
share: true
image:
  feature: 2013/11/alsi.png
  credit: Thomas Friese (CC0)
modified: 2023-11-20T12:15:00+01:00
---

Neben einem Laptop einer viel zu populären Marke besitze ich auch ein kleines Schwarzes – ein ThinkPad Edge. Bis vor kurzen <del>lief</del> <ins>kroch</ins> es mit einer Festplatte auf der das immer aktuellste Ubuntu <i class="fa fa-linux"></i> Linux mit der für so kleine Laptops ganz angenehm zu bedienenden Oberfläche [Unity](http://unity.ubuntu.com/).

Es war jedoch – hauptsächlich der preiswerten, verbauten Festplatte geschuldet – sehr langsam. Angefangen vom Hochfahren bis die grafische Oberfläche zu sehen war, über das Starten von Programmen, das Aufrufen von Webseiten, geschweige denn das Ansehen von HD-Filmchen mit Hilfe von Flash oder HTML5. Ihr kennt das…
<!--more-->

Nachdem ich das eine Weile – also eine zweistellige Monatszahl – diese Langsamkeit mit sinkender Lust und steigendem Frust mehr angeschaut als benutzt habe, stand für mich fest:

### Es muss etwas geschehen

Das Notebook habe ich mit Absicht ohne irgendeine Lizenz eines proprietären <abbr title="MS Windows">Betriebssystem</abbr>s gekauft. Erstens ist es wesentlich billiger uns zweitens habe ich Lust, daran zu basteln. Mit Ubuntu lief alles von Anfang an glatt, bis auf das ständige, sekundenlangen Einfrieren meines Trackpads. Selbst Bluetooth, WLAN, Festplattenverschlüsselung und Sound liefen out of the box. Und trotzdem gibt es immer was zu basteln, wie das Konfigurieren der [Shell](http://grml.org/zsh/) oder des [Lieblingseditors](http://www.sublimetext.com/3dev). Aber Ubuntu war mir nicht nur langsam zu langweilig, es setzt auch immer mehr auf [Quasi-Standards](https://de.wikipedia.org/wiki/Ubuntu#Kritik), die nicht der Offenheit von Linux dienen. Obwohl es ja auf dem freien Debian Linux basiert.

![Arch Linux System Information tool ALSI](/images/2013/11/alsi.png)

Kurzum, davon will ich auch weg. Mal wieder alles selber machen; learning by doing. Dafür kommen [einige Linuxe](http://www.zegeniestudios.net/ldc/index.php?lang=de) oder UNIXe in Frage – [Gentoo](http://www.gentoo.org/), [Slackware](http://www.slackware.com/), das schon erwähnte [Debian](http://www.debian.org/) oder eben [Arch Linux](https://www.archlinux.de/). Ich habe mich für letzteres wegen der [guten Dokumentation](https://wiki.archlinux.org/) und der Unterstützung durch die [Community](https://bbs.archlinux.org/) entschieden, die mindestens genauso gut wie die von Ubuntu ist, als es _neu_ und _hip_ war.

## Neue Bestimmung (Nachtrag)

Der Laptop ist immer noch in Benutzung. Jetzt läuft er mit Fedora Linux und einem eingeschränkten Konto für das Kind. Fedora ist ziemlich stabil, leicht bedienbar und immer auf möglichst neuestem Stand.

Eingeschränkt ist das Konto nur im Verwalten der Systemeinstellungen. Bei allem, was andere Konten auf dem Rechner nicht wesentlich beeinträchtigen kann, setze ich eher auf Vertrauen und freie Entscheidung als auf Verbot.  
Drauf sind alle Basics installiert – Webbrowser, LibreOffice, Spiele, Rechner, Editoren und ein paar Spiele. Weitere Software ist einfach auf Nutzerebene über Flatpak nachinstallierbar.

* * *

Diesen Artikel habe [ich](/about/) mit meinem [Thinkpad](https://duckduckgo.com/?q=Lenovo+thinkpad+edge+11) im [Firefox](https://www.mozilla.org/de/firefox/new/) auf [Arch Linux](https://www.archlinux.org/) mit Hilfe von [Sublime Text](http://www.sublimetext.com/) in [Markdown](http://kramdown.rubyforge.org/quickref.html) geschrieben. \*
{: .small}
\*) Und jetzt mit VIM auf NixOS editiert. BTW: I used to use Arch.
{: .small}

*[populären Marke]: Appel
*[grafische Oberfläche]: GUI

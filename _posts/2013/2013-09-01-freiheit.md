---
title: Für Freiheit ohne Angst
layout: post
lang: de
permalink: /3664/freiheit/
categories:
  - opinion
tags:
  - action
  - Berlin
comments: true
share: true
image:
  feature: 2013/08/Vorratsdatenspeicherung_Kritik_02.jpg
  credit: ozeflyer (CC BY 2.0)
  creditlink: http://commons.wikimedia.org/wiki/File:Vorratsdatenspeicherung_Kritik_02.jpg
---

> **Freiheit** (lateinisch libertas) wird in der Regel verstanden als die Möglichkeit, ohne Zwang zwischen verschiedenen Möglichkeiten auswählen und entscheiden zu können. Der Begriff benennt in Philosophie und Recht der Moderne allgemein einen Zustand der Autonomie eines Subjekts.

So steht es in der [allwissenden Müllhalde][1]. Und weiter:

> **Angst** ist ein Grundgefühl, welches sich in als bedrohlich empfundenen Situationen als Besorgnis und unlustbetonte Erregung äußert. Auslöser können dabei erwartete Bedrohungen etwa der körperlichen Unversehrtheit, der Selbstachtung oder des Selbstbildes sein.

Am 7. September 2013 findet in Berlin die Großdemonstration “[Freiheit statt Angst][2]” statt. Treffpunkt dafür ist um 13.00 Uhr am <del datetime="2013-09-04T19:37:45+00:00">Potsdamer Platz</del> <ins datetime="2013-09-04T19:37:45+00:00">Alexanderplatz</ins>.
<!--more-->

![Freiheit statt Angst](/images/2013/08/Vorratsdatenspeicherung_Kritik_02.jpg)

## Welche Angst?

Das Internet gehört seit Jahren zum Lebensraum. Es hat sich zu einem wesentlichen Instrument für unsere Kommunikation entwickelt. Über das Internet teilen wir unsere intimsten Geheimnisse. Und es ist unabdingbar, dass sie geheim bleiben. Es ist wichtig, dass ich entscheide, mit wem ich was teile.

Freiheit einschränken wollen vor allem diejenigen, die Angst haben.

Wovor hat unser Staat Angst, warum möchte er meine Freiheit einschränken -- meine Freiheit selbst zu entscheiden, wie ich mich im <del datetime="2013-09-01T21:35:43+00:00">Leben</del> <ins datetime="2013-09-01T21:35:43+00:00">Internet</ins> bewege? Angst vor Terror? Oder ein Stückchen der Macht zu verlieren? Legitimiert diese Angst alle Mittel, meine Freiheit einzuschränken, mich zu überwachen, zu zensieren und zu bevormunden -- [alle unsere Daten zu speichern und auszuwerten][Vorratsdatenspeicherung]? Ist das Internet ein rechtsfreier Raum für die Nutzer\*innen zum Nutzen des Staates?

__Nein!__

Und darum gehe ich am 7. September mit auf die Straße. Ich möchte keine Angst vor meinem Staat haben müssen!

### Seid mit dabei!
  
Wer meint, dass ihn das alles nichts angeht, kann sich statt dessen seine Fähigkeit [als Diktator testen][3].

* * *

{: .small}
Foto "[Kritik an der Vorratsdatenspeicherung vor dem Reichstag]" by ozeflyer ([CC BY 2.0])

[1]: https://de.wikipedia.org/wiki/Freiheit
[2]: http://blog.freiheitstattangst.de/about/
[3]: http://www.diktatorcheck.de/test/
[Vorratsdatenspeicherung]: http://de.wikipedia.org/wiki/Vorratsdatenspeicherung
[Kritik an der Vorratsdatenspeicherung vor dem Reichstag]: http://commons.wikimedia.org/wiki/File:Vorratsdatenspeicherung_Kritik_02.jpg
[CC BY 2.0]: http://creativecommons.org/licenses/by/2.0/deed.de

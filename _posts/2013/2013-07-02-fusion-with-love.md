---
title: Fusion with Love
layout: post
lang: de
permalink: /3452/fusion-with-love/
categories:
  - music
tags:
  - DJ-Set
  - Tasmo
  - Festival
  - free download
comments: true
share: true
image:
  feature: 2013/07/artworks-000052520279-mpl3p5-t500x500.jpg
  credit: Thomas Friese (CC BY-NC-SA)
---

Das Wetter der [Фузион][1] war in diesem Jahr kalt und nass. Nicht so das Festival an sich. Denn das musikalische Angebot ist immer wieder erschlagend vielfältig und schön.

Unerwartet durfte ich auch in diesem Jahr wieder in der DuBSTATION auf dem Fusion-Festival einen [ausgesuchten Teil meiner Lieblingsmusik][2] spielen. Das schönste daran ist, dass viele diesen Geschmack mit mir teilen und wir gemeinsam drei sehr schöne Stunden verbrachten:  

<iframe id="hearthis_at_track_41414" style="width:100%;height:130px;overflow:hidden;border-style:none;background:transparent;" src="https://hearthis.at/embed/41414/transparent/?hcolor=&color=&style=2&block_size=1&block_space=0&background=0&waveform=0&cover=0&autoplay=0&css="></iframe>
Listen to [For Фузион with ♥ – Dubstation at Fusion Festival 2013 3am](https://hearthis.at/tasmo/for-with-dubstation-at-fusion-festival-2013-3am/) by [tasmo](https://hearthis.at/tasmo/).
{: .small}
<!--more-->

([Download-Link](https://hearthis.at/tasmo/for-with-dubstation-at-fusion-festival-2013-3am/download/){: .download})
{: .small}

Auf dem Festival habe ich viele, tolle Acts gesehen. Erwähnen möchte ich vor allem das legendäre [Channel One Soundsystem][3], die viel gute Laune in Form von Dancehall mit viel Bass auf dem Soundsystem in die Roots Base brachten. Meine rebellische Jugend haben [Slime][4] wieder aufleben lassen. Den krönenden, gröhlenden Abschluss auf der gleichen Hangar-Bühne zelebrierten die [Ohrbooten][5]. Das wunderschöne [Orchestre Tout Puissant Marcel Duchamp][6] brachte das Luftschloss zum swingen und tanzen und [Joanna Gemma Auguri][7] spielte den Soundtrack am Lagerfeuer in der DuBSTATION.

[Symbiz Sound][8] rockten den Salon de Baille mit erwartet fettem Bass. Wenn ich mir aber was wünschen darf: Symbiz, spielt Eure Tracks länger, als nur Drop an Drop. Ich möchte gern zu Eurem Sound tanzen und nicht nur warten und staunen.  
Zion Train hätte ich lieber nicht live gesehen. Mit einem steten und nervenden Delay-Loop und hörbar angetrunkenen, an den Tonarten vorbei treffenden Sänger waren sie alles andere als ein schönes Erlebnis.

Bedanken möchte ich mich bei denen, deretwegen ich auch bei dieser Fusion wieder dabei sein durfte: [Saetchmo][9] und dem DubFish, den [Blogrebellen][10], dem DuBSTATION e.V. in Kaulitz, dem Kulturkosmos Müritz e.V. und bei Euch, die Ihr mit mir seid!

<figure class='third'>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130630-05.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130630-05.jpg" alt="Фузион" /></a>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130627-01.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130627-01.jpg" alt="Channel One Soundsystem" /></a>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130627-02.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130627-02.jpg" alt="DUB" /></a>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130628-03.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130628-03.jpg" alt="Stimmung bei Slime" /></a>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130628-04.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130628-04.jpg" alt="Stimmung bei Slime" /></a>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130630-07.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130630-07.jpg" alt="Tanzwiese" /></a>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130630-09.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130630-09.jpg" alt="Seebühne" /></a>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130630-08.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130630-08.jpg" alt="Seebühne" /></a>
	<a rel='attachment' href='/images/2013/07/Fusion2013-20130630-06.jpg'><img width="150" height="150" src="/images/2013/07/Fusion2013-20130630-06.jpg" alt="Clownfisch" /></a>
<figcaption>Фузион 2013</figcaption>
</figure>

Wer noch mehr möchte, kann [mein Set in der DuBSTATION von 2012][11] hören und [das von 2011][12]. Und da andere DJs auch tolle Sets spielen, hat der Ronny ein paar schöne [hüben][13] und "Jeden Tag ein Set" [drüben][14] zusammengefasst.

### Tracklist: {#tracklist_fusion_2013}

1. [No Body][15] by **Fed.**
1. Junglist (feat. Peter Bouncer) by **Blackstar**
1. [3D][16] by **Indidginus**
1. [23 deegres][17] by **Naydi**
1. [Bad (Donkong Remix)][18] by **Michael Jackson**
1. [Hand Gestures (Sunchase & NickBee Remix)][19] by **Noisia**
1. [Crystalline][20] by **Noisia & Hybris**
1. [Dominate][21] by **R.O**
1. [Oxygen][22] by **Taelimb & Conscience**
1. [Untitled Bass][23] by **Masta Junga**
1. MONSTA by **R.O**
1. Make It Bun Dem (MBD Mix 1 day 2 b MSTR) by **Skrillex & Damian Marley**
1. [Bun Dem (Beatport Remix)][24] by **R.O**
1. [Rest Nest][25] by **MUST DIE!**
1. [Isis][26] by **Seven Lions**
1. [The Tube (VIP MIX)][27] by **TiKay One**
1. [Oie tu! la de la cachuchita][28] by **Cero39**
1. [Nu Sound][29] by **Zinc**
1. Babylon your days are numbered by **Dub Terminator & Ras Stone**
1. [Blurred Vision][30] by **Elonious**
1. [Optimus VS Megatron][31] by **R.O**
1. Alpha Centauri by **Noisia**
1. [Marka][32] by **Strategy, Dub Phizix, Skeptical**
1. [Caxito][33] by **JSTJR x Two Sev x Banginclude**
1. [Chillaxin' by the sea (R.O remix)][34] by **Gramatik**
1. [King of my Castle RMX][35] by **B.Visible**
1. [Feeling The Blues][36] by **Lidsky**
1. [Bluestep][37] by **Gramatik**
1. Eris by **Starkey**
1. [Forbidden][38] by **FNC**
1. [Ghost][39] by **Shu**
1. [Within Ourselves][23] by **Ziplokk**
1. [Up and Down][23] by **TheNonWho**
1. Mutual Core (BHK Plates Remix) by **Björk**
1. [Legalize it!][40] by **Buddha**
1. [Robot Love][41] by **R.O**
1. [Mango][42] by **Ganja White Night**
1. [To Conclude….][43] by **Gold Top**
1. Nana (The Dreaming) (Fish Finger Remixed & Reedited) by **Sheila Chandra**
1. Eleanor Rigby (C-G's Lonely People Stripped & Re-Dressed Edit) by **The Beatles**
1. [Stairway To Hip-Hop Heaven (Gramatik Remix)][44] by **Led Zeppelin**
1. [Busted Up][45] by **Staunch**
1. [Rocks My Soul][36] (Rodway Remix) by **Lidsky**
1. [Stay Strong][46] by **JP Balboa**
1. [I Got Love (ENiGMA Dubz Remix)][47] by **Nate Dogg**
1. [DuB or DiE][48] by **Corty**
1. [New World Order Dub Transition – Part II][49] by **Corty**
1. [state of emergency (rob smith dub version)][50] by **iLLBiLLY HiTEC**
1. Javanaise Remake (Men Will Deceive You) by **Brady, LMJ**
1. [Aint No Sunshine (Jack Frost Edit)][51] by **Horace Andy**
1. [Wann strahlst Du?](https://www.youtube.com/watch?v=KDD5o630vVc) by **Erobique & Jacques Palminger**

[1]: /tag/fusion/ "Fusion @ tasmo[de~]"
[2]: #tracklist_fusion_2013
[3]: http://channelonesoundsystem.com/
[4]: https://www.google.com/search?q=slime&safe=off&tbm=vid
[5]: http://www.ohrbooten.de/
[6]: http://otpmd.bandcamp.com/
[7]: https://soundcloud.com/joannagemmaauguri
[8]: http://symbiz-sound.de/
[9]: http://saetchmo.tumblr.com/
[10]: http://blog.rebellen.info/
[11]: /1288/tom-freeze-dubstation/ "Tom Freeze @ Dubstation (Фузион 2012)"
[12]: /402/dubstation9pm/ "Dubstation 9pm @ Фузион #15"
[13]: http://www.kaputtmutterfischwerk.de/?p=2144
[14]: http://jedentageinset.wordpress.com/2013/07/02/fusion-festival-2013-die-sets/
[15]: https://soundcloud.com/mad_fed_head/fed-no-body-demo/download
[16]: http://indidginus.bandcamp.com/
[17]: http://www.mu-sick.de/
[18]: https://soundcloud.com/donkong/michael-jackson-bad-donkong
[19]: http://noisia.nl/gold
[20]: https://soundcloud.com/noisia/noisia-hybris-crystalline
[21]: https://soundcloud.com/olivierugi/r-o-dominate-click-buy-for/download
[22]: https://soundcloud.com/taelimb/oxygen-taelimb-and-conscience
[23]: http://subbass.bandcamp.com
[24]: https://soundcloud.com/olivierugi/skrillex-ft-damian-marley-make
[25]: https://soundcloud.com/mustdiemusic/must-die-rest-nest-preview/download
[26]: https://soundcloud.com/seven-lions/seven-lions-isis/download
[27]: https://soundcloud.com/tikayone/tube-vip/download
[28]: https://soundcloud.com/polenrecords/cachuchita/download
[29]: https://soundcloud.com/zinc/zinc-nu-sound-crack-house-ep1/download
[30]: https://soundcloud.com/elonious-funk/blurred-vision-by-elonious/download
[31]: https://soundcloud.com/olivierugi/r-o-optimus-vs-megatron/download
[32]: https://vimeo.com/41588919
[33]: https://soundcloud.com/jstjr/jstjr-x-two-sev-x-banginclude/download
[34]: https://soundcloud.com/olivierugi/gramatik-chillaxin-by-the-sea/download
[35]: https://soundcloud.com/b-vsbl/b-visible-king-of-my-castle
[36]: http://lidsky.bandcamp.com
[37]: http://www.youtube.com/watch?v=HShoVk4jsKE
[38]: https://soundcloud.com/fnc_uk/fnc-forbidden
[39]: http://meditationbassmusic.blogspot.de
[40]: https://soundcloud.com/ganjamedia/bert-legalize-it-dubstep-remix
[41]: https://soundcloud.com/olivierugi/r-o-robot-love/download
[42]: https://soundcloud.com/ganjawhitenight/ganja-white-night-mango/download
[43]: https://soundcloud.com/dubstep/to-conclude-by-gold-top/download
[44]: http://www.youtube.com/watch?v=K0pxDoJ6e6k
[45]: http://hopskotch.bandcamp.com
[46]: http://pragmatictheory.bandcamp.com/track/jp-balboa-stay-strong
[47]: https://soundcloud.com/enigmadubz/dank007-nate-dogg-i-got-love/download
[48]: https://soundcloud.com/corty/dub-or-die-full-dub-version/download
[49]: http://angeldustrecords.bandcamp.com
[50]: https://soundcloud.com/echo-beach-lifefidelity/illbilly-hitec-state-of
[51]: https://soundcloud.com/jack-frost/horace-andy-aint-no-sunshine/download

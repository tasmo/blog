---
title: Happy Birthday, WordPress!
layout: post
lang: de
permalink: /3350/happy-birthday-wordpress/
categories:
  - meta
tags:
  - blog
  - OpenSource
comments: true
share: true
image:
  feature: 2013/05/5763826841_47c7a7d615_o.jpg
  credit: Naoko Takano
  creditlink: http://flic.kr/p/9Mk7UR
---

Vor zehn Jahren wurde [die erste Version von WordPress][2] aus dem Blogging-System [b2][3] geklont. Seitdem wuchs rundherum eine Community, die WordPress zu dem hervorragenden und erfolgreichen Tool für Blogger gemacht hat.

Dieses bescheidene Blog <del>läuft</del> <ins>lief</ins> auch mit der Kraft von WordPress. Also ein **herzliches Dankeschön** an alle, die mitgeholfen haben, [Themes][4] und [Plugins][5] beizutragen, [Tipps][6] zu geben, [Bugs][7] zu beheben und [weiter zu entwickeln][8]!

[2]: http://web.archive.org/web/20030618021947/http://wordpress.org/
[3]: http://www.cafelog.com/ "b2"
[4]: http://wordpress.org/themes/ "Wordpress Themes"
[5]: http://wordpress.org/plugins/ "Wordpress Plugin Directory"
[6]: http://wordpress.org/support/
[7]: http://core.trac.wordpress.org/
[8]: http://make.wordpress.org/ "Making WordPress"

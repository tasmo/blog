---
title: Bondage Picknick
layout: post
lang: de
permalink: /3262/bondage-picknick/
categories:
  - Foto
tags:
  - Berlin
  - Kreuzberg
  - richtig wichtig
format: image
comments: true
share: true
---

[![Bondage Picknick at Markthalle IX](/images/2013/04/8686930276_ed30e8ccac_o.jpg)](/images/2013/04/8686930276_ed30e8ccac_o.jpg)
[Bondage Picknick at Markthalle IX][2]

Die [Markthalle IX][3] in Berlin Kreuzberg ist am Freitag und Samstag Herberge für einen kleinen Markt. Der hat im Laufe der letzten Jahre eine eigene Gentrifizierung erhalten – die blillig-Klamotten und das Gewächshaus-Obst sind jetzt teuren Käsen, sizilianischen Ölen und Bio-Blumen gewichen.
<!--more-->

Neben dem Stand mit veganen Burgern (bis auf das Brötchen auf der Unter- und Oberseite hat die Gesamtspeise nur noch wenig Ähnlichkeit mit einem Hamburger), die von zwei harmonisch, ganzheitlich lächelnden Damen zubereitet werden, gibt es Alltags- und Wochenendgegenstände für besser betuchte Rustikalliebhaber. Wo auch dieser Piknik-Korb ausgestellt ist.

[2]: http://www.flickr.com/photos/tasmo/8686930276
[3]: http://www.markthalleneun.de/

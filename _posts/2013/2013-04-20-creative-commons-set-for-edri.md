---
title: Creative Commons set for EDRi
layout: post
permalink: /3218/creative-commons-set-for-edri/
categories:
  - music
tags:
  - Creative Commons
  - copyright
  - DJ-Set
  - Tasmo
image:
  feature: 2013/04/EDRi.jpg
  credit: aprica
  creditlink: https://twitter.com/intent/user?screen_name=aprica
comments: true
share: true
---

On March 2nd, EDRi celebrated its [10th Anniversary][1] in Brussels at the bar London Calling.

[About EDRi][2]:

> European Digital Rights was founded in June 2002. Currently 35 privacy and civil rights organisations have EDRi membership. They are based or have offices in 21 different countries in Europe. Members of European Digital Rights have joined forces to defend civil rights in the information society. The need for cooperation among organisations active in Europe is increasing as more regulation regarding the internet, copyright and privacy is originating from European institutions, or from international institutions with strong impact in Europe.

## No Music no party {#nomusicnoparty}

[<img class="pull-right" alt="Musik" src="/images/2013/04/2013-03-01-15.57.48-1.jpg" width="150" />][3]A good party just needs people, drinks and music for the people to dance and celebrate.

For EDRi, of course we decided only to use free music — that is certified free. Because we care about music and musical rights we have already collected tracks released under a [Creative Commons license][4].

## Celebration Time {#celebrationtime}

In the bar/lounge [DJ MEP Ehrenhauser & friends][5] played the tunes while in the basement the party took off with sounds from the Creative Commons/[Blogrebellen][6]/[Progolog][7] crew consisting of @[CptPudding][8], @[Withoutfield][9] & @[\_Tasmo][10].

We all together had a lot of fun -- **happy birthday, EDRi!**
<!--more-->

<iframe style="width:100%; height:120px; overflow:hidden; border-style:none;" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&light=1&feed=%2Fblogrebellen%2Fedri-10th-anniversary-party-cc-mixtape%2F"></iframe>

Please excuse our small technical problems with the recording!

Track list with download links is coming very soon.

---

Thanks @[aprica](https://twitter.com/intent/user?screen_name=aprica) for the title pic!
{: .small}

*[EDRi]: European Digital Rights

[1]: http://edri.org/edrigram/number11.5/edri-at-10th-anniversary
[2]: http://edri.org/about
[3]: /images/2013/04/2013-03-01-15.57.48-1.jpg
[4]: http://creativecommons.org/
[5]: http://www.ehrenhauser.at/
[6]: http://blog.rebellen.info/2013/02/28/edri-feiert-mit-den-blogrebellen-den-10-geburtstag-in-brussel/
[7]: http://www.progolog.de/2013/03/edri-10th-anniversary-party-cc-mixtape/
[8]: https://twitter.com/intent/user?screen_name=CptPudding
[9]: https://twitter.com/intent/user?screen_name=ithoutfield
[10]: https://twitter.com/intent/user?screen_name=_Tasmo

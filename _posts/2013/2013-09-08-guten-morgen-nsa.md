---
title: Guten Morgen, NSA!
layout: post
lang: de
permalink: /3677/guten-morgen-nsa/
categories:
  - findings
tags:
  - Hip Hop
  - Video
comments: true
share: true
---

Auf der "[Freiheit statt Angst][1]"-Demo habe ich diesen Song mehrmals gehört und für gut befunden.

Das Video wirkt auf mich etwas wie das eines anderen, auch berühmten Berliner Rappers, nur der Text ist um Längen besser. Einen Kauf- oder Download-Link habe ich noch nicht gefunden. Kommt bestimmt bald, schließlich wurde das Video in nur ein paar Tagen mehrere Hunderttausendmale geklickt.

[MoTrip ft. Elmo -- Guten Morgen NSA ![MoTrip ft. Elmo -- Guten Morgen NSA](/images/2013/09/Guten-Morgen-NSA.png)](https://www.youtube.com/watch?v=HcDgEtET1Dw)

[1]: http://blog.freiheitstattangst.de/about/

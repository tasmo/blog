---
title: Nichts zu verbergen / unbequeme Freiheit
layout: post
lang: de
permalink: /3640/nichts-zu-verbergen/
categories:
  - tech
tags:
  - Android
  - GPG
  - howto
  - privacy
  - security
  - SSL
  - Verschlüsselung
image:
  feature: 2013/08/image-510682-galleryV9-dmmn.jpg
  credit: Reuters
comments: true
share: true
---

Ich genieße den Luxus der viele Features im Netz der Netze, mit deren Hilfe ich mich mit meinen Freunden unterhalten, Projekte planen und Gedanken teilen kann. Es ist toll, dass ich zum Beispiel Fotos in dieses Netz laden und mit allen, mit denen ich möchte, teilen kann.

[![Nichts zu verbergen](/images/2013/08/nichts_zu_verbergen_toilette_balken.jpg){: .pull-left width="150" height="150"}][1]
Und doch will ich einiges verbergen – nicht vor jeder und jedem, aber vor vielen. Auf Anhieb fallen mir zwei Hände voll Beispiele ein, die mir das Blut vor Scham in den Kopf steigen ließen, müsste ich sie öffentlich ausbreiten. Mir ist es unangenehm, zu ahnen, jede oder jeder könnte alles über mich herausfinden, ohne dass ich weiß, wer sie oder er ist und warum sie oder er alles über mich wissen möchte. Das ist nicht paranoid – denke ich.
<!--more-->

## Hand vorm Mund

Soll es denn privater sein und nur einzelne, ausgesuchte Kommunikationspartner\*innen meine Botschaften verstehen, nutze ich Verschlüsselung. Zum Chatten über Jabber hat sich OTR bewährt. Es ist in vielen Chat-Programmen implementiert und so für die meisten Smartphones, Tabletts und Desktops verfügbar. Für PCs gibt es da u.a. [Pidgin][2] oder das Mac-Pendant [Adium][3], für Adroid-Geräte [Xabber][4] oder [Gibberbot][5] und für das Yuppie-OS™ [ChatSecure][6].

OTR kommt mit einem Schlüsselaustausch im Hintergrund klar, ohne dass ich mich groß drum kümmern muss. Es hat aber den Haken, dass es nicht *offline* funktioniert. Das heißt, der Chat ist nur dann verschlüsselt, wenn beide Teilnehmerinnen erreichbar sind. Zwischenspeichern und später zustellen ist dafür nicht vorgesehen. Ein paar Chat-Programme beherrschen auch die Verschlüsselung via GPG – wie das geht, [ist weiter unten beschrieben][7].

Für die neuen, tragbaren Hosentaschencomputer gibt es auch komfortablere Lösungen wie z.B. [Threema][8], womit Verschlüsselung noch einfacher ist. Allerdings auch mit Schwächen, [wie Torsten Kleinz findet][9].

**Update**: Der Kommentar von [Mythmaker] hat mich erinnert, dass es unter Android ebenso einfach ist, verschlüsselte SMS zu senden und Telefonate zu führen. Die Programme dafür sind [TextSecure][10] und [RedPhone][11], welches dafür einen VoIP-Kanal benutzt.

Email ist nach wie vor eine der wichtigsten Kommunikationsformen. Sowohl in persönlichem wie auch geschäftlichen Kontakten findet darüber viel Kommunikation statt. Dabei werden die Großzahl an Emails unverschlüsselt als Klartext gesendet, trotz des hohen Gehalts an privaten, geschäftlichen Informationen und auch Daten Dritter und Unwissender. Zwar setzen die meisten Email-Service-Anbieter mittlerweile standardmäßig auf SSL-Zertifikaten basierte Verschlüsselung der Verbindung von den Nutzer\*innen zum Server ein, doch ab da werden die Emails weiterhin klar lesbar über das Internet verschickt. Dazu liegen sie auch unverschlüsselt auf den Servern der Anbieter.

Damit die Emails nicht für andere einsichtbar sind, hilft eine End-to-End-Verschlüsselung. Dabei wird die Email vor dem Absenden kodiert und beim Lesen erst wieder entschlüsselt – ist also den gesamten Transportweg über nicht lesbar.

## Email mit GPG

Die verbreitetste Methode dafür ist PGP oder besser das freie GPG.  
Dabei hat jede Teilnehmerin einen privaten und einen öffentlichen Schlüssel. Der öffentliche Schlüssel des Absenders wird zum Verschlüsseln der Nachricht genutzt, mit dem privaten die eigenen Nachrichten entschlüsselt. Üblicherweise werden die öffentlichen Schlüssel auf Key-Server geladen oder anderweitig suchbar ins Netzt gestellt. [Meinen könnt Ihr hier finden][12].

Das ist es schon für die meisten meiner Kontakte zu unbequem und aufwendig. und so benutzen sehr wenige wirklich Verschlüsselung für Email. Und das ist noch nicht einmal alles.

Um GPG verwenden zu können, sind noch ein paar Schritte mehr notwendig. Das [Hauptprogramm muss meist noch installiert][13], die Schlüssel erzeugt und veröffentlicht werden. Dann sollte noch das auserwählte Email-Programm verstehen, dass der Text da gerade noch entschlüsselt werden muss und vor allem womit. Plugins gibt es für die bekanntesten Programme am Computer wie [Thunderbird][14], [Apple Mail][15] oder [MS Outlook][16]. Mit mobilen Geräten und den meisten Webmail-Seiten (also Email direkt im Browser) sieht es schon trüber aus.

Natürlich gibt es Hoffnungsschimmer. Das Bewusstsein nach und nach zu bei mehr Nutzer\*innen zu. Je mehr verschlüsseln wollen, umso lohnenswerter ist es, passende Software und Services zu entwickeln. Der Service [Posteo][17] geht in die richtige Richtung. Und der von Nutzer\*innen finanzierte Web-Mailer [Mailpile][18] hat sich viel Gutes vorgenommen.

Für Android gibt es [APG][19] als Ent- und Verschlüsselung für die tolle Email-App [K9][20]. Für iOS habe ich [oPenGP][21] gefunden, konnte es aber nicht selbst testen.

## Und jetzt?

Es gibt verständliche Wege, Verschlüsselung zu nutzen – wenn der Wunsch da ist. Zum einen scheuen einige den Aufwand, andere meinen, es gehöre zum guten Ruf, nichts verbergen zu wollen. Solange das eine weit verbreitete Ansicht ist, wird es nicht Standard sein, verschlüsselt zu kommunizieren und nur das, was explizit nicht verborgen bleiben soll, in das Netz zu schreiben.

Sicher ist nichts sicher. Es gibt immer einen mehr oder weniger aufwendigen Weg, an Informationen zu kommen, die ausschließlich für andere bestimmt sind. Im [Kleinen][22] sowie im [Großen][23]. Ich möchte doch weiter versuchen, dem systematischen [Misstrauen][24] und [Aushorchen][25] etwas entgegen zu setzen.

* * *

{: .small}
Fotos: Toilettenmann via [TerLiMa](http://terlima-news.blogspot.de/2013/06/uberwachungsprogramm-prism-ich-hab-ja.html)

[1]: /images/2013/08/nichts_zu_verbergen_toilette_balken.jpg
[2]: http://pidgin.im/
[3]: http://www.adium.im/
[4]: https://play.google.com/store/apps/details?id=com.xabber.android
[5]: https://play.google.com/store/apps/details?id=info.guardianproject.otr.app.im
[6]: https://chatsecure.org/
[7]: #email_mit_gpg
[8]: http://threema.ch/
[9]: https://plus.google.com/u/0/107223467325602754395/posts/LmeiWcX843W
[10]: https://github.com/WhisperSystems/TextSecure/blob/master/README.md
[11]: https://github.com/WhisperSystems/RedPhone/blob/master/README.md
[12]: /about/impress/contact/ "Kontakt / contact"
[13]: http://gnupg.org/howtos/de/GPGMiniHowto-2.html
[14]: https://www.enigmail.net/
[15]: https://gpgtools.org/
[16]: http://gpg4win.org/
[17]: https://posteo.de/
[18]: http://www.indiegogo.com/projects/mailpile-taking-e-mail-back
[19]: http://www.thialfihar.org/projects/apg/
[20]: https://play.google.com/store/apps/details?id=com.fsck.k9
[21]: https://itunes.apple.com/de/app/id405279153#
[22]: http://www.erdbeerlounge.de/beichten/Heimlich-SMS-gelesen-_t375747s1
[23]: http://www.theguardian.com/world/prism
[24]: http://www.youtube.com/watch?v=VZxd8w11YSA
[25]: https://www.youtube.com/watch?v=qPOHLD2i9hE&list=PL1AB9505F7B3BCD76
[Mythmaker]: https://postpoeia.name/blogs/mytho

*[Jabber]: XMPP
*[OTR]: Off-the-Record Messaging
*[VoIP]: Voice over IP
*[PGP]: Pretty Good Privacy
*[GPG]: The Gnu Privacy Guard
*[iOS]: iPhone OS

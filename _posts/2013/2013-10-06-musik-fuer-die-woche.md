---
layout: post
lang: de
title:  Musik für die Woche
permalink: /3706/musik-fur-die-woche/
categories:
 - musik
tags: 
 - compilation
 - music
 - free download
comments: true
share: true
modified: 2021-11-04T15:10:00+02:00
---

![Frances and Her Rabbit!](/images/2013/10/Frances_and_Her_Rabbit.gif){: .fullwidth}[^1]

Den funky Leckerbissen der Woche habe ich auf Soundcloud gefunden. _scratchandsniff_ hat [The World von _Charles Bradley_ re-rubbed](https://soundcloud.com/scratchandsniff/charles-bradley-the-world):

{: .small}
Update: Weil der Track nicht mehr da ist, hier ein anderer.

[![Charles Bradley - No Time For Dreaming (scratchandsniff re-rub)](/images/2013/charles-bradley.jpg)](https://www.youtube.com/watch?v=-eiQEA6JV5E)

In dieser Woche bin ich auch über Soundcloud auf die herrlichen Remixes von [_LuQuS_](https://soundcloud.com/lukas-2) gestoßen. Hier ein schönes Beispiel:
<!--more-->

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F36179941&amp;color=666666&amp;auto_play=false&amp;show_artwork=true"></iframe>

Das _Tru Thoughts_-Label hat die schön entspannte Zusammenstellung [FREE COMPILATION: 15 Tracks](http://www.tru-thoughts.co.uk/news/article/free-compilation-15-tracks) zum freien Herunterladen veröffentlicht. unter diesen Perlen ist das tolle Stück Dinosaurs von _Bonobo_:

<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=1230058287/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/track=1546999189/transparent=true/" seamless><a href="https://tru-thoughts.bandcamp.com/album/animal-magic">Animal Magic by Bonobo</a></iframe>

Dieser Track hat mich schlagartig an einen meiner absoluten Lieblingskünstler erinnert – _Amon Tobin_. Auf Bandcamp hat er wiederum [seine Favouriten zusammengestellt](https://amontobin.bandcamp.com/album/featured-tracks):

<small>Update: Das Album existiert leider nicht mehr. Deshalb jetzt eine andere, schöne Compilation.</small>

<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=4127743069/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="https://music.amontobin.com/album/verbal-remixes-collaborations">Verbal Remixes &amp; Collaborations by Amon Tobin</a></iframe>

Für Menschen mit einer Voliebe für drockenen Drum'n'Bass hat [Mururoar](http://mururoar.com/blog/?p=12793) den [Neosignal Recordings Podcast Volume 007](https://soundcloud.com/phace/phace_podcast_007) von _Phace_ gediggt:

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F108659090&amp;color=666666&amp;auto_play=false&amp;show_artwork=true"></iframe>

Trap und Dubstep können nerven – keine Frage. Doch die [#1 SKANKA EP](http://freqnasty.bandcamp.com/album/1-skanka-ep) von _FreQ Nasty_ ist fat:

<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=430584018/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="https://freqnasty.bandcamp.com/album/1-skanka-ep-wallpapers-of-artwork">#1 SKANKA EP (+Wallpapers of Artwork) by FreQ Nasty</a></iframe>

Ihr kennt _Led Zeppelin_? Aber vielleicht kennt Ihr _DJ Schmolli_ noch nicht:

> Remix des Tages: Rock of Ages von DJ Schmolli [youtu.be/K-8Rd9QBlJI](https://youtu.be/K-8Rd9QBlJI) -- Dein Lieblingsremix? Hier entlang: [rechtaufremix.org/petition](https://rechtaufremix.org/petition)
> 
> --- Right 2 Remix (@right2remix) [October 3, 2013](https://twitter.com/right2remix/statuses/385779302064607233)
{: .twitter-tweet}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Und noch ein Remix :) [Sexual Healing Remix From _DIRTY-SYNTHESIZER_](https://soundcloud.com/sexy-synthesizer/sexual-healing-remix-from):

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F41440931&amp;color=666666&amp;auto_play=false&amp;show_artwork=true"></iframe>

Zu guter Letzt ein Lese-Tipp: [Warum uns komprimierter Digitalklang so nervt](http://www.welt.de/wissenschaft/article120646901/Warum-uns-komprimierter-Digitalklang-so-nervt.html)

[^1]: GIF: [okkult motion pictures](http://okkultmotionpictures.tumblr.com/post/56593401367/frances-and-her-rabbit)

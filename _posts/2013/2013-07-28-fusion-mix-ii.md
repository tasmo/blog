---
title: 'Da war noch was… (Fusion-Mix II)'
layout: post
lang: de
permalink: /3612/fusion-mix-ii/
categories:
  - music
tags:
  - Creative Commons
  - DJ-Set
  - free download
  - Festival
  - GEMAfrei
  - Tasmo
format: video
comments: true
share: true
---

Auf dem Fusion Festival 2013 hatte ich das Glück gleich zwei mal Musik spielen zu dürfen.  
Nachdem ich am Samstagmorgen [in der Dubstation in den Sonnenaufgang][1] spielte, startete abends mein erstes [Video-Remix-Projekt][2] im Rahmen des Workshop-Programms [Spring Lessons -- Musiksehen Schwarzweiss][3]. Und danach gab es noch ein paar entspannte töne von mir:

<iframe id="hearthis_at_track_770202" style="width:100%;height:130px;overflow:hidden;border-style:none;background:transparent;" src="https://hearthis.at/embed/770202/transparent/?hcolor=&color=&style=2&block_size=1&block_space=0&background=0&waveform=0&cover=0&autoplay=0&css="></iframe>
Listen to [Musiksehen Schwarzweiss – Fusion 2013 Workshop Area](https://hearthis.at/tasmo/musiksehen-schwarzweiss-fusion/) by [tasmo](https://hearthis.at/tasmo/).
{: .small}

## Tracklist:
<!--more-->

1. [Sassanid][4] by **Thousand Yard Prayer** <small>(<[CC BY-NC-ND 3.0])</small>
2. [Princess][5] by **FLeCK** <small>(all rights reserved)</small>
3. [Thoda Resham Lagta Hai][6] (Daniel Haaksman Remix) by **Latah Mangeshkar** <small>(all rights reserved)</small>
4. [Begin][7] by **Je$u$** <small>([CC BY-NC-SA 3.0])</small>
5. [Caxito][8] by **JSTJR x Two Sev x Banginclude** <small>(all rights reserved)</small>
6. [Amal][9] by **Speciez** <small>([CC BY-NC-ND 3.0])</small>
7. [Loaded][10] (planet 4 dub) by **Yaskah** <small>([CC BY-NC-ND 3.0])</small>
8. [On The Wing Of A Green Dragon][11] by **Rotem Moav** <small>([CC BY-NC-ND 3.0])</small>
9. [Oti Igbekele][12] by **Umoja** <small>(all rights reserved)</small>
10. [Smoke Riddim][13] by **Sub Swara** <small>(all rights reserved)</small>
11. [Ungu-Jah][14] by **DubRaJah** <small>([CC BY-NC-ND 3.0])</small>
12. [The Center][15] (Dub Vortex Mix) by **Beatfarmer** <small>([CC BY-NC-ND 3.0])</small>
13. [Without You][16] (Hard Mix Remix) by **Rainbow Arabia** <small>(all rights reserved)</small>
14. [Stealth Camel][17] by **Secret Archives of the Vatican** <small>([CC BY-NC-ND 3.0])</small>
15. [Basscharma][18] by **Ethnicalvibes** <small>([CC BY-NC-ND 3.0])</small>
16. [Nana (The Dreaming) (Fish Finger Remix)][19] by **Sheila Chandra** <small>(all rights reserved)</small>
17. [Mango][20] by **Ganja White Night** <small>(all rights reserved)</small>
18. [Kimi Dake][21] by **OverHertz** <small>(all rights reserved)</small>

[1]: /3452/fusion-with-love/ "Fusion with Love"
[2]: /3436/mein-erstes-video-remix-projekt/ "Mein erstes Video-Remix-Projekt"
[3]: http://caramk.blogspot.de/2013/07/spring-lessons-musiksehen-black-and.html
[4]: http://subbass.bandcamp.com/track/thousand-yard-prayer-sassanid
[5]: https://soundcloud.com/fleckathens/fleck-princess/download
[6]: https://soundcloud.com/daniel-haaksman/latah-rangeshkar-thoda-resham/download
[7]: http://gergaz.bandcamp.com/album/schleifen
[8]: https://soundcloud.com/jstjr/jstjr-x-two-sev-x-banginclude/download
[9]: http://subbass.bandcamp.com/track/speciez-amal
[10]: http://subbass.bandcamp.com/track/yaskah-loaded-planet-4-dub
[11]: http://subbass.bandcamp.com/track/rotem-moav-on-the-wing-of-a-green-dragon
[12]: http://inirecordings.bandcamp.com/album/je-ka-lo
[13]: http://www.xlr8r.com/mp3/2012/04/smoke-riddim
[14]: http://freemusicarchive.org/music/DubRaJah/Ethnic_Profound/5_-_DubRaJah_-_Ungu-Jah
[15]: http://subbass.bandcamp.com/track/beatfarmer-the-center-dub-vortex-mix
[16]: http://www.xlr8r.com/mp3/2011/03/without-you-hard-mix-remix
[17]: http://subbass.bandcamp.com/track/secret-archives-of-the-vatican-stealth-camel
[18]: http://subbass.bandcamp.com/track/ethnicalvibes-basscharma
[19]: http://fishfinger.bandcamp.com/track/sheila-chandra-nana-the-dreaming
[20]: https://soundcloud.com/ganjawhitenight/ganja-white-night-mango/download
[21]: https://soundcloud.com/dubstep/kimi-dake-by-overhertz-dubstep/download
[CC BY-NC-ND 3.0]: http://creativecommons.org/licenses/by-nc-nd/3.0/
[CC BY-NC-SA 3.0]: http://creativecommons.org/licenses/by-nc-sa/3.0/

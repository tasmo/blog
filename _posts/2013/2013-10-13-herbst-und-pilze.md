---
layout: post
lang: de
title:  Herbst und Pilze
permalink: /3713/herbst-und-pilze/
categories:
 - Foto
tags:
 - Fotos
 - selfmade
comments: true
share: true
image:
  feature: 2013/10/Herbst-und-Pilze-20131013-16.jpg
  credit: Thomas Friese
  creditlink: http://www.flickr.com/photos/tasmo/sets/72157636516037973
---

Anstatt an diesem Wochenende _Ingress_ zu spielen oder einfach im Bett zu bleiben und _The Newsroom_ weiter zu gucken, wollte ich mir mal diesen goldenen Herbst anschauen, von dem sie™ alle reden. Denn auch das Internet™ weiß schon lange:

>Rausgehen ist wie Fenster aufmachen, nur krasser.

Und was sah ich? Kein Gold! Die Bäume in der Stadt scheinen schneller ihre Blätter loswerden zu wollen. Dafür fanden wir™ – ganz hebrstgemäß – Pilze:

[![Herbst und Pilze](/images/2013/10/10254694136_dcf81df9f4_c.jpg)](http://www.flickr.com/photos/tasmo/10254694136/)
<!--more-->

[![Herbst und Pilze](/images/2013/10/10254633374_978f11edbd_c.jpg)](http://www.flickr.com/photos/tasmo/10254633374/)

[![Herbst und Pilze](/images/2013/10/10254649464_a88b20609a_c.jpg)](http://www.flickr.com/photos/tasmo/10254649464/)

[![Herbst und Pilze](/images/2013/10/10254754095_1d20f1586a_c.jpg)](http://www.flickr.com/photos/tasmo/10254754095/)

[![Herbst und Pilze](/images/2013/10/10254757165_241806dbbc_c.jpg)](http://www.flickr.com/photos/tasmo/10254757165/)

[![Herbst und Pilze](/images/2013/10/10254853853_83c0824bf8_c.jpg)](http://www.flickr.com/photos/tasmo/10254853853/)

[![Herbst und Pilze](/images/2013/10/10254739296_163a02ac2b_c.jpg)](http://www.flickr.com/photos/tasmo/10254739296/)

[![Herbst und Pilze](/images/2013/10/10254752595_29294253e1_c.jpg)](http://www.flickr.com/photos/tasmo/10254752595/)

[![Herbst und Pilze](/images/2013/10/10254758675_7651b3e623_c.jpg)](http://www.flickr.com/photos/tasmo/10254758675/)

[![Herbst und Pilze](/images/2013/10/10254740195_d533499ab2_c.jpg)](http://www.flickr.com/photos/tasmo/10254740195/)

[![Herbst und Pilze](/images/2013/10/10254740215_bf6d7775d6_c.jpg)](http://www.flickr.com/photos/tasmo/10254740215/)

[![Herbst und Pilze](/images/2013/10/10254757966_cd94289ce2_c.jpg)](http://www.flickr.com/photos/tasmo/10254757966/)

[![Herbst und Pilze](/images/2013/10/10254728716_8516c6b521_c.jpg)](http://www.flickr.com/photos/tasmo/10254728716/)

[![Herbst und Pilze](/images/2013/10/10254721766_ce8ee4aab2_c.jpg)](http://www.flickr.com/photos/tasmo/10254721766/)

[![Herbst und Pilze](/images/2013/10/10254715566_a4efee5ec7_c.jpg)](http://www.flickr.com/photos/tasmo/10254715566/)

[![Herbst und Pilze](/images/2013/10/10254717725_5913ae7fa3_c.jpg)](http://www.flickr.com/photos/tasmo/10254717725/)

[![Herbst und Pilze](/images/2013/10/10254824283_f3dc9b3ca4_c.jpg)](http://www.flickr.com/photos/tasmo/10254824283/)

[![Herbst und Pilze](/images/2013/10/10254615264_38e987f2e7_c.jpg)](http://www.flickr.com/photos/tasmo/10254615264/)

[![Herbst und Pilze](/images/2013/10/10254700256_2176c6942f_c.jpg)](http://www.flickr.com/photos/tasmo/10254700256/)

[![Herbst und Pilze](/images/2013/10/10254601954_5e9c45eb53_c.jpg)](http://www.flickr.com/photos/tasmo/10254601954/)

[![Herbst und Pilze](/images/2013/10/10254696245_2971d25fbe_c.jpg)](http://www.flickr.com/photos/tasmo/10254696245/)

[![Herbst und Pilze](/images/2013/10/10254688756_63f290b36d_c.jpg)](http://www.flickr.com/photos/tasmo/10254688756/)

[![Herbst und Pilze](/images/2013/10/10254800603_c4e25d697b_c.jpg)](http://www.flickr.com/photos/tasmo/10254800603/)

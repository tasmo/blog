---
layout: post
lang: de
title: Die Geister, die ich rief
permalink: /3699/die-geister-die-ich-rief/
categories:
 - meta
tags:
 - meta
 - blog
 - code
comments: true
share: true
modified: 2015-03-20T19:17:18+01:00
---

## Was bisher geschah:

Mein [bisheriges Blog](http://tasmo.de/) läuft auf dem PHP[^1]-basierten CMS [Wordpress](http://wordpress.org/). Mal selbst gehostet, dann wieder nicht und seit geraumer Zeit bei den [Ubernauten](http://uberspace.de/). OK, anfangs habe ich auch andere [Blogging-System](https://en.wikipedia.org/wiki/Blog_software) probiert (Drupal oder Blogger zum Beispiel) und fand es auch schon immer cool, von der [Kommandozeile aus zu bloggen](http://nanoblogger.sourceforge.net/), zugleich war mir die Einrichtung doch zu nerdig. Für meine Sammel&shy;leidenschaft befülle ich auch das eine oder andere Micro-Blog auf [Tumblr](http://tumblr.tasmo.de/).

Wordpress ist im Laufe der ständigen Entwicklung sehr vielseitig und vom bloßen Weblog-System zu einem Richtigen Seiten&shy;verwaltungs&shy;instrument gewachsen. Mit allerlei Plugins und ein paar PHP-Kenntnissen konnte ich es zuletzt relativ an meine Berdürfnisse anpassen. Ein weiterer, großer Pluspunkt ist, dass es rundherum eine rege OpenSource-Community gibt, mit Foren und Blog-Posts mit Lösungen zu fast allen Schwierigkeiten.

## Die alten Zähne wurden schlecht…

Mit den steigenden Möglichkeiten wird das Paket um Wordpress auf "meinem" Server immer dicker. Die Entscheidung, wo ich anfangen soll, es zu entschlacken ist nach meiner oberflächlichen Einschätzung ungefähr so aufwendig, wie alles von Grund auf neu aufzusetzen. Auf jeden Fall möchte ich gerade wieder ein etwas einfacheres, neues, puristisches Blog.
<!--more-->

## Tabula Rasa

Genau in die Phase der Erkundung kam die Nachricht von einer freien, offenen und einfachen Plattform, die nach Mittbestimmung und Finanzierung auf Kickstarter sucht. [Ghost](http://blog.ghost.org/launch/).
Ghost basiert auf der JavaScript-Umgebung Node.js, die einen seperaten Webserver überflüssig macht. Der [Editor von Ghost](https://en.ghost.org/features) nimmt die einfache Markup-Sprache [Markdown](http://daringfireball.net/projects/markdown/syntax) zum Formatieren der Texte. Sie ist übersichtlich, schnell zu lernen und einfach zu benutzen.
![screenshot](/images/2013/09/Bildschirmfoto_2013_09_27_um_21_22_38.png)
Zum Einfügen von Bilder stehen chicke Platzhalter für Drag'n'Drop zur Verfügung. Das voreingestellte Theme "Caspar" ist schön und schön schlicht. Ich habe es geklont und noch etwas angepasst.
Wie Ghost auf einem Webserver installiert wird, weiß @[Tradem](http://twitter.com/Tradem) [in seiner Anleitung](http://fork.postpoeia.name/ghost-auf-uberspace-installieren/) sehr gut zu erläutern.
Ich will Ghost zum ausprobieren lieber unter der Subdomain [ghost.](http://ghost.tasmo.de/) laufen lassen und habe noch eine kleine Änderung dafür in die Datei `.htaccess` geschrieben:

{% highlight viml %}
RewriteEngine On
RewriteBase /
RewriteRule (.*) http://localhost:2368/$1 [QSA,P]
{% endhighlight %}

## So!

Was jetzt noch fehlt, ist ein Dashboard zum Verwalten von viel Inhalt, statische Seiten für eine [Datenschutz&shy;erklärung](/about/privacy/) und ein [Impressum](/about/impress/) und ein genauso schlankes Kommentar-System. Aber Ghost ist OpenSource (der Quell-Code wird demnächst veröffentlicht), sehr jung und übersichtlich. Ist also alles nur eine Frage von kurzer Zeit.

* * *

## update

Mittlerweile habe ich mein Blog komplett auf Datei-Basis umgestellt. _Markdown_ ist immernoch die Formatierungssprache meiner Wahl. Erzeugt werden die Seiten jetzt aber mit [Jekyll](/about). Das habe ich auf der re:publica 14 [in einem Workshop zusammengefasst](/2014/05/rp14/). Und als Kommentarsystem nutze ich seit neuestem das selbstgehostete [isso](//posativ.org/isso/).

[^1]: PHP: rekursives Akronym und Backronym für _PHP: Hypertext Preprocessor_, ursprünglich _Personal Home Page Tools_ <small>(Quelle: [Wikipedia](https://de.wikipedia.org/wiki/PHP))</small>

*[CMS]: Content Management System

---
title: Musik zum Wochenende
layout: post
lang: de
permalink: /3633/musik-zum-wochenende/
categories:
  - Blick nach draußen
tags:
  - compilation
  - music
  - free download
format: video
comments: true
share: true
---

Nur eine kleine, schnelle Zusammenstellung der Musik, die mich in den vergangenen Tagen begeistert hat:

[NUMBE:RA - All I Want Is Love ![NUMBE:RA - All I Want Is Love](/images/2013/08/NUMBE_RA%20-%20All%20I%20Want%20Is%20Love.png)](https://www.youtube.com/watch?v=ei3iSEv2uhU)
<!--more-->

[Antonio Vivaldi - La primavera (Spring) RV269, Op.8, No.1 on Floppy drives ![Antonio Vivaldi - La primavera (Spring) RV269, Op.8, No.1 on Floppy drives](/images/2013/08/Antonio%20Vivaldi%20-%20La%20primavera%20%28Spring%29%20RV269%2C%20Op.8%2C%20No.1.png)](http://www.youtube.com/watch?v=uTVJQ2yzyRY)

[R.O -- I'm going to Australia ! ( Preview )](https://soundcloud.com/olivierugi/r-o-im-going-to-australia)

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/102819637"></iframe>

[](https://soundcloud.com/djmundi/electro-sha3bi-mega-mix/download){: .download} [DJ Mundi -- Egyptian Electro Sha3bi Mix](https://soundcloud.com/djmundi/electro-sha3bi-mega-mix/)

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/105013457"></iframe>

[Jono McCleery -- 'Ballade'(Radio Edit)](https://soundcloud.com/ninja-tune/jono-mccleery-ballade-radio-1)

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/103090326"></iframe>

[](https://soundcloud.com/felixfeliz/no-i-didnt-go-to-work-today/download){: .download} [Felix Feliz -- No, I didn't go to work today](https://soundcloud.com/felixfeliz/no-i-didnt-go-to-work-today/)

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/104609915"></iframe>

[](https://soundcloud.com/deepheads/nina-simone-feel-good-skyphos/download){: .download} [Nina Simone -- Feel Good (Skyphos Remix)](https://soundcloud.com/deepheads/nina-simone-feel-good-skyphos/)

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/99506698"></iframe>

[EARMILK Presents: DJ AA](https://soundcloud.com/earmilk/earmilk-presents-dj-aa)

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/103268361"></iframe>

---

Gesammelt u.a. von [Saetchmo][1], [Ronny][2] uvm. und gesammelt in meinem [Tumblr][3].

[1]: http://saetchmo.tumblr.com/ "Saetchmo"
[2]: http://www.kaputtmutterfischwerk.de/ "Kraftfuttermischwerk"
[3]: http://tumblr.tasmo.de/

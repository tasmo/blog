---
title: Sie sind unter uns!
layout: post
lang: de
permalink: /3361/sie-sind-unter-uns/
categories:
  - Foto
tags:
  - selfmade
  - Fotos
  - richtig wichtig
format: image
comments: true
share: true
---

![Sie sind unter uns!][1]  
[Sie sind unter uns! at Volkspark Hasenheide][2]

Jedes Jahr ärgere ich mich zur gleichen Zeit, nämlich im April, über die Trampel der "Neuköllner Maientage". Denn wenn die Sonne die ersten Grashalme bestrahlt, wird sie Hasenheide von Schaustellern mit ihren dröhnenden und nachts flackenden Karussells, Scootern und Jahrmarktsbuden übernommen.

Und trotzdem scheint es Menschen zu geben, die sich da amüsieren wollen. Wer hat Recht? Auf jeden Fall sieht es dann Anfang Mai immer so oder ähnlich aus.

[1]: /images/2013/06/8987514311_918deebe1d_c.jpg
[2]: http://www.flickr.com/photos/tasmo/8987514311

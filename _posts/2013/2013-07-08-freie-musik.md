---
title: Freie Musik für freie Projekte
layout: post
lang: de
permalink: /freie-musik/
redirect_from: /3191/freie-musik/
categories:
  - Blick nach draußen
tags:
  - Creative Commons
  - GEMAfrei
  - copyright
  - music
  - howto
comments: true
share: true
image:
  feature: 2013/07/free_cultural_turntable.jpg
  credit: 'Thomas Friese / CC BY-NC-SA 3.0; Remix aus "Turntable" (http://www.flickr.com/photos/mrguep/4132947695/) von David Gallard / CC BY-NC-SA 2.0 & Logo of "Definition of Free Cultural Works" (http://freedomdefined.org/Definition)'
---

Viele Aktionen, Projekte und Aufführungen von Freiwilligen wachsen aus eigener Kraft und auf eigene Kosten. Musik ist dabei oft ein wesentlicher Teil.

Wenn die Musik frei verfügbar ist und von Menschen kommt, die ihr Herz, ihre Energie und Zeit dafür geben, ist das umso schöner. Zudem ist es möglich, durch das Nutzen freier Musik die Finanzierung *seltsamer Vereine zur Verwaltung und Monetarisierung von Aufführungs- und Vervielfältigungsrechten* zu umgehen und so unnötige Zusatzkosten zu vermeiden oder einfach ein Zeichen zu setzen.

Eine gute Kennzeichnung solcher frei benutzbarer Musik ist, sie unter eine freie Lizenz zu stellen. Bei kreativen Werken haben sich vor allem die [Creative-Commons-Lizenzen][1] (nachfolgend kurz mit "**CC**" bezeichnet) etabliert. Diese umfassen sowohl [freie][2] als auch [eingeschränkte][3] Rechte. Frei heißt dabei nicht automatisch kostenlos. So können Künstlerinnen beim Nutzen einer solchen Lizenz auch entlohnt werden. Wahlweise Einschränkungen sehen die verschiedenen Lizenzen etwa bei der kommerziellen Weiterverwendung vor und der Veränderung (Remix) der Werke mit oder ohne Nutzen der gleichen Lizenz und Nennung der Urheber\*innen.
<!--more-->

### Fallbeispiel:

EDRi ist der europäische Dachverband der Organisationen für digitale Bürgerrechte, eines der Hauptthemen ist modernes Urheberrecht und freies Wissen. Für die [Zehn-Jahres-Feier][5] haben [CptPudding][6], [Withoutfield][7] und ich uns daher in den Heuhaufen des Internets gestürzt, um tanzbare und unserem Geschmack entsprechende Musik zu finden, die frei verfügbar und nutzbar ist. Am Anfang steht dabei eine Suche nach Portalen, die CC-Musik gut filtern und somit findbar machen.

## Die Plattennadel im CC-Heuhaufen {#heuhaufen}

Wo finde ich also diese kostenlose Musik, wo kann ich suchen? Folgende Plattformen habe ich bisher intensiver angeschaut:

### Bandcamp {#bandcamp}

[![Screenshot von Bandcamp.com](/images/2013/bandcamp.jpg)][Bandcamp]

[Bandcamp] ist ein guter Ort für Musikerinnen, eigene Musik zu streamen, zu publizieren und auch zu verkaufen. Auf dieser Plattform sind alle Musikstile zu finden, von akustischer Musik über elektronische Bässe bis hin zu Soundtracks. Auf Bandcamp gibt es freie und unfreie Musik gemischt. Die Suche nach freien Inhalten ist auf Bandcamp umständlich, einen Filter dafür gibt es nicht. Einige Künstlerinnen taggen ihre Werke richtig mit "cc" oder "CreativeCommons" etc. – viele aber falsch.

Technisch ist Bandcamp aktuell und optisch angenehm übersichtlich. Einzelne Tracks oder ganze Alben können direkt heruntergeladen oder gekauft werden. Dabei steht die Musik in verschiedenen Formaten und Qualitäten zur Auswahl. Die Preisspanne wird von den Künstlerinnen bestimmt. Der Player von Bandcamp kann auch über einen Embed-Code einfach in jede Seite eingebunden werden.

### Soundcloud {#soundcloud}

[![Screenshot von Soundcloud.com](/images/2013/soundcloud.jpg)][9]

Auf [Soundcloud][9] nach Musik zu stöbern und zu hören macht Spaß. Es ist durch *continuous play* zu dem ein guter Radio-Ersatz. Musik kann "geherzt" werden und angemeldete Nutzerinnen können an ihren Lieblingsstellen kommentieren. Jede kann eigene Sets mit bevorzugter Musik zusammenstellen. Die Auswahl ist riesig, da fast alle aktuellen Musikerinnen und Bands ihre Inhalte darauf promoten und präsentieren. Soundcloud ist das soziale Netzwerk für Musikinteressierte.

Soundcloud hat eine eigene [Seite für CC-Musik][10]. Hier gilt aber das gleiche wie für [Bandcamp][11], wenn die Tracks falsch klassifiziert sind, nützt der beste Filter nichts. Bis auf dieses Manko ist die Suche gut und einfach strukturiert, lässt sich nach Titeln, Sets, Künstlerinnen und Gruppen filtern. Auf Soundcloud gibt es hauptsächlich unfreie Musik zu hören und herunterzuladen, aber auch ein großes Repertoire an freien Songs.

Der Player ist gut benutzbar, CC-Musik ist mit den kleinen Symbolen unter jedem Track gekennzeichnet. Die Suche ist übersichtlich. Den Soundcloud-Player gibt es in Flash und HTML5 zum Einfügen in jede Webseite und es gibt Apps für Android, iOS und [Mac][12].

### Jamendo {#jamendo}

[![Screenshot von Jamendo.com](/images/2013/jamendo.jpg)][13]

[Jamendo][13] ist das Profi-Portal für CC-Musik. Die Seite wirbt mit "Kostenlose Musik für jedermann" und behält damit Recht.  
Musikalisch wird eine große Breite von unbekannt bis populär angeboten. Um Musik zu entdecken, gibt es Empfehlungen und verschiedene Radio-Streams. Die Suche bietet eine intelligente Autovervollständigung und kann nach Land, Sprache und CC-Lizenz gefiltert werden sowie danach, ob sie für die kommerzielle Nutzung gekauft werden kann.

Auf Jamendo wird Musik für private Nutzung kostenlos zur Verfügung gestellt. Sie ist nicht "frei" und kommerzielle Nutzerinnen müssen für die [Verwendung und Verwertung bezahlen][14]. Die Einnahmen werden 50:50 mit den [Künstlerinnen][15] geteilt. Den Künstlerinnen kann auch direkt gespendet werden.

Jede angemeldete Nutzerin kann Bewertungen abgeben, eigene Sets zusammenstellen und Werke kommentieren. Die Seite ist angenehm anzuschauen, der Player hält auch dann nicht an, wenn die Suche nach anderer Musik weitergeht – wie auch bei [Soundcloud][16]. Er kann als Widget in andere Webseiten eingebunden werden. Jamendo bietet kostenlose Apps für iOS, Android und BlackBerry an.

### Free Music Archive {#freemusicarchive}

[![Scrrenshot von Freemusicarchive.org](/images/2013/freemusicarchive.jpg)][17]

Beim [Free Music Archive][17] ist alle Musik – wie der Name schon sagt – frei. Die meiste Musik davon steht hier unter einer Creative-Commons-Lizenz.

Musikalisch gibt es viele unbekannte Perlen jeglicher Genres zu entdecken. Das wenigste ist bereits einschlägig bekannt und abgenutzt. Vorherrschend sind hier nicht professionell bearbeitete Produktionen zu finden.

Die Sortierung auf der Plattform folgt einer anderen Struktur. Sie ist aufgeteilt in eine Liste nach *Kuratoren* (Labels oder Nutzerinnen) und nach *Genres*. Die Suche ist sehr umfangreich und lässt sich sowohl nach vielen musikalischen Kriterien filtern als auch nach Art der gewünschten Lizenzen.  
Der Player des Free Music Archive ist simpel. Über angezeigte Schlagworte, Genres, der Dateihierarchie und das Label wird der Weg zu verwandter Musik geöffnet.

Besonders gut zum Stöbern eignen sich die empfohlenen und kuratierten Zusammenstellungen. Insgesamt eine schöne und praktische Seite.

### Archive.org {#netlabels}

[![Scrrenshot von rchive.org/details/netlabels](/images/2013/netlabels.jpg)][18]

Die [Netlabel-Sammlung][18] des [Internet Archive][19] beherbergt ebenfalls ausschließlich kostenlose Musik. Die meiste davon steht unter einen freien CC-Lizenz.

Wie der Name schon sagt, stehen hier [Netlabels][20] und deren Inhalte im Vordergrund. Musikalisch gibt es einiges zu entdecken. Die Empfehlungen und Statistiken bieten gute Pfade dazu.

Auf der Suche nach Musik kann jeweils nach Label, Genre, Schlagworten, Lizenz und Künstlerin gesucht und sortiert werden. Die Seite bietet eine Fülle an Suchmöglichkeiten, hat dafür jedoch kein einfach verständliches Interface. Insgesamt wirkt die Seite zwar seriös aber auch sehr technisch, hat den Charme eines Rechenzentrums und ist zudem überfrachtet.

Gut gelöst ist der Player von Archive.org. Dieser lässt sich einfach mit einem Code in Webseiten einbetten und bietet den Download und verschiedenen Formaten. Neben *MP3* wird die Musik oft auch im *OGG*- oder *FLAC*-Codec angeboten.

### ccMixter

[![Scrrenshot von ccmixter.org](/images/2013/ccmixter.jpg)][21]

[ccMixter][21] ist die CC-Remix-Plattform, lädt also explizit ein, Musik zu remixen und erneut zu teilen. Dabei entstehen Remixes von Werken namhafter und unbekannter Künstler. Es dürfen ausschließlich freie oder unter CC stehende Werke geremixt und veröffentlicht werden.

Auf den ersten Blick wirkt die Seite genauso unübersichtlich und wenig einladend wie die [Netlabel-Seite][22].

ccMixter gibt den Suchenden verschiedene Hilfen. Es gibt sowohl Empfehlungen, als auch für Remixer sinnvolle Kategorien – dazu die üblichen Schlagworte und die Verlinkung der Künstlerinnen. Was bei ccMixter einzigartig ist, es kann auch nach den für die Remixe benutzten Werken gefiltert und gesucht werden.

Der Player ist sehr spartanisch. Er kann nicht eingebunden werden. Über Playlisten gibt es auch so etwas wie eine Radio-Funktion, um sich inspirieren oder berauschen zu lassen.

### Und wo noch: {#wonoch}

Weitere Plattformen, die ich mir noch nicht näher angeschaut habe, sind der Klassiker [scene.org][23], die Netlabel-Plattform [Phlow][24], der Free-Sound-Pool [opsound][25], die Musik-Suchmaschine [soundclick][26], die US-Seite [ArtistServer][27], das belgische [electrobel][28] und das Sample-Mekka [Freesound.org][29]. Eine sehr ausführliche Liste ist auf der [Seite der Denver Public Schools][30] zu finden.

## CC-Mischmasch {#ccmischmasch}

CC-Lizenzen werden oft unwissend eingesetzt. Ein Blick auf [SoundCloud][31] oder [Bandcamp][32] zeigt, dass viele Künstlerinnen die Lizenzen für geremixte Inhalte benutzen, für die sie gar nicht geeignet sind. Urheberrechtlich geschützte Inhalte dürfen nicht für ein neues Werk einfach so in eine CC-Lizenz überführt werden. Das betrifft sowohl lange Mixe von DJs als auch einzeln geremixte Tracks.

Auch die Wahl der falschen Lizenz führt zu ungewollten Einschränkungen. Eine Lizenz, die beispielsweise die kommerzielle Nutzung verbietet, verbietet gleichzeitig die Aufführung im öffentlichen Raum für nonprofit-Veranstaltungen.

## TL;DR

Musik für spezielle Gelegenheiten zu finden, ist im Internetz ähnlich schwierig wie zu der Zeit, als der Plattenladen nebenan noch der wichtigste Anlaufpunkt für Musik war. Heute gibt es Filter bei der Suche und viele Seiten und Blogs, die Musik ähnlich dem Plattendealer empfehlen. Die Vielfalt und Fülle an Musik im Internet steht dem entgegen. Doch gute, passende und freie Musik zu suchen, ist spannend und lohnt sich.

* * *

Quellen und Empfehlungen:
{: #quellen}

* <http://creativecommons.org/licenses/>
* <http://creativecommons.org/record-labels>
* <http://creativecommons.org/music-communities>
* <http://search.creativecommons.org/?lang=de>
* <http://creativecommons.org/legalmusicforvideos>
* <http://mashable.com/2007/10/27/creative-commons/>
* <http://www.sitepoint.com/30-creative-commons-sources/>
* <http://mediacommons.psu.edu/instruction/freemedia>
* [http://en.wikipedia.org/wiki/Creative\_Commons-licensed\_content_directories][33]
* <http://www.socialbrite.org/sharing-center/free-music-directory/>
* <https://sites.google.com/site/dpsresourcesite/Home/creative-commons-overview-and-resources>
* <http://depts.washington.edu/trio/trioquest/resources/audio/cc.php>
* <http://de.wikipedia.org/wiki/Freie_Musik>

[1]: http://wiki.creativecommons.org/Musician
[2]: http://creativecommons.org/licenses/by-sa/3.0/de/
[3]: http://creativecommons.org/licenses/by-nd/3.0/de/
[5]: /3218/creative-commons-set-for-edri/
[6]: https://twitter.com/intent/user?screen_name=CptPudding
[7]: https://twitter.com/intent/user?screen_name=Withoutfield
[Bandcamp]: http://bandcamp.com/
[9]: http://soundcloud.com/
[10]: https://soundcloud.com/creativecommons
[11]: #bandcamp
[12]: /3125/soundcloud-lasst-mac-app-fallen/ "Soundcloud lässt Mac-App fallen"
[13]: http://www.jamendo.com/
[14]: https://pro.jamendo.com/
[15]: http://artists.jamendo.com/
[16]: #soundcloud
[17]: http://freemusicarchive.org/
[18]: http://archive.org/details/netlabels
[19]: http://archive.org/
[20]: http://de.wikipedia.org/wiki/Netlabel
[21]: http://ccmixter.org/
[22]: #netlabels
[23]: http://de.scene.org/pub/music/groups/
[24]: http://phlow.de/netlabel/mp3/index.php
[25]: http://opsound.org/
[26]: http://www.soundclick.com/business/license_list.cfm
[27]: http://www.artistserver.com/music.cfm
[28]: http://be.electrobel.org/
[29]: http://www.freesound.org/
[30]: https://sites.google.com/site/dpsresourcesite/Home/creative-commons-overview-and-resources
[31]: http://soundcloud.com/creativecommons
[32]: http://bandcamp.com/tag/creative-commons
[33]: http://en.wikipedia.org/wiki/Creative_Commons-licensed_content_directories

*[EDRi]: European Digital Rights

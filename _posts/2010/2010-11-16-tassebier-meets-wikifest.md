---
title: 'DJ? Tasmo @ #Tassebier meets #Wikifest'
layout: post
permalink: /245/tassebier-meets-wikifest/
date: 2010-11-16
dsq_thread_id:
  - 173787192
categories:
  - music
tags:
  - Tassebier
  - DJ-Set
  - Tasmo
  - music
  - selfmade
  - Tom Freeze
comments: true
share: true
image:
  feature: 2010/11/wikifest.jpg
  credit: '@mogreens'
  creditlink: https://twitter.com/mogreens
---

<iframe style="width:100%; height:360px; border:none;" src="//www.mixcloud.com/widget/iframe/?feed=http%3A%2F%2Fwww.mixcloud.com%2Fblogrebellen%2Fdj-tasmo-tassebier-meets-wikifest%2F&mini=&stylecolor=&hide_artwork=&embed_type=widget_standard&embed_uuid=bd8cbbb0-1dd9-49d5-92b9-adaee53c5494&hide_tracklist=&hide_cover=1&autoplay="></iframe>

[DJ? Tasmo @ #Tassebier Meets #Wikifest][1]

---

Foto by Stefan @[mogreens](https://twitter.com/mogreens) / gallery @ [Blogrebellen](http://blog.rebellen.info/2010/11/16/in-bildern-tassebier-meets-wikifest/)
{: .small}

[1]: http://www.mixcloud.com/blogrebellen/dj-tasmo-tassebier-meets-wikifest/?utm_source=widget&utm_medium=web&utm_campaign=base_links&utm_term=cloudcast_link

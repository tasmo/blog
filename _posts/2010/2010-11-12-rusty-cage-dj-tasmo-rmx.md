---
title: Johnny Cash – Rusty Cage (DJ? Tasmo RMX)
layout: post
permalink: /231/rusty-cage-dj-tasmo-rmx/
dsq_thread_id:
  - 171602484
categories:
  - music
tags:
  - Tasmo
  - glitch
  - remix
  - selfmade
  - Tom Freeze
comments: true
share: true
image:
  feature: 2010/11/rusty-cage.png
---

<iframe style="width:100%; height:166px; overflow:hidden; border-style:none;" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F6958132&show_artwork=true"></iframe>

R.I.P., Johnny! I did [this remix](https://soundcloud.com/tasmo/rusty-cage-dj-tasmo-rmx) just for fun 2 month before he went.

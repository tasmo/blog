---
title: Das war Jugend hackt
subtitle: Jugendliche programmieren für Flüchtlinge und gegen Überwachung
description: Ein Wochenende lang haben sich 135 Jugendliche Deutschland in Berlin bei Jugend hackt getroffen und waren froh, "endlich unter normalen Leuten" zu sein.
layout: post
lang: de
categories:
  - Education
tags:
  - OpenSource
  - OpenData
  - Berlin
  - Kids
comments: true
share: true
image:
  feature: 2015/jugend-hackt-begeisterte-teilnehmerinnen.jpg
  credit: © Leonard Wolf / CC-BY-3.0
  creditlink: https://www.flickr.com/photos/okfde/22239302612/in/album-72157659982826391/
---

Sie sind jung, die meisten von ihnen können programmieren und sie wollen mit Code die Welt verbessern. Ein Wochenende lang haben sich 135 Jugendliche in Berlin bei Jugend hackt getroffen und waren froh, "endlich unter normalen Leuten" zu sein.

Jugend hackt ist ein Wettbewerb, der eigentlich gar keiner ist. Eine Jury -- diesmal bestehend aus Imma Chienku von [Refugees Emancipation e.V.], Cecilia Palmer von [Fashion & Code], Tim Pritlove vom [Chaos Computer Club e.V.] und Jens Ohlig als Vertreter der Mentor\*innen -- vergibt symbolische Preise in fünf Kategorien. Doch der eigentliche Gewinn ist, in knapp zwei Tagen ein OpenSource-Projekt selbstständig in spontanen Gruppen von der Ideenfindung über die Realisierung bis zur Präsentation zu erarbeiten. 48 Mentor\*innen standen den jungen Teilnehmer\*innen für die Ideenentwicklung und Umsetzung zur Seite.

Es geht aber nicht nur darum, Projekte fertig zu bekommen. Die Jugendlichen sollen lernen, ein Ziel gemeinsam zu verfolgen und zu hinterfragen, was sie eigentlich erreichen wollen. Am ganzen Wochenende finden deshalb gleichzeitig begleitende Vorträge und Workshops statt, in denen neben besserem Coden auch [Hackerethik] beigebracht wird.<!--more-->

## Schwerpunkt \#refugeeswelcome

Erstmals stand Jugend hackt unter dem aktuellen Hauptthema \#refugeeswelcome und Antirassismus. Der Preis in dieser neuen Kategorie, die in Zusammenarbeit mit der [Amadeu Antonio Stiftung] entstand, ging an das Team von [Germany says Welcome], ein Netzwerk, um Flüchtlinge in vielen Belangen zu Unterstützen. Helfer\*innen können sich mit Flüchtlingen vernetzen, es gibt eine Karte, auf der alle wichtigen Ämter verzeichnet sind und eine Tauschbörse für Dinge und Dienstleistungen.

[![I'm a Refugees - Ask me Anything (CC-BY 3.0 Leonard Wolf)](/images/2015/jugend-hackt-im-a-refugee-ama.jpg)](https://www.flickr.com/photos/okfde/22064085240/in/album-72157659982826391/)

Die Jury befindet, dass "[Germany says Welcome] in diesem historischen Moment, in dem sich Deutschland gerade befindet, das Gefühl von Zugehörigkeit für Flüchtlinge stärkt und bei der Verständigung und Barrieren zu überwinden hilft." Bereits bei Jugend hackt West in diesem Jahr wurde das Projekt angestoßen und jetzt in Berlin in einer erweiterten Gruppe ausgebaut.

Das Team zum [Refugee Phrasebook] stellt ein Wörterbuch mit den wichtigsten Bergriffen zur Verständigung im Alltag mit Geflüchteten als offene Daten zugänglich online. So sollen nicht nur Sprachbarrieren überwunden werden. [Fuck Borders] will -- mit einem Meldesystem und einer Karte von Europas Grenzen und den offenen Lücken darin -- helfen, Grenzen zu überwinden. Das Netzwerk [Active Germany] macht gemeinsame Aktionen von Geflüchteten und Helfer\*innen und/oder Einheimischen möglich und schafft so ein besseres Verständnis anderer Kulturen und Sprachen.

## Einfach bedienbare Verschlüsselung

Das ebenso aktuelle Thema Überwachung beschäftigt Jugendliche genauso. Mit aufklärenden Webseiten, interaktiven Spielen und verschlüsselter Kommunikation finden sie vielfältige Lösungen.

Bei dem Projekt [Querschläger] stießen sogar die Mentor\*innen an ihre Wissensgrenzen und konnten dazulernen. Die in der funktionalen Programmiersprache Haskell geschriebene Bibliothek für das verschlüsselte und anonyme Chat-Programm [Ricochet], das das Tor-Netzwerk nutzt, vereinfacht für Entwickler das Implementieren des Messaging-Dienstes. Für die vorbildliche Umsetzung bekam die Gruppe den Preis für den "besten Code" von der Jury überreicht.

Ebenfalls um einfach bedienbare Verschlüsselung geht es bei [invisiblePGP]. Damit ist eine Verschlüsselung von Emails unabhängig vom benutzten Mailprogramm möglich. Ein Verschlüsselungsserver wird zwischen Mailprogramm und Mailserver geschaltet, der die Emails im Speicher ver- und entschlüsselt. [invisiblePGP] kommuniziert dabei verschlüsselt mit dem jeweiligen Endgerät. Der Entwickler Motitz Fago meint: "Schon seit 1991 existiert PGP und noch niemand hat es bisher geschafft, es einfach bedienbar für jeden zu machen. Das ist jetzt mein Versuch."

## Von wegen Politikverdrossenheit

Weil [das Gesetz zur Vorratsdatenspeicherung (VDS) am 16. Oktober] viel zu schnell verabschiedet werden konnte, will "[18-5088]" verdeutlichen, welche Auswirkungen das Loggen unserer Mobilfunkdaten auf uns hat. Auf der Webseite soll der Gesetzestext in mehreren Sprachen mit Zugang für alle stehen und die Daten werden visualisiert.

Die Plattform [Polivote] schafft direkteren Einfluss von Bürgern auf Gesetze. Die Webseite gibt Feedback an Politiker, zeigt aktuelle Informationen zur Plenarsitzung im Bundestag und Gesetzesentwürfe einfach verständlich an. Diese können dann mit Daumen hoch oder Daumen runter bewertet werden.

[Was der BND kann, kannst Du auch] sammelt wie ein Geheimdienst Daten von einzelnen Personen in sozialen Netzwerken. Zur Verdeutlichung werden die angefallenen Daten auf der Webseite aufbereitet und in auswertbaren Diagrammen dargestellt. Damit der Dienst nicht zweckentfremdet werden kann, müssen sich die Besucher\*innen vorher authentifizieren.

Bei dem Spiel [NoCam] zeigt eine Karte Überwachungskameras in der Umgebung in. Ziel ist es, eine Route durch die Stadt zu finden und dabei von so wenig Kameras wie möglich eingefangen zu werden. Kommt man einer Kamera zu nahe, vibriert das Smartphone und es werden Punkte abgezogen.

## Eine Frage der Optik

Am [Privacy Patch] haben gleich vier Gruppen (für Software, Design, IR LED und LCD-Display) zusammen gearbeitet. Sie haben verschiedene Verfahren entwickelt, mit denen die Kamera im Smartphone nur das erfasst, was sie auch darf.

Wahlweise werden die Bilder entweder mit LEDs -- von einem Mate-Deckel getragen -- vor der Linse geblendet oder ein LCD-Display davor wird einfach undurchsichtig schwarz. Bisher funktioniert die Bedienung über einen Schalter an der Smartphone-Hülle, später soll das System selber erkennen, welche App gerade Bilder machen will und ob sie das darf.

[![Soziale Angelegenheit (CC-BY 3.0 Leonard Wolf)](/images/2015/jugend-hackt-soziale-angelegenheit.jpg)](https://www.flickr.com/photos/okfde/22065404619/in/album-72157659982826391/)

Vom richtigen Blickwinkel ist abhängig, was die Betrachter\*innen beim [Lichtfelddisplay] angezeigt bekommen. Mittels eines Layers aus Linsen über dem Bildschirm werden gleichzeitig verschiedene Inhalte in verschiedene Richtungen angezeigt.

Alle Teilnehmer\*innen konnten sich das Themengebiet selbst aussuchen. Die weiter vorgeschlagenen Bereiche Umwelt, Gesundheit und Zukunftsstadt dienten lediglich als Anhaltspunkte.

## Mit System die Welt verbessern

"Weil eine großartige Idee in nur kurzer Zeit elegant und konkret umgesetzt wurde", ging der Preis für "das beste Design" an die [Latampel], ein intelligentes Ampelsystem, mit dessen Hilfe Unfälle mit Fahrradfahrern verhindert werden und unnötiges Warten an roten Ampeln vermieden wird. "Nicht jedes Projekt muss gleich die ganze Welt verändern, denn auch kleine, konkrete Schritte sind ein guter Weg, die Welt zu verbessern." Deshalb ging auch der Hauptpreis "mit Code die Welt verbessern" an diese Gruppe.

Anhand der Bewegungsdaten von Menschengruppen können mit [CrowdFlow] menschliche Staus vermeiden und so die städtische Infrastruktur optimiert werden. Zur Umsetzung brauchte die Gruppe nach eigener Aussage vor allem 100 Meter Netzwerkkabel, Kabelbinder, Koffein und [Kafka]. Bei [UDE] werden mit Sensoren Werte aus der Luft erfasst und als Open Data zur Verfügung gestellt.

## Aha-Erlebnisse

Für den größten Aha-Moment bei der Jury hat [MusicSync] gesorgt. Mehrere mobile Geräte, egal ob mit iOS oder Android betrieben, spielen gleichzeitig die selbe Musik ab und bilden so ein Netzwerk aus Lautsprechern. "Das Team hat eine konkrete Idee schnell und zielorientiert umgesetzt.", so die Jury.

Der [Smart Mirror] zeigt beim Blick in den Spiegel die wichtigsten Informationen wie Nachrichten und Wetter an und das [Gute-Laune-Fenster] sorgt dazu noch mit Katzenbildern selbst bei Morgenmuffeln für ein Lächeln. Mit dem Bewertungssytem [Teecher] können Schüler sogar unangenehmen Lehrern die Meinung sagen und mit der [Eventkarte] ist es endlich möglich, sich schon vor der Hinfahrt abzusprechen und gemeinsam zu Events fahren.

[![Jugend hackt Abschlußpräsentation 2015](/images/2015/jugend-hackt-abschlusspraesentation-2015.jpg)](https://www.youtube.com/watch?v=mmv_IFvJgIs)

Die [Pillbox] ist eine intelligente Tablettenschachtel, ein RaspberryPi im Inneren steuert die Ausgabe und alarmiert, wenn die Medizin nicht genommen wurde. Die Webseite [Krebs-Check24] zeigt, wie und wo die Krankheit Krebs verbreitet ist. [Scan ess einfach] zeigt nach dem Einscannen des Barcodes einfach verständlich in einer App, was wirklich im jeweiligen Produkt drin ist.

Das Steuerungsmodul [Antikollisionssystem 360°] warnt beim Steuern von Drohnen vor Hindernissen und reagiert mit einem Ausweichmanöver, wenn etwas zu nahe kommt. Mit dem Konfigurationswerkzeug [Servoxit] ist das Verwalten und Bedienen mehrerer Server gleichzeitig ein Kinderspiel. Und wenn ich wissen will, wie erfolgreich mein geplanter Tweet wahrscheinlich sein wird, analysiere ich den Text vorher mit [Tweesion]. Die Web-App gibt u.a. Empfehlungen für fehlende Hashtags.

Veranstaltet wird Jugend hackt vom [Open Knowledge Foundation Deutschland e.V.] und von [mediale pfade].

---

{: .small}
[Alle Bilder] hat Leonard Wolf gemacht und unter [CC-BY 3.0] veröffentlicht.

{: .small}
(Dieser Artikel erschien zuerst auf [Netzpolitik.org].)

[Refugees Emancipation e.V.]: http://www.refugeesemancipation.com/
[Fashion & Code]: http://fashionandcode.com/
[Chaos Computer Club e.V.]: https://www.ccc.de/
[Open Knowledge Foundation Deutschland e.V.]: http://okfn.de/
[mediale pfade]: http://medialepfade.org/
[Hackerethik]: http://www.ccc.de/hackerethics
[Amadeu Antonio Stiftung]: https://www.amadeu-antonio-stiftung.de/
[Germany says Welcome]: http://germany-says-welcome.de/
[Refugee Phrasebook]: https://hackdash.org/projects/562259dcd7a1daf032edaab0
[Fuck Borders]: https://github.com/Jugendhackt/fuckborders
[Active Germany]: https://github.com/MaschiniDev/Active-Germany
[Querschläger]: https://github.com/jugendhackt/haskell-ricochet
[Ricochet]: https://ricochet.im/
[invisiblePGP]: https://github.com/MoritzFago/invisiblePGP
[das Gesetz zur Vorratsdatenspeicherung (VDS) am 16. Oktober]: https://netzpolitik.org/2015/vds-im-bundestag-reaktionen-aus-presse-politik-ngos-und-verbaenden/
[18-5088]: https://github.com/Jugendhackt/18-5088
[Polivote]: https://github.com/Jugendhackt/polivote
[Was der BND kann, kannst Du auch]: https://hackdash.org/projects/5622d5c6d7a1daf032edab2f
[NoCam]: https://github.com/jugendhackt/nocam
[Privacy Patch]: https://hackdash.org/projects/562263c9d7a1daf032edaabb
[Lichtfelddisplay]: https://hackdash.org/projects/56225c1fd7a1daf032edaab1
[MusicSync]: https://github.com/AndroidMusicSync/
[Eventkarte]: https://github.com/Jugendhackt/Eventkarte
[Smart Mirror]: https://hackdash.org/projects/562252d1d7a1daf032edaa98
[Gute-Laune-Fenster]: https://hackdash.org/projects/562263a6d7a1daf032edaab9
[Teecher]: https://hackdash.org/projects/56226e81d7a1daf032edaad2
[Latampel]: https://hackdash.org/projects/5622531ad7a1daf032edaa99
[CrowdFlow]: https://hackdash.org/projects/5622e418d7a1daf032edab33
[Kafka]: https://kafka.apache.org/
[UDE]: https://hackdash.org/projects/56223e45d7a1daf032edaa89
[Pillbox]: https://hackdash.org/projects/56228cc5d7a1daf032edaaf5
[Krebs-Check24]: https://hackdash.org/projects/56228aafd7a1daf032edaaf3
[Scan ess einfach]: https://hackdash.org/projects/562255d9d7a1daf032edaa9f
[Antikollisionssystem 360°]: https://hackdash.org/projects/5622b24ad7a1daf032edab25
[Servoxit]: https://hackdash.org/projects/56224bb0d7a1daf032edaa95
[Tweesion]: https://github.com/M3J/Tweesion
[Alle Bilder]: https://www.flickr.com/photos/okfde/sets/72157659982826391
[CC-BY 3.0]: http://creativecommons.org/licenses/by/3.0/de/
[Netzpolitik.org]: https://netzpolitik.org/2015/jugend-hackt-in-berlin/

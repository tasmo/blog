---
title: "re:claim your blog engine"
subtitle: Einfach Bloggen mit Markdown-Text, Git und Jekyll
layout: post
lang: de
date: 2014-05-07T11:50:00+02:00
modified: 2016-02-17T14:50:00+02:00
categories: [tech]
tags: [republica,workshop,selfmade,code,blog,howto,OpenSource]
comments: true
share: true
---

## `$(blogging the nerd way)`

Auf der [re:publica 14][republica] gebe ich <abbr title="2014-05-07">heute</abbr> meinen [ersten Workshop](http://re-publica.de/session/einfach-bloggen-markdown-text-git-und-jekyll).  
Er findet um 15 Uhr auf [Stage B](https://de.foursquare.com/v/stage-b--republica-14/5369fb79498e44b850307b51) statt.

Das Thema habe ich von meinem Blog abgeleitet. Ich habe viel Zeit und Recherche in [dieses Projekt](/) gesteckt und möchte meine Erfahrungen mit der Einrichtung ich gern mit Euch teilen.<!--more-->

Die Folien für den Workshop habe ich unter [rp14.tasmo.de](https://gh.tasmo.de/reveal-jekyll/) zu veröffentlicht.  
`<update>` …und auch als komplettes Projekt [auf GitHub gestellt](https://github.com/tasmo/reveal-jekyll/tree/gh-pages).

Der Audio-Mitschnitt von [Voice Republic](https://voicerepublic.com/talks/einfach-bloggen-mit-markdown-text-git-und-jekyll) ist da:

<iframe style="overflow:hidden; border:none; width:445px; height:220px;" src="https://voicerepublic.com/embed/talks/einfach-bloggen-mit-markdown-text-git-und-jekyll"></iframe>
(Download als [mp4][mp4]{: .download} / [mp3][mp3]{: .download} / [ogg][ogg]{: .download})
{: .small}

[republica]: http://re-publica.de/
[mp4]: https://voicerepublic.com/vrmedia/191-clean.mp4
[mp3]: https://voicerepublic.com/vrmedia/191-clean.mp3
[ogg]: https://voicerepublic.com/vrmedia/191-clean.ogg

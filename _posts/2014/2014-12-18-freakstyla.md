---
title: Freak Styla!
subtitle: Soulful Freestyle Electronics am 20.12.2014 im Auster-Club
layout: post
lang: de
categories:
  - music
tags:
  - DJ-Set
  - music
  - selfmade
  - Tasmo
comments: true
share: true
---

Lust auf Tanzen in einem schönen Club oder einfach in guter Gesellschaft bei geschmackvoller Musik gemütlich mit Leuten treffen?

Da habe ich den Tipp:  
Im Kreuzberger [Auster-Club](http://www.auster-club.com/) (ehemaliger Privat-Club in der Markthalle IX) spielen am Samstagabend ab 10 Uhr die "netten DJs von nebenan" [Nellski](http://nellski.com/), [2bfuzzy](http://www.mixcloud.com/tobi-kirsch/) und \_Tasmo (also ich) für Dich handerlesene Tracks. [("Freakstylas!" Event auf Facebook)](https://www.facebook.com/events/900102046668482/)

> Die Party Freakstyla! hat sich ganz der stilistischen Musik-Vielfalt verschrieben und bringt im Dezember drei DJs mit gemeinsamem musikalischen Background zusammen. Die Party bringt Stile wie Funky Breaks, Swing, soulful freestyle, Global Beats und Mashup spielerisch unter einem Konzept zusammen. 2b fuzzy bringt den Auster- Club zurück zu seinen Wurzeln mit einem musikalisch vielseitigen Programm. Für den Auftakt hat er Nellski und Tasmo eingeladen, die beide für ihre stilistische Vielfalt bekannt sind.

Damit Du auch weißt, was Dich erwartet, hier ein paar klangliche Kostproben:[^1]

<iframe id="hearthis_at_track_132332" style="overflow:hidden;border:none;background-color:transparent;height:150px;width:100%;" src="https://app.hearthis.at/embed/132332/transparent_black/?hcolor=00c8d6&color=&style=1&block_size=2&block_space=1&background=1&waveform=0&cover=0&autoplay=0&css="></iframe>
<!--more-->

Nellski im Weidendom auf der Fusion 2014:[^2]

<iframe style="overflow:scroll;border:none;width:100%;height:166px;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/157044366&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>

2befuzzy (alias Tobi Kirsch) mit einem Latin-Jazz.Mix:[^3]

<iframe style="overflow:scroll;border:none;width:100%;height:180;" src="https://www.mixcloud.com/widget/iframe/?feed=http%3A%2F%2Fwww.mixcloud.com%2Ftobi-kirsch%2Ffreakstyla-promo-mix%2F&amp;embed_uuid=a455f507-f48e-4758-9aad-139ca7fdf914&amp;replace=0&amp;hide_cover=1&amp;light=1&amp;embed_type=widget_standard&amp;hide_tracklist=1"></iframe>

Und zum Entspannen danach noch mein Beitrag für den [musikalischen Adventskalender im Progolog](http://www.progolog.de/category/adventskalender/) von Cpt. Pudding:[^4]

<iframe style="overflow:scroll;border:none;width:100%;height:145px;" id="hearthis_at_track_132152_light" src="https://hearthis.at/embed/132152/transparent/"></iframe>

[^1]: [\_Tasmo -- Freakstyla! Promo Mix](https://hearthis.at/tasmo/freakstyla/)
[^2]: [Nellski live at Fusion Festival 2014 Weidendom Abschluss Set](https://soundcloud.com/nellski/fusion-festival-2014)
[^3]: [2befuzzy -- Freakstyla! Promo Mix](https://www.mixcloud.com/tobi-kirsch/freakstyla-promo-mix/)
[^4]: [Advent Allstars feat. \_Tasmo -- Leise wobbelt der Schnee \[progoak14\]](https://hearthis.at/progolog-adventskalender/advent-allstars-feat-tasmo-leise-wobbelt-der-schnee-progoak14/)

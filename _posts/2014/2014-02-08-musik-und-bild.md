---
layout: post
lang: de
title:  Stimme und Bass
subtitle: Eine musikalische Wochenrückschau
permalink: /stimme-und-bass/
categories: [music]
tags: [music, video, compilation]
comments: true
share: true
image:
  feature: 2014/Lenkemz-ft-Slick.jpg
  credit: THUMB
  creditlink: https://www.youtube.com/watch?v=Yv70gF2X3PI
---

Eine Stimme allein hat sehr viel Kraft. Sie lässt zu Sagendes bei den Hörenden ankommen. Und Bass kann das Ganze noch Gewaltig verstärken. Dann noch etwas Off-Beat und Breaks...
<!--more-->

_Ghetts_ zieht mein Hören und Sehen in seinen Bann -- Hip Hop mit Botschaft und Bass! Dass ich großer Fan von _Two Fingers_ aka _Amon Tobin_ bin, ist kein Geheimnis. Es lohnt sich auch, ihm auf Twitter zu folgen, denn [er entdeckte für mich](https://twitter.com/twofingersmusic/status/432090615363678208) [Rebel:
![Ghetts - Rebel](/images/2014/Ghetts-Rebel.jpg)](https://www.youtube.com/watch?v=Q-ljuxGLiZw)

Mit nicht weniger Bass und Power sind die _Ragga Twins_ unterwegs. Ein explosives gemisch aus Raggamuffin, Jungle und Hip Hop – und überhaugt ganz schön old school. [Dreadly Zone:
![Ragga Twins - Dreadly Zone](/images/2014/Ragga-Twins-Dreadly-Zone.jpg)](https://www.youtube.com/watch?v=z55rcica6qQ)

Mit richtig Speed im Flow kommt _Angel Haze_ daher und beamt mich weg mit [Werkin Girls:
![Angel Haze - Werkin Girls](/images/2014/Angel-Haze-Werkin-Girls.jpg)](https://www.youtube.com/watch?v=szj7efHG-00)

Den Bass-Vogel abgeschossen hat allerdings _Lenkemz ft. Slick Don_ mit [Can't See U:
![Lenkemz ft Slick Don - Can't See U](/images/2014/Lenkemz-ft-Slick-Don-Cant-See-U.jpg)](https://www.youtube.com/watch?v=Yv70gF2X3PI)

_Weld EL 15_ sind aus Tunesien. Ich verstehe keine Wort ihrer Botschaft, doch das Video spricht dafür, dass sie ihre Sache ernst meinen. Sie posen mit dem Luxus-Schlitten vor Wänden mit Einschusslöchern -- eine Szene um die sie einige deutsche "Gangster"-Rapper beneiden. Ich nicht. [Boulicia Kleb:
![Weld EL 15 - Boulicia Kleb](/images/2014/Weld-EL-15-Boulicia-Kleb.jpg)](https://www.youtube.com/watch?v=6owW_Jv5ng4)

Die Spaß-Rapper von _Digges Ding_ haben sich mit Unterstützung der Bundeszentrale für politische Bildung der Aktion [YouTuber gegen Nazis](http://www.bpb.de/youtubergegennazis) angeschlossen und mit _OG_ einen feinen Anti-Nazi-Reim veröffentlicht. [Hey Mr. Nazi:
![Digges Ding - Hey Mr. Nazi](/images/2014/Digges-Ding-Hey-Mr-Nazi.jpg)](https://www.youtube.com/watch?v=t7Ir-GjeweE)

---
title: Some Old Bullshit
subtitle: Mein erstes Blog (wieder)gefunden
layout: post
lang: de
date: 2014-04-01T10:23:00+01:00
categories: [blog]
tags: [old,blog]
comments: true
share: true
---

Beim Aufräumen auf meiner Platte habe ich mein erstes Blog wiedergefunden.

Da ich es auf [Movable Type](http://movabletype.org/) geschrieben habe, ist es einfach in HTML-Dateien archiviert. Das macht es mir jetzt einfach, alles nochmal durchzusehen. Ich habe ein paar Posts von "damals" restauriert:

* [Mein erstes mal…](/2004/11/mein-erstes-mal) (11.11.2004)
  * so wie ein allererster Blogpost eben ist :trollface:
* [ganz vergessen](/2005/02/ganz-vergessen/) (9.2.2005)
  * u.a. wie ich mein Powerbook™ (fast) zerstörte
* [Grund zur Paranoia](/2006/12/grund-zur-paranoia/) (28.12.2006)
  * Heise über den 23C3
* [verspätetes Weihnachtsgeschenk](/2006/12/verspatetes-weihnachtsgeschenk) (29.12.2006)
  * das [Parocktikum-Archiv](http://www.parocktikum.de/archiv.php)
  * passt gut zu [50 Jahre DT64](http://dt64-festival.de/)
* [Mit PowerPoint in’s neue Jahr](/2007/01/mit-powerpoint-ins-neue-jahr) (1.1.2007)
* [Mit Firefox per Du - Version 2.0](/2007/01/mit-firefox-per-du-version-20) (5.1.2007)
* [Ist der Mond doch kein Amerikaner?](/2007/01/ist-der-mond-doch-kein-amerikaner) (12.1.2007)
* [Willkommen in der Copy&Paste-Gesellschaft](/2007/01/willkommen-in-der-copypaste-gesellschaft) (14.1.2007)
* [Vorfreude auf Ubuntustudio](/2007/01/vorfreude-auf-ubuntustudio) (22.1.2007)
* [Schadenfreude](/2007/01/schadenfreude) (23.1.2007)
  * Razzia bei der GEZ 
* [Noche ‘ne Linux-Audio-Distribution](/2007/01/noche-ne-linux-audio-distribution) (31.1.2007)
  * [JackLab](http://www.jacklab.org/)
* [Die Welt aus anderen Augen](/2007/01/die-welt-aus-anderen-augen) (31.1.2007)
  * [Worldmapper](http://www.worldmapper.org/) kurz vorgestellt
* [Telekomik](/2007/01/telekomik) (31.1.2007)
  * Manfred Krug über die T-Aktie
* [Fluchttiere](/2008/12/fluchttiere) (3.12.2008)
  * ein Ausschnitt aus der Performance "Wann stören wir uns endlich"
* [Das Jahresendevorspiel von Hal Faber](/2009/01/das-jahresendevorspiel-von-hal-faber) (2.1.2009)
  * viele Zitate über das Denken und Nicht-Denken
<!--more-->

Und hier der Soundtrack dazu:  
[Beastie Boys - Some Old Bullshit](https://www.youtube.com/watch?v=WUALtxHh7kY)

*[damals]: 2004 - 2009

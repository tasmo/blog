---
title: c-base spring dance 2012
layout: post
categories:
  - music
tags:
  - DJ-Set
  - music
  - selfmade
  - Tasmo
  - c-base
comments: true
share: true
---

Friday night after the final presentation of an Ableton Live workshop @ [c-base](//c-base.org/) Berlin...

<iframe style="overflow:scroll;border:none;width:100%;height:145px;" id="hearthis_at_track_115287_light" src="https://hearthis.at/embed/115287/transparent/?cover=1"></iframe>

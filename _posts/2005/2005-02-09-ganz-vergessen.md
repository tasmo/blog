---
title: ganz vergessen
layout: post
lang: de
date: 2005-02-09T13:37:00+01:00
categories:
 - Foto
tags: [old,Fotos]
comments: true
share: true
---

habe ich, das Bild von heute zu bloggen. Hat ja eh keiner gemerkt. Und doch, bitteschön:

Das war nämlich ein kurioser Tag: erst waren wir Sandkästen besorgen, damit (nunmehr) heute abend die 5 Visionäre ihre Anschauungen darbieten können. Ja, anhand von Sandkästen. Auf dem Weg zum Fabriktheater bin ich, wie so oft schon, an der Saffainsel vorbeigekommen. Von da konnte man heute die Berge sehen, die man gestern nicht sah.

Und das sah so aus:

[![ZSee24](/images/2005/285731650_cd9101b82b_o.jpg)](https://www.flickr.com/photos/73942751@N00/285731650)

Dann wollte ich das natürlich bloggen. Aber, da mein wundertolles Powerbook schon seit Tagen doofe Geräusche von sich gab, dachte ich, dem kann ich abhelfen. Also aufgeschraubt, Lüfter gesucht, Kontaktspray gefunden und munter drauf los! Sch...!!! Das war Kontaktkleber! Stand aber nicht drauf. Dann ging die Hektik los. WD-40 gesucht, alles ausgebaut, erneut eingesprüht, nein, getränkt, gesäubert, so gut es ging und: der Lüfter klappert und stößt an... :(

Dann habe ich so zusammengesucht, was noch zu retten war und nach vielem, vielem Basteln geht es jetzt wieder und ich kann wieder tolle Fotos bloggen... :)

P.S.: Morgen fahre ich erstmal nach Basel, freue mich total drauf. Nur, da gibt es nicht den Zürichsee... 

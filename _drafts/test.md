---
layout: post
title: This is just a test
---

This is [a reference link][ref]. Note how the URL does not
directly follow the link text but is put elsewhere in the
document, often at the end of the file or at the end of a
section.

:smile:
:trollface:
:joy:

[ref]: http://daringfireball.net/projects/markdown/syntax#link
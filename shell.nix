with import <nixpkgs> {};
pkgs.mkShell {
  name = "bundix";
  buildInputs = [
    ruby_3_0
    bundix
    git
    gnumake
    jekyll
    libiconv
    libpcap
    libxml2
    libxslt
    openssl
    pkg-config
    readline
    sass
    sassc
    stdenv
    zlib
  ];
}
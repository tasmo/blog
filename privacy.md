---
layout: page
permalink: /about/privacy/
title: Datenschutzerklärung
robots: noindex,nofollow,noarchive,noodp,noydir
share: false
comments: true
---

## Geltungsbereich ##

Mit dieser Datenschutzerklärung möchte ich Sie über die Art, den Umfang und Zweck der Erhebung und Verwendung personenbezogener Daten auf "[{{ site.title }}](/)" aufklären.

## Zugriffsdaten / Logdatei ##

Ich erhebe Daten über jeden Zugriff auf "[{{ site.title }}](/)" (Server-Logdaten). Zu den Zugriffsdaten gehören:

Name der abgerufenen Webseite, Datei, Datum und Uhrzeit des Abrufs, übertragene Datenmenge, Meldung über erfolgreichen Abruf, Browsertyp nebst Version, das Betriebssystem der Nutzer\*innen, Referrer URL (die zuvor besuchte Seite), IP-Adresse und der anfragende Provider.

Ich verwende die Protokolldaten nur für statistische Auswertungen zum Zweck des Betriebs, der Sicherheit und der Optimierung von "[{{ site.title }}](/)". Ich behalte mir jedoch vor, die Protokolldaten nachträglich zu überprüfen, wenn aufgrund konkreter Anhaltspunkte der berechtigte Verdacht einer rechtswidrigen Nutzung besteht.

### Cookies ###

Wenn Nutzer\*innen Seiten von "[{{ site.title }}](/)" aufrufen, werden ein oder mehrere Cookies auf deren Rechner gespeichert.  
Ein Cookie ist eine kleine Textdatei, die auf dem Computer der Nutzer\*innen gespeichert wird. Es enthält eine bestimmte Zeichenfolge, die Ihren Browser eindeutig identifiziert und dadurch eine Analyse der Benutzung der Website zu ermöglicht. Cookies können mir dabei helfen, den Komfort und die Qualität dieser Webseiten zu verbessern, indem zum Beispiel Nutzer\*inneneinstellungen gespeichert werden.

Eine Nutzung von "[{{ site.title }}](/)" ist auch ohne Cookies möglich. Nutzer\*innen können in Ihrem Browser das Speichern von Cookies deaktivieren, auf bestimmte Websites beschränken oder Ihren Browser so einstellen, dass er sie benachrichtigt, bevor ein Cookie gespeichert wird. Sie können die Cookies über die Datenschutzfunktionen ihres Browsers jederzeit von der Festplatte ihres Rechners löschen. Ich versuche alles, dass trotzdem die Funktionen und die Benutzer\*innenfreundlichkeit auf den betreffenden Seiten nicht eingeschränkt werden.

Die Cookies von "[{{ site.title }}](/)" richten auf dem Rechner der Nutzer\*innen keinen Schaden an und enthalten keine Viren.

{% if site.piwik %}
### Piwik ###

Obwohl ich sehr selten die Statistiken ansehe, benutze ich für "[{{ site.title }}](/)" <a onclick="javascript:_gaq.push(['_trackEvent','outbound-article','http://piwik.org/']);" href="http://piwik.org/">Piwik</a>, eine Open-Source-Software zur statistischen Auswertung der Zugriffe. Die durch [Cookies](#cookies) erzeugten Informationen über die Benutzung von "[{{ site.title }}](/)" werden auf einem Server in Deutschland gespeichert. Die IP-Adresse wird sofort nach der Verarbeitung und vor deren Speicherung anonymisiert.

<iframe src="http://piwik.tasmo.de/index.php?module=CoreAdminHome&amp;action=optOut&amp;language=de" style="height:13em; width:100%; border:none;"></iframe>
{% endif %}

## Einbindung von Diensten und Inhalten Dritter ##

Hin und wieder binde ich innerhalb "[{{ site.title }}](/)" Inhalte Dritter ein, wie zum Beispiel Videos von _YouTube_, iFrames von _Hearthis.at_ und RSS-Feeds oder Grafiken von anderen Webseiten. Dies setzt immer voraus, dass die Anbieter dieser Inhalte die IP-Adresse der Nutzer\*innen wahr nehmen -- ohne die IP-Adresse, könnten sie die Inhalte nicht an den Browser der jeweiligen Nutzer\*innen senden. Die IP-Adresse ist so für die Darstellung dieser Inhalte erforderlich. Ich bemühe mich nur solche Inhalte zu verwenden, deren jeweilige Anbieter die IP-Adresse lediglich zur Auslieferung der Inhalte verwenden. Jedoch habe ich keinen Einfluss darauf, falls Anbieter dieser Inhalte die IP-Adresse z.B. für statistische Zwecke speichern. Soweit dies mir bekannt ist, kläre ich Sie als Nutzer\*in darüber auf.

### Google Analytics ###

Obwohl ich auf "[{{ site.title }}](/)" _Google Analytics_, einen Webanalysedienst der Google Inc. ("Google"), nicht benutze, kann ich durch das [Einbindung von Diensten und Inhalten Dritter](#einbindung-von-diensten-und-inhalten-dritter) leider nicht ausschließen, dass _Google_ trotzdem Daten erhebt. Auch _Google Analytics_ verwendet [Cookies](#cookies). Die durch Cookies erzeugten Informationen werden von _Google_ in der Regel an einen Server in den USA übertragen und dort gespeichert.

Die im Rahmen von _Google Analytics_ von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von _Google_ zusammengeführt. Sie können die Erfassung der durch das Cookie erzeugten und auf ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an _Google_ sowie die Verarbeitung dieser Daten durch _Google_ verhindern, indem sie das unter dem folgenden Link verfügbare (oder ein entsprechendes) Browser-Plugin herunterladen und installieren: [Browser-Add-on zur Deaktivierung von _Google Analytics_](http://tools.google.com/dlpage/gaoptout?hl=de).

## Widerruf, Änderungen, Berichtigungen und Aktualisierungen ##

Sie als Nutzer\*in meiner Seite haben das Recht, auf Antrag unentgeltlich Auskunft zu erhalten über die personenbezogenen Daten, die über sie gespeichert wurden. Zusätzlich haben Sie das Recht auf Berichtigung unrichtiger Daten, Sperrung und Löschung dieser personenbezogenen Daten, soweit dem keine gesetzliche Aufbewahrungspflicht entgegensteht.

> Sunlight is said to be the best of disinfectants.  
-- _Justice Louis D. Brandeis_

Ich habe weder in der Vergangenheit Daten der Nutzer\*innen von "[{{ site.title }}](/)" mit oder ohne Anfrage an Dritte weitergegeben, noch werde ich das in Zukunft tun!

### Kontaktaufnahme ###

Bei der [Kontaktaufnahme](/about/impress/) mit mir werden die Ihre Angaben zwecks Bearbeitung der Anfrage sowie für den Fall, dass Anschlussfragen entstehen, gespeichert.

* * *

<smalL>
Quelle: Datenschutz-Muster von Rechtsanwalt Thomas Schwenke auf [I LAW it](http://rechtsanwalt-schwenke.de/smmr-buch/datenschutz-muster-generator-fuer-webseiten-blogs-und-social-media/)
</smalL>

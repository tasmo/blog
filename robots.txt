---
---
User-agent: *
Disallow: /about/
Disallow: /files/
Disallow: /articles/
Disallow: /assets/
Disallow: /page*/
Sitemap: {{ site.url }}/sitemap.xml

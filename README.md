---
layout: page
permalink: /about/site/
title: README
tags: [meta, blog, Jekyll]
comments: true
published: true
---

# "[tasmo\[rocks~\]](https://tasmo.rocks/)"

... runs with the power of [Jekyll](http://jekyllrb.com/):

> Jekyll is a simple, blog aware, static site generator written in the [Ruby](https://ruby-lang.org/) language. It takes text files written in [Textile](http://txstyle.org/) or [Markdown](https://daringfireball.net/projects/markdown/), runs it through a template and [Liquid](http://liquidmarkup.org/) converters and spits out a complete, static website suitable for serving with your web server.

## Theme

The theme I use on this site is a modified version of [Michael Rose's HPSTR Jekyll Theme](https://github.com/mmistakes/jekyll-theme-hpstr).

## Issues

Feel free to post any issues or requests here: [codeberg.org/tasmo/blog](https://codeberg.org/tasmo/blog/issues)

## License

The theme and side skeleton are licensed under the MIT License (MIT):

<pre>
Copyright (c) 2020 Michael Rose / Thomas Friese

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
</pre>

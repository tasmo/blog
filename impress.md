---
layout: page
permalink: /about/impress/
title: Impressum
robots: noindex,nofollow,noarchive,noodp,noydir
share: false
comments: true
---

Angaben gemäß [§ 5 TMG](https://github.com/bundestag/gesetze/blob/master/t/tmg/index.md#-5-allgemeine-informationspflichten "Telemediengesetz"):

Verantwortlich für den Inhalt nach [§ 55 Abs. 2 RStV](http://revosax.sachsen.de/vorschrift_gesamt/1236.html#p55 "Rundfunkstaatsvertrag"):

<address>
    <span class="vcard">
        <span class="adr">
            <i class="fa fa-male fa-fw" title="name">
                <span class="hidden">name:</span>
            </i>
            <span class="fn"> Thomas Friese</span>
            <br />
            <i class="fa fa-user fa-fw" title="nickname (IRC++)">
                <span class="hidden">nick:</span>
            </i>
            <span class="nickname"> <a href="/about/tasmo">tasmo</a></span>
            <br />
            <i class="fa fa-bookmark fa-fw" title="home page">
                <span class="hidden">home:</span>
            </i>
            <span class="url fn"> <a href="/">tasmo.rocks</a></span>
            <br />
            <i class="fa fa-envelope fa-fw" title="email">
                <span class="hidden">email:</span>
            </i>
            <span class="email"> <a href="&#109;&#097;&#105;&#108;&#116;&#111;:&#112;&#111;&#115;&#116;&#064;&#115;&#101;&#099;&#117;&#114;&#101;&#046;&#116;&#097;&#115;&#109;&#111;&#046;&#100;&#101;">&#112;&#111;&#115;&#116;&#064;&#115;&#101;&#099;&#117;&#114;&#101;&#046;&#116;&#097;&#115;&#109;&#111;&#046;&#100;&#101;</a></span>
            <br />
            <i class="fa fa-comments fa-fw" title="threema">
                <span class="hidden">threema:</span>
            </i>
            <span class="chat"> <a class="iframe" data-fancybox-type="iframe" href="/threema-key-fingerprint.html">HYW5BMBK</a></span>
            <br />
            <i class="fa fa-phone fa-fw" title="mobile phone number">
                <span class="hidden">mobile:</span>
            </i>
            <span class="tel"> <script type="text/javascript">document.write(
"+49-(0)30-62938877".replace(/[a-zA-Z]/g, function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}));</script></span>
            <br />
            <i class="fa fa-ambulance fa-fw" title="international emergency phone number">
                <span class="hidden">emergency:</span>
            </i>
            <span class="emergency"> <a href="https://www.youtube.com/watch?v=ab8GtuPdrUQ">0118 999 88199 9119 725 3</a></span>
        </span>
    </span>
</address>

<i class="fa fa-key fa-fw em1"></i> <strong id="gpg">GPG</strong><sup id="fnref:1"><a href="#fn:1" class="footnote">1</a></sup>:<br>
<span style="padding-left: 1.5em;">Short Key ID: <code><a class="iframe" data-fancybox-type="iframe" href="/GPG-key.html">7CDBF580</a></code></span><br>
<span style="padding-left: 1.5em;">Fingerabdruck: <code><a class="iframe" data-fancybox-type="iframe" href="/GPG-key.html">FC1B A19F B77D A327 FBDF  E24C 6562 7F47 7CDB F580</a></code></span><br>
<span style="padding-left: 1.5em;"><small>(<a href="/7CDBF580.pub"><i class="fa fa-download em1"></i> download</a>)</small></span>

<div class="footnotes">
  <ol>
    <li id="fn:1">
      <p>“<a href="http://de.wikipedia.org/wiki/GNU_Privacy_Guard">GNU Privacy Guard</a>” <a href="#fnref:1" class="reversefootnote">&#8617;</a></p>
    </li>
  </ol>
</div>
